package home.crsk.todaysweather.domain.common.gateway.data.forecast

import home.crsk.todaysweather.domain.common.entity.Forecast
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import io.reactivex.Observable

interface ForecastRepositoryFacade {

    companion object {
        val ONE_HOUR_IN_SECONDS = 3600L
        val IN_FLIGHT = Forecast.NULL
    }

    fun observe(locationKey: String, langWUCode: String, unit: UnitEnum, remoteForced: Boolean = false): Observable<Forecast>
}