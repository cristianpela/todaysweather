package home.crsk.todaysweather.domain.common.usecases

import io.reactivex.ObservableTransformer

class ResetUseCase: UseCase{
    override fun execute(): ObservableTransformer<in Request, out Response> {
        return ObservableTransformer { upstream -> upstream.map{ Response.Stateful.Reset } }
    }

}