package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import com.google.gson.Gson
import home.crsk.todaysweather.drivers.data.remote.retrofit.BaseRetrofitTest
import io.reactivex.observers.TestObserver
import org.junit.Test
import kotlin.test.assertEquals

class AutoCompleteRepositoryRetrofitTest : BaseRetrofitTest() {

    val test: TestObserver<AutoCompleteListJSON> by lazy {
        mockRetrofit.create(AutoCompleteRemoteService::class.java).find("foo").test()
    }

    @Test
    fun find() {
        val bodyContent = loadResource("test_auto_complete.json").readText()
        server.enqueue(createResponse(bodyContent))
        test.assertNoErrors()
        assertEquals(1, test.valueCount())
        test.assertValueAt(0, { it == Gson().fromJson(bodyContent, AutoCompleteListJSON::class.java) })
    }
}

