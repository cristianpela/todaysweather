package home.crsk.todaysweather.domain.common.other

import home.crsk.todaysweather.domain.common.gateway.data.forecast.CorruptedQueryResultError
import home.crsk.todaysweather.domain.common.gateway.data.forecast.InvalidApiKeyError
import home.crsk.todaysweather.domain.common.gateway.data.forecast.QueryNotFoundError
import home.crsk.todaysweather.domain.common.log.Loggy
import io.reactivex.*
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

interface SchedulersContract {

    fun main(): Scheduler

    fun io(): Scheduler

    fun computation(): Scheduler

}

/**
 * based on <a href="http://kevinmarlow.me/better-networking-with-rxjava-and-retrofit-on-android/">article</a>
 */
fun errorBackOffFlowable(retries: Int = 4, initialDelay: Int = 5, unit: TimeUnit = TimeUnit.SECONDS,
                         skip: List<Class<out Throwable>> = listOf(
                                 QueryNotFoundError::class.java,
                                 InvalidApiKeyError::class.java),
                         delayScheduler: Scheduler = Schedulers.computation()):
        Function<in Flowable<out Throwable>, Flowable<*>> {

    assert(retries >= 1 && initialDelay >= 1) {
        "Retries and initial delay must be larger than 1. Provided $retries and $initialDelay"
    }

    val backOffCompletedCode = -1

    return Function { errors ->
        errors.zipWith(Flowable.range(1, retries + 1),
                BiFunction<Throwable, Int, Pair<Throwable, Int>> { err, i ->
                    if (i == retries || err.javaClass in skip)
                        err to backOffCompletedCode
                    else err to i
                })
                .flatMap {
                    if (it.second == backOffCompletedCode)
                        Flowable.error(it.first)
                    else {
                        Flowable.timer(delayStrategy(initialDelay, unit, it), unit, delayScheduler)
                    }
                }

    }
}


/**
 * based on <a href="http://kevinmarlow.me/better-networking-with-rxjava-and-retrofit-on-android/">article</a>
 */
fun errorBackOffObservable(retries: Int = 4, initialDelay: Int = 5, unit: TimeUnit = TimeUnit.SECONDS,
                           skip: List<Class<out Throwable>> = listOf(
                                   QueryNotFoundError::class.java,
                                   InvalidApiKeyError::class.java),
                           delayScheduler: Scheduler = Schedulers.computation()):
        Function<in Observable<out Throwable>, Observable<*>> {

    assert(retries >= 1 && initialDelay >= 1) {
        "Retries and initial delay must be larger than 1. Provided $retries and $initialDelay"
    }

    val backOffCompletedCode = -1

    return Function { errors ->
        errors.zipWith(Observable.range(1, retries + 1),
                BiFunction<Throwable, Int, Pair<Throwable, Int>> { err, i ->
                    if (i == retries || err.javaClass in skip)
                        err to backOffCompletedCode
                    else err to i
                })
                .flatMap {
                    if (it.second == backOffCompletedCode)
                        Observable.error(it.first)
                    else {
                        Observable.timer(delayStrategy(initialDelay, unit, it), unit, delayScheduler)
                    }
                }

    }
}

private fun delayStrategy(initialDelay: Int, unit: TimeUnit, pair: Pair<Throwable, Int>): Long =
        (initialDelay * pair.second.toDouble()).toLong().also {
            Loggy.d("Back-Off Retry Delay: $it $unit for ${pair.first}:${pair.first.message}")
        }

fun <T> T.observable(): Observable<out T> = Observable.just<T>(this)
fun <T> T.single(): Single<out T> = Single.just<T>(this)
fun <T> T.maybe(): Maybe<out T> = Maybe.just<T>(this)

fun Scheduler.nowMillis() = now(TimeUnit.MILLISECONDS)