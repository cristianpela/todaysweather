package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.data.forecast.CorruptedQueryResultError
import home.crsk.todaysweather.domain.common.gateway.data.forecast.QueryNotFoundError
import home.crsk.todaysweather.domain.common.other.advance
import java.util.*

fun FullForecast.toForecastEntity(lastUpdate: Long): ForecastEntity? {

    val forecastJSON: ForecastJSON = this.forecast!! //npe worthy

    val simpleForecasts: List<SimpleForecast> = forecastJSON.simpleforecast?.forecastday?.map {

        val timeData = with(it?.date!!) {
            TimeData.fromPieces(year, month, day, hour, min, sec)
        }


        val high = it.high.let {
            UnitPayload.both(it?.celsius!!, it.fahrenheit) {
                it.degreeSymbol
            }
        }

        val low = it.low.let {
            UnitPayload.both(it?.celsius!!, it.fahrenheit) {
                it.degreeSymbol
            }
        }

        val maxWindSpeed = WindSpeed(
                it.maxwind.let {
                    UnitPayload.both(it?.kph!!, it.mph) {
                        it.speedMeasure
                    }
                },
                it.maxwind?.dir!!,
                it.maxwind.degrees
        )

        val avgWindSpeed = WindSpeed(
                it.avewind.let {
                    UnitPayload.both(it?.kph!!, it.mph) {
                        it.speedMeasure
                    }
                },
                it.avewind?.dir!!,
                it.avewind.degrees
        )

        val rainQuantity = it.qpfAllday.let {
            UnitPayload.both(it?.mm!!.toFloat(), it.inch!!.toFloat()) {
                it.rainMeasure
            }
        }

        val snowQuantity = it.snowAllday.let {
            UnitPayload.both(it?.cm!!.toFloat(), it.inch!!.toFloat()) {
                it.snowMeasure
            }
        }

        SimpleForecast(timeData, high, low, it.conditions!!, it.iconUrl!!, it.pop,
                maxWindSpeed, avgWindSpeed, it.maxhumidity, rainQuantity, snowQuantity)
    } ?: emptyList()

    val textForecasts: List<TextForecast> = forecastJSON.txtForecast?.forecastday?.mapIndexed { index, item ->

        // 0, 0, 1, 1, 2, 2 - to simulate day period and night period for the same day.
        val period = index / 2

        val timeData = simpleForecasts.first().time.advance(Calendar.DAY_OF_MONTH, period)

        val text = UnitPayload.both(item?.fctTextMetric!!, item.fctText!!)

        TextForecast(timeData, item.period, item.iconUrl!!, item.title!!, text, item.pop)

    } ?: emptyList()

    val hourlyForecasts: List<HourForecast> = this.hourlyForecast?.map {

        val timeData = with(it?.fCTTIME!!) {
            TimeData.fromPieces(year, mon, mday, hour, min, sec)
        }

        val temperature = it.temp!!.let {
            UnitPayload.both(it.metric, it.english) {
                it.degreeSymbol
            }
        }

        val windSpeed = WindSpeed(
                it.wspd.let {
                    UnitPayload.both(it?.metric!!, it.english) {
                        it.speedMeasure
                    }
                },
                it.wdir?.dir!!,
                it.wdir.degrees
        )

        val feelsLike = it.feelslike!!.let {
            UnitPayload.both(it.metric, it.english) {
                it.degreeSymbol
            }
        }
        HourForecast(timeData, temperature, it.condition!!, it.iconUrl!!, windSpeed, feelsLike,
                it.pop, it.humidity)
    } ?: emptyList()

    if (simpleForecasts.isEmpty() || textForecasts.isEmpty() || hourlyForecasts.isEmpty())
        throw CorruptedQueryResultError

    return ForecastEntity(simpleForecasts, textForecasts, hourlyForecasts, lastUpdate)
}


typealias ForecastJSON = Forecast
typealias ForecastEntity = home.crsk.todaysweather.domain.common.entity.Forecast
