@file:Suppress("unused", "UNUSED_PARAMETER")

package home.crsk.todaysweather.domain.common.presentation

import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface PresentationStreamOps<S : State> {
    fun bindIntents(intentStream: Observable<Intent>): Disposable
    fun observeState(): Observable<S>
    fun observeErrors(): Observable<Throwable>
    fun observeMessages(): Observable<CommonMessage>
}

@JvmOverloads
fun <S : State> PresentationStreamOps<*>.decorate(
        stateDecorator: ObservableTransformer<State, S>,
        errorDecorator: ObservableTransformer<Throwable, Throwable> = ObservableTransformer { up -> up },
        messageDecorator: ObservableTransformer<CommonMessage, CommonMessage> = ObservableTransformer { up -> up }
): PresentationStreamOps<S> {

    return object : PresentationStreamOps<S> {

        override fun bindIntents(intentStream: Observable<Intent>): Disposable = this@decorate.bindIntents(intentStream)

        override fun observeErrors(): Observable<Throwable> = this@decorate.observeErrors()

        override fun observeMessages(): Observable<CommonMessage> = this@decorate.observeMessages()

        override fun observeState(): Observable<S> = this@decorate.observeState().compose(stateDecorator)

    }


}

fun <S : State> compositePresentationStreamOps(provider: (Array<out State>) -> S,
                                               vararg streamOps: PresentationStreamOps<*>): PresentationStreamOps<S> {

    return object : PresentationStreamOps<S> {

        override fun bindIntents(intentStream: Observable<Intent>): Disposable {
            val compositeDisposables = CompositeDisposable()
            streamOps.forEach {
                compositeDisposables.add(it.bindIntents(intentStream))
            }
            return compositeDisposables
        }

        override fun observeState(): Observable<S> = Observable
                .combineLatest(streamOps.map { it.observeState() }) { states: Array<Any> ->
                    val arrayOfStates = states.map { it as State }.toTypedArray()
                    provider(arrayOfStates).apply {
                        if (arrayOfStates.map { it.inFlight }.reduce { acc, curr -> acc || curr }) {
                            inFlight()
                        } else {
                            completed()
                        }
                    }
                }

        override fun observeErrors(): Observable<Throwable> = Observable
                .merge(streamOps.map { it.observeErrors() })

        override fun observeMessages(): Observable<CommonMessage> = Observable
                .merge(streamOps.map { it.observeMessages() })

    }
}

