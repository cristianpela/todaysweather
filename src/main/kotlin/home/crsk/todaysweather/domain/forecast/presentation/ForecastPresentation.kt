package home.crsk.todaysweather.domain.forecast.presentation

import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.other.*
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Intent.*
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.presentation.State
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import java.util.concurrent.TimeUnit

/**
 * watchers:
 * reacts to current location forecast changes (due to hourly updates) or to current location changes (due to new search)
 * reacts to favorite changes (due favorite change action and if current location matches the change)
 *
 * actions:
 * retry manually when an error occurs
 * favorite/un-favorite the current location
 * change current location via auto-complete selection and then save it
 */
class ForecastPresentation(private val forecastWatchUseCase: UseCase,
                           private val forecastUpdateUseCase: UseCase,
                           private val favoriteWatchUseCase: UseCase,
                           private val favoriteChangeUseCase: UseCase,
                           private val settingsSaveUseCase: UseCase,
                           private val navigationToUseCase: UseCase,
                           schedulersContract: SchedulersContract,
                           private val initialState: ForecastState = ForecastState(),
                           private val timeReference: () -> TimeData = { now().timeData() })
    : Presentation<ForecastState>(schedulersContract) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                //forecast intents
                    ForecastWatchIntent -> Request.ForecastWatchRequest()
                    CancelIntent -> Request.CompositeRequest(
                            Request.ForecastWatchRequest(true),
                            Request.FavoriteWatchRequest(true))
                    RetryIntent -> Request.RetryRequest
                //favorite intents
                    FavoriteWatchIntent -> Request.FavoriteWatchRequest(false)
                    is FavoriteChangeIntent -> Request.FavoriteChangeRequest(it.location)
                //save search intents
                    is SelectLocationIntent -> it.locationKey?.let { Request.SaveRequest.SaveLocationRequest(Location.bare(it)) }
                            ?: Request.NoRequest
                //save unit - change from metric to english and vice-versa
                    is Intent.UnitChangeIntent -> Request.SaveRequest.SaveUnitRequest(it.unit)
                //navigation intents
                    is NavigationIntent -> Request.NavigationToRequest(it.route)
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<ForecastState, Response.Stateful, ForecastState> =
            BiFunction { carrier, response ->
                when (response) {
                    Response.Stateful.InFlight -> carrier.copy()(true)
                    Response.Stateful.Completed -> carrier.copy()()
                    is Response.Stateful.FavoriteChangeResponse -> {
                        carrier.currentLocation?.locationKey.let {
                            if (response.favorite.locationKey == it)
                                carrier.copy(currentLocation = carrier
                                        .currentLocation?.copy(favorite = response.favorite.isFavorite)).apply { completed() }
                            else
                                carrier()
                        }
                    }
                    is Response.Stateful.ForecastResponse -> {
                        val currentLocation = response.location

                        val convertedForecast = convertForecast(response)
                        val hourForecast = convertedForecast[1] as DisplayableHourForecast
                        val hourForecastIndex = findCurrentHourIndex(response)
                        val todayForecast = convertedForecast[0] as TodayForecast
                        @Suppress("UNCHECKED_CAST")
                        val threeDaysHourForecasts = convertedForecast[2] as List<DisplayableHourForecast>
                        val unit = convertedForecast[3] as UnitEnum

                        val lastUpdate = lastUpdate(response.forecast.lastUpdate)
                        val isFresh = isFresh(response.forecast.lastUpdate)
                        ForecastState(todayForecast, hourForecast, threeDaysHourForecasts,
                                hourForecastIndex, currentLocation, lastUpdate, isFresh, unit)()
                    }
                    else -> carrier
                }
            }

    private fun lastUpdate(lastUpdate: Long): String? =
            lastUpdate.timeData().display("dd.MM.yyyy HH:mm")


    private fun isFresh(lastUpdate: Long): Boolean =
            !timeReference().time.isTimeOlderThan(1, TimeUnit.DAYS, lastUpdate)

    private fun findCurrentHourIndex(response: Response.Stateful.ForecastResponse): Int {
        val currentTime = timeReference()
        return response.forecast.hourlyForecasts.indexOfFirst {
            with(it.time) {
                currentTime.year == year
                        && currentTime.month == month
                        && currentTime.day == day
                        && currentTime.hour == hour
            }
        }.let {
                    if (it == -1) {
                        val lastIndex = response.forecast.hourlyForecasts.lastIndex
                        if (currentTime.time > response.forecast.hourlyForecasts[lastIndex].time.time) {
                            lastIndex
                        } else {
                            0
                        }
                    } else {
                        it
                    }
                }
    }

    private fun convertForecast(response: Response.Stateful.ForecastResponse): Array<Any> {

        val currentTime = timeReference()

        val hourForecast = response.forecast.hourlyForecasts.firstOrNull {
            with(it.time) {
                currentTime.year == year
                        && currentTime.month == month
                        && currentTime.day == day
                        //the first time the hour is one in advance of current system hour
                        && (currentTime.hour == hour || currentTime.hour == hour - 1)
            }
        } ?: response.forecast.hourlyForecasts.last()


        val (todaySimpleForecast, nextDaysSimpleForecasts) = with(response.forecast.simpleForecasts) {
            val tsf = response.forecast.simpleForecasts.firstOrNull {
                with(it.time) {
                    currentTime.year == year
                            && currentTime.month == month
                            && currentTime.day == day
                }
            } ?: response.forecast.simpleForecasts.last()


            val todaySimpleForecastIndex = response.forecast.simpleForecasts.indexOf(tsf)
            val ndsf: List<NextDaySummary> =
                    if (todaySimpleForecastIndex + 1 < response.forecast.simpleForecasts.size)
                        response.forecast.simpleForecasts
                                .subList(todaySimpleForecastIndex + 1, response.forecast.simpleForecasts.size)
                                .asSequence()
                                .map {
                                    NextDaySummary(
                                            it.time.display("dd MMM"),
                                            it.icon,
                                            "${it.low.obtain()}/${it.high.obtain()}")
                                }
                                .toList()
                    else
                        emptyList()

            tsf to ndsf
        }



        val (dayTxtForecast, nightTxtForecast) = response.forecast.textForecasts.filter {
            with(it.time) {
                currentTime.year == year
                        && currentTime.month == month
                        && currentTime.day == day
            }
        }.let {
                    if (it.isEmpty())
                        Pair(it[it.lastIndex - 1], it.last())
                    else
                        Pair(it.first(), it.last())
                }

        val displayableHourForecast = convertToDisplayableHourForecast(hourForecast, "hh:mm a")

        val threeDaysDisplayableHourForecasts = response.forecast.hourlyForecasts.map {
            convertToDisplayableHourForecast(it, "HH:mm")
        }

        val unit = hourForecast.temperatureDegree.unit // doesn't matter where you get unit,
        //as long as that struct has a temperature unit field

        return arrayOf(
                TodayForecast(
                        todaySimpleForecast.low.obtain().let {
                            if (it.value.isNullLike()) "?" else it.toString()
                        } + "/" + todaySimpleForecast.high.obtain().let {
                            if (it.value.isNullLike()) "?" else it.toString()
                        },
                        dayTxtForecast.title,
                        dayTxtForecast.text.obtain().value,
                        dayTxtForecast.icon,
                        nightTxtForecast.title,
                        nightTxtForecast.text.obtain().value,
                        nightTxtForecast.icon,
                        todaySimpleForecast.icon,
                        todaySimpleForecast.conditions,
                        todaySimpleForecast.precipitationChance.asPercent(),
                        "${todaySimpleForecast.averageWindSpeed}(max.${todaySimpleForecast.averageWindSpeed})",
                        todaySimpleForecast.humidity.asPercent(),
                        todaySimpleForecast.rainQuantity.obtain()
                                .let { if (it.value > 0) it.toString() else null },
                        todaySimpleForecast.snowQuantity.obtain()
                                .let { if (it.value > 0) it.toString() else null },
                        nextDaysSimpleForecasts
                ), displayableHourForecast, threeDaysDisplayableHourForecasts, unit)

    }

    private fun convertToDisplayableHourForecast(hourForecast: HourForecast, hourFormat: String) =
            DisplayableHourForecast(
                    hourForecast.time.display(hourFormat),
                    hourForecast.temperatureDegree.obtain().let { uv ->
                        if (uv.value.isNullLike()) "?" else uv.toString()
                    },
                    hourForecast.condition,
                    hourForecast.icon,
                    hourForecast.precipitationChance.asPercent(),
                    hourForecast.humidity.asPercent(),
                    hourForecast.windSpeed.speed.obtain().let {
                        if (it.value.isNullLike()) "?" else it.toString()
                    },
                    hourForecast.windSpeed.direction.let {
                        if (it.isEmpty()) "?" else it
                    },
                    hourForecast.windSpeed.degrees.toStringOrNullLike(),
                    hourForecast.feelsLike.obtain().let {
                        if (it.value.isNullLike()) "?" else it.toString()
                    }
            )

    override fun defaultState(): ForecastState = initialState

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(
                    dispatchRequest<Request.ForecastWatchRequest>(requestObservable, forecastWatchUseCase),
                    dispatchRequest<Request.RetryRequest>(requestObservable, forecastUpdateUseCase),

                    dispatchRequest<Request.FavoriteWatchRequest>(requestObservable, favoriteWatchUseCase),
                    dispatchRequest<Request.FavoriteChangeRequest>(requestObservable, favoriteChangeUseCase),

                    dispatchRequest(requestObservable, listOf(
                            Request.SaveRequest.SaveUnitRequest::class.java,
                            Request.SaveRequest.SaveLocationRequest::class.java
                    ), settingsSaveUseCase),
                    dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase)
            )

}

data class ForecastState(val todayForecast: TodayForecast? = null,
                         val hourForecast: DisplayableHourForecast? = null,
                         val threeDaysHourForecasts: List<DisplayableHourForecast>? = null,
                         val hourForecastIndex: Int? = null,
                         val currentLocation: Location? = null,
                         val lastUpdate: String? = null,
                         val isFresh: Boolean = false,
                         val unit: UnitEnum? = null) : State()

data class TodayForecast(
        val tempMinMaxDisplay: String,
        val textDayTitle: String,
        val textDay: String,
        val textDayIcon: String,
        val textNightTitle: String,
        val textNight: String,
        val textNightIcon: String,
        val todayIconUrl: String,
        val todayCondition: String,
        val todayPrecipitationChance: String,
        val todayWindSpeed: String,
        val todayHumidity: String,
        val todayRainQuantity: String?,
        val todaySnowQuantity: String?,
        val nextDaysSummary: List<NextDaySummary> = emptyList()
)

data class DisplayableHourForecast(val hourDisplay: String,
                                   val hourTempDisplay: String,
                                   val hourCondition: String,
                                   val hourIconUrl: String,
                                   val hourPrecipitationChance: String,
                                   val hourHumidity: String,
                                   val hourWindSpeed: String,
                                   val hourWindDirection: String,
                                   val hourWindDegrees: String,
                                   val hourFeelsLike: String)

data class NextDaySummary(val date: String, val iconUrl: String, val tempMinMaxDisplay: String)