package home.crsk.todaysweather.domain.common.entity

import org.junit.Assert.assertEquals
import org.junit.Test



class LocationTest {


    @Test
    fun `distance between locations`() {
        val origin = Location(latitude = 45.666356, longitude = 21.172986)
        val error = 500.0 //in meters

        assertEquals(7310.0, Location.distanceBetweenLocations(
                origin,
                Location(latitude = 45.630115, longitude = 21.251264)
        ), error)

        assertEquals(1770.0, Location.distanceBetweenLocations(
                origin,
                Location(latitude = 45.650519, longitude = 21.170583)
        ), error)

        assertEquals(5300.0, Location.distanceBetweenLocations(
                origin,
                Location(latitude = 45.629155, longitude = 21.130414)
        ), error)
        assertEquals(700.0,
                Location.distanceBetweenCoordinates(45.75372,45.759998,  21.22571, 21.230000),
                error)
    }

}