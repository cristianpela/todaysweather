package home.crsk.todaysweather.domain.common.usecases

import io.reactivex.Observable
import io.reactivex.ObservableTransformer

class Validator<T>(private val rules: List<ValidationRule<T>> = listOf()) {

    fun validateTransformer(): ObservableTransformer<T, T> = ObservableTransformer { up ->
        up.flatMap { item -> validate(item) }
    }

    @Suppress("MemberVisibilityCanPrivate")
    fun validate(item: T): Observable<T> = validate(item, Observable.just(item))

    /**
     * Let the stream continue if item passes the check
     */
    fun <N> validate(item: T, next: Observable<N>): Observable<N> {
        val failedRule = rules.firstOrNull { !it.validate(item) }
        return if (failedRule == null) next else Observable.error(failedRule.fail())
    }

}

interface ValidationRule<in T> {

    fun validate(item: T): Boolean

    fun fail(): Throwable

}