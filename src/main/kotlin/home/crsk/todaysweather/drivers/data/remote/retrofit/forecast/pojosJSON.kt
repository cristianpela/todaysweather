package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import com.google.gson.annotations.SerializedName

import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class FullForecast(

        @field:SerializedName("response")
        val response: Response? = null,

        @field:SerializedName("forecast")
        val forecast: Forecast? = null,

        @field:SerializedName("hourly_forecast")
        val hourlyForecast: List<HourlyForecastItem?>? = null
)

//#############################################3

@Generated("com.robohorse.robopojogenerator")
data class Avewind(

        @field:SerializedName("kph")
        val kph: Int,

        @field:SerializedName("mph")
        val mph: Int,

        @field:SerializedName("dir")
        val dir: String? = null,

        @field:SerializedName("degrees")
        val degrees: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Date(

        @field:SerializedName("tz_short")
        val tzShort: String? = null,

        @field:SerializedName("pretty")
        val pretty: String? = null,

        @field:SerializedName("ampm")
        val ampm: String? = null,

        @field:SerializedName("year")
        val year: Int,

        @field:SerializedName("isdst")
        val isdst: String? = null,

        @field:SerializedName("weekday")
        val weekday: String? = null,

        @field:SerializedName("weekday_short")
        val weekdayShort: String? = null,

        @field:SerializedName("epoch")
        val epoch: Long? = null,

        @field:SerializedName("sec")
        val sec: Int,

        @field:SerializedName("min")
        val min: Int,

        @field:SerializedName("month")
        val month: Int,

        @field:SerializedName("hour")
        val hour: Int,

        @field:SerializedName("monthname_short")
        val monthnameShort: String? = null,

        @field:SerializedName("monthname")
        val monthname: String? = null,

        @field:SerializedName("tz_long")
        val tzLong: String? = null,

        @field:SerializedName("yday")
        val yday: Int,

        @field:SerializedName("day")
        val day: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Dewpoint(

        @field:SerializedName("metric")
        val metric: String? = null,

        @field:SerializedName("english")
        val english: String? = null
)

@Generated("com.robohorse.robopojogenerator")
data class FCTTIME(

        @field:SerializedName("mday_padded")
        val mdayPadded: String? = null,

        @field:SerializedName("month_name")
        val monthName: String? = null,

        @field:SerializedName("weekday_name_night_unlang")
        val weekdayNameNightUnlang: String? = null,

        @field:SerializedName("ampm")
        val ampm: String? = null,

        @field:SerializedName("year")
        val year: Int,

        @field:SerializedName("tz")
        val tz: String? = null,

        @field:SerializedName("epoch")
        val epoch: Long,

        @field:SerializedName("month_name_abbrev")
        val monthNameAbbrev: String? = null,

        @field:SerializedName("weekday_name_abbrev")
        val weekdayNameAbbrev: String? = null,

        @field:SerializedName("mon")
        val mon: Int,

        @field:SerializedName("mon_abbrev")
        val monAbbrev: String? = null,

        @field:SerializedName("sec")
        val sec: Int,

        @field:SerializedName("min")
        val min: Int,

        @field:SerializedName("hour")
        val hour: Int,

        @field:SerializedName("min_unpadded")
        val minUnpadded: String? = null,

        @field:SerializedName("civil")
        val civil: String? = null,

        @field:SerializedName("weekday_name_night")
        val weekdayNameNight: String? = null,

        @field:SerializedName("pretty")
        val pretty: String? = null,

        @field:SerializedName("mon_padded")
        val monPadded: String? = null,

        @field:SerializedName("weekday_name_unlang")
        val weekdayNameUnlang: String? = null,

        @field:SerializedName("isdst")
        val isdst: String? = null,

        @field:SerializedName("hour_padded")
        val hourPadded: String? = null,

        @field:SerializedName("UTCDATE")
        val uTCDATE: String? = null,

        @field:SerializedName("yday")
        val yday: Int,

        @field:SerializedName("mday")
        val mday: Int,

        @field:SerializedName("weekday_name")
        val weekdayName: String? = null,

        @field:SerializedName("age")
        val age: String? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Features(

        @field:SerializedName("hourly")
        val hourly: Int,

        @field:SerializedName("forecast")
        val forecast: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Feelslike(

        @field:SerializedName("metric")
        val metric: Int,

        @field:SerializedName("english")
        val english: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Forecast(

        @field:SerializedName("simpleforecast")
        val simpleforecast: Simpleforecast? = null,

        @field:SerializedName("txt_forecast")
        val txtForecast: TxtForecast? = null
)

@Generated("com.robohorse.robopojogenerator")
data class ForecastdayItem(

        @field:SerializedName("date")
        val date: Date? = null,

        @field:SerializedName("icon_url")
        val iconUrl: String? = null,

        @field:SerializedName("period")
        val period: Int,

        @field:SerializedName("maxhumidity")
        val maxhumidity: Int,

        @field:SerializedName("skyicon")
        val skyicon: String? = null,

        @field:SerializedName("avewind")
        val avewind: Avewind? = null,

        @field:SerializedName("icon")
        val icon: String? = null,

        @field:SerializedName("avehumidity")
        val avehumidity: Int,

        @field:SerializedName("snow_allday")
        val snowAllday: SnowAllday? = null,

        @field:SerializedName("qpf_day")
        val qpfDay: QpfDay? = null,

        @field:SerializedName("maxwind")
        val maxwind: Maxwind? = null,

        @field:SerializedName("pop")
        val pop: Int,

        @field:SerializedName("qpf_night")
        val qpfNight: QpfNight? = null,

        @field:SerializedName("high")
        val high: High? = null,

        @field:SerializedName("minhumidity")
        val minhumidity: Int,

        @field:SerializedName("low")
        val low: Low? = null,

        @field:SerializedName("snow_night")
        val snowNight: SnowNight? = null,

        @field:SerializedName("snow_day")
        val snowDay: SnowDay? = null,

        @field:SerializedName("conditions")
        val conditions: String? = null,

        @field:SerializedName("qpf_allday")
        val qpfAllday: QpfAllday? = null,

        @field:SerializedName("title")
        val title: String? = null,

        @field:SerializedName("fcttext")
        val fctText: String? = null,

        @field:SerializedName("fcttext_metric")
        val fctTextMetric: String? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Heatindex(

        @field:SerializedName("metric")
        val metric: String? = null,

        @field:SerializedName("english")
        val english: String? = null
)

@Generated("com.robohorse.robopojogenerator")
data class High(

        @field:SerializedName("celsius")
        val celsius: Int,

        @field:SerializedName("fahrenheit")
        val fahrenheit: Int
)

@Generated("com.robohorse.robopojogenerator")
data class HourlyForecastItem(

        @field:SerializedName("icon_url")
        val iconUrl: String? = null,

        @field:SerializedName("sky")
        val sky: String? = null,

        @field:SerializedName("wdir")
        val wdir: Wdir? = null,

        @field:SerializedName("wx")
        val wx: String? = null,

        @field:SerializedName("temp")
        val temp: Temp? = null,

        @field:SerializedName("dewpoint")
        val dewpoint: Dewpoint? = null,

        @field:SerializedName("feelslike")
        val feelslike: Feelslike? = null,

        @field:SerializedName("qpf")
        val qpf: Qpf? = null,

        @field:SerializedName("wspd")
        val wspd: Wspd? = null,

        @field:SerializedName("icon")
        val icon: String? = null,

        @field:SerializedName("uvi")
        val uvi: String? = null,

        @field:SerializedName("FCTTIME")
        val fCTTIME: FCTTIME? = null,

        @field:SerializedName("heatindex")
        val heatindex: Heatindex? = null,

        @field:SerializedName("pop")
        val pop: Int,

        @field:SerializedName("condition")
        val condition: String? = null,

        @field:SerializedName("snow")
        val snow: Snow? = null,

        @field:SerializedName("fctcode")
        val fctcode: String? = null,

        @field:SerializedName("humidity")
        val humidity: Int,

        @field:SerializedName("mslp")
        val mslp: Mslp? = null,

        @field:SerializedName("windchill")
        val windchill: Windchill? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Low(

        @field:SerializedName("celsius")
        val celsius: Int,

        @field:SerializedName("fahrenheit")
        val fahrenheit: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Maxwind(

        @field:SerializedName("kph")
        val kph: Int,

        @field:SerializedName("mph")
        val mph: Int,

        @field:SerializedName("dir")
        val dir: String? = null,

        @field:SerializedName("degrees")
        val degrees: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Mslp(

        @field:SerializedName("metric")
        val metric: Double? = null,

        @field:SerializedName("english")
        val english: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Qpf(

        @field:SerializedName("metric")
        val metric: Double? = null,

        @field:SerializedName("english")
        val english: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class QpfAllday(

        @field:SerializedName("mm")
        val mm: Double? = null,

        @field:SerializedName("in")
        val inch: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class QpfDay(

        @field:SerializedName("mm")
        val mm: Double? = null,

        @field:SerializedName("in")
        val inch: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class QpfNight(

        @field:SerializedName("mm")
        val mm: Double? = null,

        @field:SerializedName("in")
        val inch: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Response(

        @field:SerializedName("features")
        val features: Features? = null,

        @field:SerializedName("version")
        val version: String? = null,

        @field:SerializedName("termsofService")
        val termsofService: String? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Simpleforecast(

        @field:SerializedName("forecastday")
        val forecastday: List<ForecastdayItem?>? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Snow(

        @field:SerializedName("metric")
        val metric: Double? = null,

        @field:SerializedName("english")
        val english: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class SnowAllday(

        @field:SerializedName("in")
        val inch: Double? = null,

        @field:SerializedName("cm")
        val cm: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class SnowDay(

        @field:SerializedName("in")
        val inch: Double? = null,

        @field:SerializedName("cm")
        val cm: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class SnowNight(

        @field:SerializedName("in")
        val inch: Double? = null,

        @field:SerializedName("cm")
        val cm: Double? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Temp(

        @field:SerializedName("metric")
        val metric: Int,

        @field:SerializedName("english")
        val english: Int
)

@Generated("com.robohorse.robopojogenerator")
data class TxtForecast(

        @field:SerializedName("date")
        val date: String? = null,

        @field:SerializedName("forecastday")
        val forecastday: List<ForecastdayItem?>? = null
)

@Generated("com.robohorse.robopojogenerator")
data class Wdir(

        @field:SerializedName("dir")
        val dir: String? = null,

        @field:SerializedName("degrees")
        val degrees: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Windchill(

        @field:SerializedName("metric")
        val metric: Int,

        @field:SerializedName("english")
        val english: Int
)

@Generated("com.robohorse.robopojogenerator")
data class Wspd(

        @field:SerializedName("metric")
        val metric: Int,

        @field:SerializedName("english")
        val english: Int
)