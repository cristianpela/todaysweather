package home.crsk.todaysweather.domain.common.presentation

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.SimpleEchoUseCase
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import util.BaseTest
import java.lang.Thread.sleep

class PresentationTest : BaseTest() {

    @Test
    fun `should keep the state when resubscribe`() {

        val presentation = MockPresentation(SystemSchedulers())

       // val test = presentation.observeState().test()


        presentation.bindIntents(Observable.just(Intent.RetryIntent))

        presentation.observeState().subscribe{println(it)}


//        test.assertValueAt(1, MockState("Hello World"))
//                .assertValueAt(0, MockState())
//
//        presentation.observeState().test()
//                .assertValueAt(0, MockState("Hello World"))

        sleep(5000)


    }
}

data class MockState(val state: String = ""): State()

class MockPresentation(schedulers: SchedulersContract) : Presentation<MockState>(schedulers) {

    val useCase = SimpleEchoUseCase { Response.Stateful.Data("Hello World") }

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    Intent.RetryIntent -> Request.RetryRequest
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<MockState, Response.Stateful, MockState> =
            BiFunction { _, response ->
                MockState((response as Response.Stateful.Data<*>).data as String)
            }

    override fun defaultState(): MockState = MockState()

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>>
            = listOf(dispatchRequest(requestObservable, listOf(Request.RetryRequest::class.java), useCase))

}


class SystemSchedulers : SchedulersContract {
    override fun main(): Scheduler = Schedulers.newThread()

    override fun io(): Scheduler = Schedulers.io()

    override fun computation(): Scheduler = Schedulers.computation()
}