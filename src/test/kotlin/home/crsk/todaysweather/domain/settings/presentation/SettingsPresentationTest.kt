package home.crsk.todaysweather.domain.settings.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.DisplayableSettings
import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.settings.SettingsState
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.BehaviorSubject
import org.junit.Test
import util.BaseTest
import util.neverUseCase
import kotlin.test.assertEquals

class SettingsPresentationTest : BaseTest() {

    private val lang = Language("iso", "wu", "lang", 1)

    private val publisher = BehaviorSubject
            .createDefault<Response.Stateful.DisplayableSettingsResponse>(createResponse(language = lang))

    private val settingsWatchCurrentUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer { up ->
            up.flatMap { publisher }
        }
    }

    private val unit = UnitEnum.METRIC

    private val location = Location.bare("foo")

    private val settingsSaveUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer { up ->
            up.flatMap { req ->
                Response.Stateful.Completed.observable().doOnNext {
                    val prev = publisher.value.displayableSettings
                    publisher.onNext(
                            when (req) {
                                is Request.SaveRequest.SaveLocationRequest -> createResponse(req.location, prev.language, prev.unit)
                                is Request.SaveRequest.SaveUnitRequest -> createResponse(prev.location, prev.language, req.unit)
                                else -> publisher.value
                            }
                    )
                }
            }
        }
    }

    private fun createResponse(location: Location = Location(),
                               language: Language = Language("initISO", "initISO", "initName", 1),
                               unit: UnitEnum = UnitEnum.METRIC): Response.Stateful.DisplayableSettingsResponse =
            Response.Stateful.DisplayableSettingsResponse(
                    DisplayableSettings(location, language, unit))

    //todo right now, settings presentation is not used in prod
    @Test
    fun `should change state for each action`() {

        val presentation = SettingsPresentation(
                settingsWatchCurrentUseCase,
                settingsSaveUseCase,
                neverUseCase(),
                schedulers)

        val test = presentation.observeState().test()

        presentation.bindIntents(
                Observable.merge(
                        Intent.SettingsWatchCurrentIntent.observable(),
                        Intent.UnitChangeIntent(UnitEnum.ENGLISH).observable(),
                        Intent.SelectLocationIntent("foo").observable())
        )

        assertEquals(
                listOf(
                        SettingsState(),
                        SettingsState(displayableSettings = DisplayableSettings(Location(), lang, unit)),
                        SettingsState(displayableSettings = DisplayableSettings(Location(), lang, UnitEnum.ENGLISH)),
                        SettingsState(displayableSettings = DisplayableSettings(location, lang, UnitEnum.ENGLISH))
                ),
                test.values())
    }
}