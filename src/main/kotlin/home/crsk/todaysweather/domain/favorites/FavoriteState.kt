package home.crsk.todaysweather.domain.favorites

import home.crsk.todaysweather.domain.common.presentation.State
import home.crsk.todaysweather.domain.favorites.presentation.DisplayableFavorite

data class FavoriteState(val favorites: List<DisplayableFavorite> = emptyList(),
                         val hasUndoHistory: Boolean = false): State()