package home.crsk.todaysweather.domain.common.other

import home.crsk.todaysweather.domain.common.log.nl

fun String.toSnakeCase(): String {
    val builder = StringBuilder()
    for (i in 0..this.lastIndex) {
        var c = this[i]
        if (c.isUpperCase()) {
            if (i in 1..(lastIndex - 1) && this[i + 1].isLowerCase() && this[i - 1] != '_')
                builder.append("_")
            c = c.toLowerCase()
        }
        builder.append(c.toLowerCase())
    }
    return builder.toString()
}

fun Number.asPercent(): String =
        if (isNullLike()) "?%"
        else
            when (this) {
                is Float,
                is Double -> String.format("%.2f%", this)
                else -> "$this%"
            }

fun Number.isNullLike() =
        when (this) {
            is Float -> this == Float.MAX_VALUE
            is Int -> this == Int.MAX_VALUE
            is Long -> this == Long.MAX_VALUE
            is Double -> this == Double.MAX_VALUE
            else -> false
        }

fun Number.toStringOrNullLike(): String =
        when {
            this is Float && this == Float.MAX_VALUE -> "?"
            this is Int && this == Int.MAX_VALUE -> "?"
            this is Long && this == Long.MAX_VALUE -> "?"
            this is Double && this == Double.MAX_VALUE -> "?"
            else -> toString()
        }


fun <T> Collection<T>.toPrettyString(): String {
    val builder = StringBuilder()
    this.forEach { builder.append(it).append(nl()) }
    return builder.toString()
}

inline fun <reified T> T.simpleName(): String = T::class.simpleName!!

open class TranslateableError(val wrapped: Throwable,
                              val transId: String? = null,
                              val params: Array<Any> = emptyArray()) : Error()
