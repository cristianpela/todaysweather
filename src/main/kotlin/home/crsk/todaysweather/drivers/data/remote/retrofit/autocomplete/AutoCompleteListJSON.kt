package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import com.google.gson.annotations.SerializedName

data class AutoCompleteListJSON(@SerializedName("RESULTS") val results: List<AutoCompleteJSON>)