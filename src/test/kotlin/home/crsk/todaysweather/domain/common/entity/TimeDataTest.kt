package home.crsk.todaysweather.domain.common.entity

import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.now
import io.reactivex.subjects.BehaviorSubject
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class TimeDataTest{


    @Test
    fun `should get time data from pieces`(){
        val td = TimeData
                .fromPieces(2018, 2, 5,
                        19, 0, 0)
        assertEquals(2018, td.year)
        assertEquals(2, td.month)
        assertEquals(5, td.day)
        assertEquals(19, td.hour)
        assertEquals(0, td.min)
        assertEquals(0, td.second)
    }

    @Test
    fun `testing subject`(){
        val subject = BehaviorSubject.create<Int>()
        subject.onNext(1)
        subject.subscribe().dispose()
        subject.onNext(2)
        subject.test().assertValue(2)
    }

    @Test
    fun `testing format`(){
        val savedLocale = Locale.getDefault()
        Locale.setDefault(Locale("ro"))
        val timeData = TimeData.fromPieces(2018, 2, 8, 13, 30)
        assertEquals("08 Feb.",
                timeData.display("dd MMM."))
        Loggy.d(timeData.display("dd.MM.yyyy HH:mm"))
        Locale.setDefault(savedLocale)
    }
}