package home.crsk.todaysweather.domain.common.gateway.data.settings

import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

interface SettingsService {
    /**
     * Checks if there is a setup. By that, it means location, language, and unit are set.</br>
     * Throw a NoSetupError otherwise
     */
    fun check(): Completable

    fun current(): Single<Settings>

    fun save(settings: Settings): Completable

    fun observe(): Observable<Settings>

}

