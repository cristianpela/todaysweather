package home.crsk.todaysweather.domain.setup.usecases

import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer

class LanguageSetupUseCase(private val languageProvider: LanguageProvider): UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer {
        up -> up.cast(Request.LanguageSetupRequest::class.java)
            .flatMap{languageProvider.availableLanguages()
                    .map{ Response.Stateful.LanguagesResponse(it) }
                    .toObservable()}
    }
}