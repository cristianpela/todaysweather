package home.crsk.todaysweather.domain.favorites

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.*
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Test
import util.BaseTest
import util.assertEq

class UndoFavoriteManagerImplTest : BaseTest() {

    val repository = mock<FavoriteRepository> {
        on { setFavorite(any()) } doReturn Completable.complete()
    }

    val undoManager = UndoFavoriteManagerImpl(repository)

    @Test
    fun `execute adding and undo adding favorites`() {

        Observable.fromArray(
                Add(Location(city = "foo", locationKey = "foo", favorite = true)),
                Add(Location(city = "bar", locationKey = "bar", favorite = true)),
                Undo,
                Undo,
                Undo,
                Pass(Location(city = "zar", locationKey = "zar", favorite = true))
        ).compose(undoManager.execute()).test().values()
                .assertEq(listOf(
                        Info(true, 1,"[foo]" ),
                        Info(true, 2,"[bar, foo]" ),
                        Info(true, 1,"[foo]" ),
                        Info(false, 0,"[]" ),
                        Info(false, 0,"[]" ),
                        Info(false, 0,"[]" )
                ))


        val captor = argumentCaptor<Location>()
        verify(repository, times(5)).setFavorite(captor.capture())

        captor.allValues.assertEq(listOf(
                Location(city = "foo", locationKey = "foo", favorite = true),
                Location(city = "bar", locationKey = "bar", favorite = true),
                Location(city = "bar", locationKey = "bar", favorite = false),
                Location(city = "foo", locationKey = "foo", favorite = false),
                Location(city = "zar", locationKey = "zar", favorite = true)))
    }

    @Test
    fun `execute clear`() {

        Observable.fromArray(
                Add(Location(city = "foo", locationKey = "foo", favorite = true)),
                Add(Location(city = "bar", locationKey = "bar", favorite = true)),
                Clear
        ).compose(undoManager.execute()).test().values()
                .assertEq(listOf(
                        Info(true, 1,"[foo]" ),
                        Info(true, 2,"[bar, foo]" ),
                        Info(false, 0,"[]" )
                ))


        val captor = argumentCaptor<Location>()
        verify(repository, times(2)).setFavorite(captor.capture())

        captor.allValues.assertEq(listOf(
                Location(city = "foo", locationKey = "foo", favorite = true),
                Location(city = "bar", locationKey = "bar", favorite = true)))

    }
}