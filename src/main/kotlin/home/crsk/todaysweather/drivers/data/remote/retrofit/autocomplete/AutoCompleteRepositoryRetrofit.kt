package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import io.reactivex.Maybe
import io.reactivex.Single

class AutoCompleteRepositoryRetrofit(private val autoCompleteRemoteService: AutoCompleteRemoteService) : AutoCompleteRepository {

    override fun find(partialSearch: String): Maybe<List<Location>> =
            autoCompleteRemoteService.find(partialSearch)
                    .filter { !it.results.isEmpty() }
                    .map {
                        it.results.filter { it.type == "city" }.map { json ->
                            Location(json.name, json.c, json.zmw, json.lat, json.lon)
                        }
                    }

    override fun findExact(city: String, country: String): Maybe<List<Location>> =
            autoCompleteRemoteService.findExact(city, country)
                    .filter { !it.results.isEmpty() }
                    .map {
                        it.results.filter { it.type == "city" }.map { json ->
                            Location(json.name, json.c, json.zmw, json.lat, json.lon)
                        }
                    }


    override fun findByKey(key: String): Maybe<Location> = throw UnsupportedOperationException()

    override fun save(locations: List<Location>): Single<List<Location>> = throw UnsupportedOperationException()
}