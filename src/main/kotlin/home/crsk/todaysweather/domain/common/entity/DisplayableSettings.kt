package home.crsk.todaysweather.domain.common.entity

data class DisplayableSettings(val location: Location, val language: Language, val unit: UnitEnum)