package home.crsk.todaysweather.domain.common.gateway

import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_ISO_CODES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_NAMES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_WU_CODES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.LANGUAGE_FLAG
import home.crsk.todaysweather.domain.common.other.toSnakeCase
import io.reactivex.Single
import java.util.*
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class LanguageProviderImpl(private val resourceFinder: ResourceFinder) : LanguageProvider {

    private fun languages(): List<Language> {
        val arrISO: Array<String> = resourceFinder.findArray(ARRAY_LANG_ISO_CODES_KEY)
        val arrWU: Array<String> = resourceFinder.findArray(ARRAY_LANG_WU_CODES_KEY)
        val arrNames: Array<String> = resourceFinder.findArray(ARRAY_LANG_NAMES_KEY)
        val defaultISO = Locale.getDefault().language

        assert(arrISO.size == arrWU.size && arrWU.size == arrNames.size)
        return generateSequence(0) { it + 1 }
                .map { Language(
                        arrISO[it],
                        arrWU[it],
                        arrNames[it],
                        resourceFinder.findDrawableId("${LANGUAGE_FLAG}_${arrISO[it]}"),
                        arrISO[it].equals(defaultISO, true))
                }.take(arrISO.size).toList()
    }

    override fun availableLanguages(): Single<List<Language>> =
            Single.defer { Single.just(languages()) }

    override fun findLanguageByISO(isoCode: String): Single<Language> =
            Single.defer {
                // careful, should not be null!
                Single.just(languages().firstOrNull { it.isoCode == isoCode })
            }

    override fun translate(langKeyName: String, vararg params: Any): String =
            resourceFinder.findString(langKeyName, *params)

    override fun translator(vararg params: Any): ReadOnlyProperty<Nothing?, String> = object : ReadOnlyProperty<Nothing?, String> {
        override fun getValue(thisRef: Nothing?, property: KProperty<*>): String =
                this@LanguageProviderImpl.translate(property.name.toSnakeCase(), *params)
    }
}


