package home.crsk.todaysweather.domain.common.gateway.data.forecast

import home.crsk.todaysweather.domain.common.entity.Forecast
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single

interface ForecastRepository {

    fun saveBatch(locationKey: String, forecast: Forecast): Single<Forecast>

    fun getBatch(locationKey: String, unit: UnitEnum): Maybe<Forecast>

    fun getLastUpdate(locationKey: String): Single<Long>

    fun clear(locationKey: String): Completable

}