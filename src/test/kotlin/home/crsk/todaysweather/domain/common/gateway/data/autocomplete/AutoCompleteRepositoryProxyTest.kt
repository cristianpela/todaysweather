package home.crsk.todaysweather.domain.common.gateway.data.autocomplete

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Location
import io.reactivex.Maybe
import io.reactivex.Single
import org.junit.Test
import org.mockito.ArgumentMatchers.anyList
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest

class AutoCompleteRepositoryProxyTest : BaseTest() {

    val location = Location()
    val locations: List<Location> = listOf(location)

    val search: SearchRepository = mock {}

    @Test
    fun `given search should fetch results from local if search occurred before`() {
        val local: AutoCompleteRepository = mock {
            on { find(anyString()) } doReturn Maybe.just(locations)
        }

        val remote: AutoCompleteRepository = mock {
            on { find(anyString()) } doReturn Maybe.empty()
        }

        val search: SearchRepository = mock {
            on { find(anyString()) } doReturn Maybe.just("foo")
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.find("foo").test()

        test.assertValueAt(0, locations)

        verify(local).find(anyString())
        verify(remote, never()).find(anyString())

    }

    @Test
    fun `given search should fetch results from remote if search never occurred before`() {
        val local: AutoCompleteRepository = mock {
            on { save(anyList()) } doReturn Single.just(locations)
            on { find(anyString()) } doReturn Maybe.empty()
        }

        val remote: AutoCompleteRepository = mock {
            on { find(anyString()) } doReturn Maybe.just(locations)
        }

        val search: SearchRepository = mock {
            on { find(anyString()) } doReturn Maybe.just("foo")
            on { save(anyString()) } doReturn Single.just("foo")
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.find("foo").test()

        test.assertValueAt(0, locations)

        verify(remote).find(anyString())
        verify(local).find(anyString())
        verify(local).save(anyList())
        verify(search).save(anyString())

    }

    @Test
    fun `given search should return empty list when not found`() {

        val local: AutoCompleteRepository = mock {
            on { find(anyString()) } doReturn Maybe.empty()
        }

        val remote: AutoCompleteRepository = mock {
            on { find(anyString()) } doReturn Maybe.empty<List<Location>>()
        }

        val search: SearchRepository = mock {
            on { find(anyString()) } doReturn Maybe.empty()
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.find("foo").test()

        test.assertValue(emptyList())

    }

    @Test
    fun `given exact search should return from local`() {

        val local: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.just(listOf(location))
        }

        val remote: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.empty()
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.findExact("foo", "bar").test()

        test.assertValueAt(0, listOf(location))

        verify(remote, never()).findExact(anyString(), anyString())

    }

    @Test
    fun `given exact search should return from remote if is not saved locally and then save it to local`() {

        val local: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.empty()
            on { save(anyList())} doReturn Single.just(locations)
        }

        val remote: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.just(listOf(location))
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.findExact("foo", "bar").test()

        test.assertValueAt(0, listOf(location))

        verify(local).findExact(anyString(), anyString())
        verify(remote).findExact(anyString(), anyString())
        verify(local).save(eq(locations))

    }

    @Test
    fun `given exact search should return error if result is empty from remote`() {
        val local: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.empty()
        }

        val remote: AutoCompleteRepository = mock {
            on { findExact(anyString(), anyString()) } doReturn Maybe.empty()
        }

        val proxyRepo = AutoCompleteRepositoryProxy(local, remote, search, schedulers)

        val test = proxyRepo.findExact("foo", "bar").test()

        test.assertError(AutoCompleteNotFoundError)
    }

}