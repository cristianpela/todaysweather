package home.crsk.todaysweather.domain.main

import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screen
import home.crsk.todaysweather.domain.common.gateway.Screens
import home.crsk.todaysweather.domain.common.presentation.State

@Suppress("unused")
data class NavigationState(val route: Route = Route.NONE): State()