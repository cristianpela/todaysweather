package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import home.crsk.todaysweather.domain.common.entity.Forecast
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRemoteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.drivers.data.remote.retrofit.handleHttpException
import io.reactivex.Observable

class ForecastRemoteRepositoryRetrofit(private val apiKey: String,
                                       private val temperatureRemoteService: ForecastRemoteService,
                                       private val lastUpdate: Long? = System.currentTimeMillis()) : ForecastRemoteRepository {

    override fun getBatch(locationKey: String, langCode: String): Observable<Forecast> =
            temperatureRemoteService.getBatch(apiKey, locationKey, langCode.toUpperCase())
                    .map {
                        Loggy.d("ForecastRemoteRepositoryRetrofit: Getting Forecast from Wunderground server for locationKey $locationKey")
                        it.toForecastEntity(lastUpdate ?: System.currentTimeMillis())
                    }
                    .compose(handleHttpException(::forecastErrorHandler))
}