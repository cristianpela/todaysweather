package home.crsk.todaysweather.domain.common.gateway

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.other.TranslateableError
import io.reactivex.Observable
import io.reactivex.Single

interface LocationDetector {
    fun detect(useLastKnownLocation: Boolean): Observable<Location>
}

class LocationNotDetectedError(wrapped: Error,
                               transId: String? = null,
                               params: Array<Any> = emptyArray<Any>()) : TranslateableError(wrapped, transId, params)
