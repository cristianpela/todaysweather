package home.crsk.todaysweather.domain.common.usecases.autocomplete

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Maybe
import io.reactivex.subjects.PublishSubject
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

class AutoCompleteUseCaseTest : BaseTest() {
    @Test
    fun `should ensure input is valid`() {

        val testScheduler = schedulers.computationPool[0]

        val autoCompleteRepository = mock<AutoCompleteRepository> {
            on { find(anyString()) } doReturn Maybe.just(listOf(Location()))
        }

        val useCase = AutoCompleteUseCase(autoCompleteRepository)

        val input = PublishSubject.create<Request.AutoCompleteSearchLocationRequest>()

        val test = input.compose(useCase.execute())
                .test()

        //first input - VALID
        input.onNext(Request.AutoCompleteSearchLocationRequest("abc", ""))
        testScheduler.advanceTimeBy(AutoCompleteUseCase.INPUT_DELAY_MILLIS, TimeUnit.MILLISECONDS)
        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, Response.Stateful.AutoCompleteResponse(listOf(Location()),"abc",""))

        //second input - INVALID
        input.onNext(Request.AutoCompleteSearchLocationRequest("a", ""))
        testScheduler.advanceTimeBy(AutoCompleteUseCase.INPUT_DELAY_MILLIS, TimeUnit.MILLISECONDS)
        test.assertValueAt(2, Response.Stateful.InFlight)
        test.assertValueAt(3, Response.Stateful.Completed)
        test.assertValueAt(4, { (it as Response.Error).err is AutoCompleteSilentValidationError })

        //third input - VALID
        input.onNext(Request.AutoCompleteSearchLocationRequest("foo", ""))
        testScheduler.advanceTimeBy(AutoCompleteUseCase.INPUT_DELAY_MILLIS, TimeUnit.MILLISECONDS)
        test.assertValueAt(5, Response.Stateful.InFlight)
        test.assertValueAt(6, Response.Stateful.AutoCompleteResponse(listOf(Location()), "foo", ""))

        //cancel request
        input.onNext(Request.AutoCompleteSearchLocationRequest.CANCEL)
        testScheduler.advanceTimeBy(AutoCompleteUseCase.INPUT_DELAY_MILLIS, TimeUnit.MILLISECONDS)
        test.assertValueAt(7, Response.Stateful.Completed)

        //valid calls verifier
        val captor = argumentCaptor<String>()
        verify(autoCompleteRepository, times(2)).find(captor.capture())
        assertEquals(listOf("abc", "foo"), captor.allValues)


    }

}