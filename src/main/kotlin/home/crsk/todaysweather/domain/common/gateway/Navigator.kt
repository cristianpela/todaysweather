package home.crsk.todaysweather.domain.common.gateway

import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.CopyOnWriteArrayList

interface Navigator {

    fun observe(): Observable<Route>

    fun navigateTo(route: Route)

    fun addInterceptor(interceptor: NavigationInterceptor)

    fun removeInterceptor(interceptor: NavigationInterceptor)
}

typealias Screen = kotlin.String

object Screens {
    val SCREEN_MANUAL_SETUP: Screen = "manual"
    val SCREEN_AUTOMATIC_SETUP: Screen = "automatic"
    val SCREEN_LANGUAGE_SETUP: Screen = "language"
    val SCREEN_CHOOSE_SETUP_TYPE: Screen = "setup-choose"
    val SCREEN_SETTINGS: Screen = "settings"
    val SCREEN_FAVORITES: Screen = "favorites"
    val SCREEN_APP: Screen = "app"
    val SCREEN_NONE = ""
    val SCREEN_BACK = "back"
    val SCREEN_SEARCH = "search"
}

data class Route(val from: Screen, val to: Screen, val extras: Map<String, Any>? = null,
                 val addToBackStack: Boolean = false, val transitionInfo: TransitionInfo? = null) {
    companion object {
        val NONE = Route(Screens.SCREEN_NONE, Screens.SCREEN_NONE)
        fun justExtras(extras: Map<String, Any>?): Route = Route(Screens.SCREEN_NONE, Screens.SCREEN_NONE, extras)
        fun back(extras: Map<String, Any>?): Route = Route(Screens.SCREEN_BACK, Screens.SCREEN_BACK, extras)
    }

    operator fun invoke() = to

    fun reverse(): Route = Route(to, from, extras, addToBackStack, transitionInfo)

    data class TransitionInfo(var nameId: Int? = null,
                              var commonViewId: Int? = null,
                              var sharedElementEnterTransitionId: Int? = null,
                              var fromExitTransitionId: Int? = null,
                              var toEnterTransitionId: Int? = null) {

        companion object {
            fun withoutSharedElement(fromExitTransitionId: Int, toEnterTransitionId: Int) =
                    TransitionInfo(null,
                            null,
                            null,
                            fromExitTransitionId,
                            toEnterTransitionId)
        }

        fun isWithoutSharedElement(): Boolean = nameId == null
    }
}

class NavigatorImpl(private val settingsService: SettingsService) : Navigator {

    private val dispatcher: Subject<Route> = PublishSubject.create<Route>().toSerialized()

    companion object {
        private const val KEY_CURRENT_SETTINGS = "KEY_CURRENT_SETTINGS"
    }

    private val interceptors = CopyOnWriteArrayList<NavigationInterceptor>()
            .apply {
                add(object : NavigationInterceptor { // internal interceptor
                    override fun onNavigationIntercept(navChain: NavChain): Route {
                        return if (navChain.route.extras != null
                                && navChain.route.extras[KEY_CURRENT_SETTINGS] != null) {
                            val newScreen = with(navChain.route.extras[KEY_CURRENT_SETTINGS] as Settings) {
                                if (langCodeISO == null && locationKey == null) {
                                    Screens.SCREEN_LANGUAGE_SETUP
                                } else if (langCodeISO != null && locationKey == null) {
                                    Screens.SCREEN_CHOOSE_SETUP_TYPE
                                } else {
                                    Screens.SCREEN_APP
                                }
                            }
                            navChain.proceed(Route(Screens.SCREEN_NONE, newScreen))
                        } else {
                            navChain.proceed(navChain.route)
                        }
                    }
                })
            }

    override fun observe(): Observable<Route> = settingsService
            .observe()
            .map { Route.justExtras(mapOf(KEY_CURRENT_SETTINGS to it)) }
            .mergeWith(dispatcher.serialize())
            .map { processNavigation(it) }

    override fun navigateTo(route: Route) {
        dispatcher.onNext(route)
    }

    private fun processNavigation(route: Route): Route {
        assert(interceptors.size >= 1, { "Must have at least one interceptor" })
        val iterator = interceptors.iterator()
        return iterator.next().onNavigationIntercept(NavChain(route, iterator))
    }

    override fun addInterceptor(interceptor: NavigationInterceptor) {
        interceptors.add(interceptor)
    }

    override fun removeInterceptor(interceptor: NavigationInterceptor) {
        if (interceptors.size > 1) // guard to not allow removing the internal interceptor by accident
            interceptors.remove(interceptor)
    }
}


interface NavigationInterceptor {

    fun onNavigationIntercept(navChain: NavChain): Route

}

class NavChain(val route: Route,
               private val interceptorsIterator: Iterator<NavigationInterceptor>) {

    fun proceed(route: Route = this.route): Route {
        return if (interceptorsIterator.hasNext())
            interceptorsIterator
                    .next()
                    .onNavigationIntercept(NavChain(route, interceptorsIterator))
        else route
    }
}