package home.crsk.todaysweather.domain.common.usecases

import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screen
import java.util.*

sealed class Request {
    object NoRequest : Request()
    object RetryRequest : Request()
    object ResetRequest : Request()

    object LanguageSetupRequest : Request()
    class AutomaticSetupRequest(val useLastKnownLocation: Boolean) : Request()
    class RetryAutomaticSetupRequest(val useLastKnownLocation: Boolean) : Request()

    class NavigationWatchRequest(val cancel: Boolean = false) : Request()
    class NavigationToRequest(val route: Route) : Request()

    class AutoCompleteSelectLocationRequest(val locationKey: String, val owner: String) : Request()
    class AutoCompleteSearchLocationRequest(val partialSearch: String, val owner: String?) : Request() {
        companion object {
            val CANCEL = AutoCompleteSearchLocationRequest(UUID.randomUUID().toString(), null)
        }
    }

    sealed class SaveRequest : Request() {
        class SaveLocationRequest(val location: Location) : SaveRequest()
        class SaveLanguageRequest(val langCodeISO: String) : SaveRequest()
        class SaveUnitRequest(val unit: UnitEnum) : SaveRequest()
        class SaveAllRequest(val settings: Settings) : SaveRequest()
    }

    class SelectLocationRequest(val locationKey: String? = null) : Request()

    class ForecastWatchRequest(val cancel: Boolean = false) : Request()

    class FavoriteWatchRequest(val cancel: Boolean = false) : Request()
    object FavoritesFetchRequest : Request()
    class FavoriteChangeRequest(val location: Location?, val undoable: Boolean = false) : Request()


    data class Data<out T>(val data: T) : Request()

    class SettingsWatchCurrentRequest(val cancel: Boolean = false) : Request()


    class CompositeRequest(vararg val requests: Request) : Request()

}

sealed class Response {

    sealed class Stateful : Response() {
        data class Data<out T>(val data: T) : Stateful()
        object Completed : Stateful()
        object InFlight : Stateful()
        object Reset : Stateful()
        data class SelectedLocationResponse(val location: Location) : Stateful()
        data class AutoCompleteSelectLocationResponse(val locationKey: String, val owner: String) : Stateful()
        data class AutoCompleteResponse(val locations: List<Location>, val partialSearch: String, val owner: String?) : Stateful()
        data class ForecastResponse(val location: Location, val forecast: Forecast) : Stateful()
        data class FavoriteChangeResponse(val favorite: Favorite) : Stateful()
        data class UndoableFavoriteChangeResponse(val hasUndoHistory: Boolean) : Stateful()
        data class FavoritesFetchResponse(val favorites: List<Favorite>) : Stateful()
        data class LanguagesResponse(val languages: List<Language>) : Stateful()
        data class DisplayableSettingsResponse(val displayableSettings: DisplayableSettings) : Stateful()
        data class Setup(val settings: Settings) : Stateful()
        data class NavigationResponse(val route: Route) : Stateful()
    }

    class Message(val message: CommonMessage) : Response()

    class Error(val err: kotlin.Throwable) : Response()
}


sealed class PresentationResponse {

    class Message(val message: CommonMessage) : PresentationResponse()

    class Error(val err: kotlin.Throwable) : PresentationResponse()

}

sealed class CommonMessage {
    data class Data<out T>(val data: T) : CommonMessage()
    data class AutoComplete(val locations: List<Location>) : CommonMessage()
    data class AutoCompleteSelectLocationMessage(val locationKey: String, val owner: String) : CommonMessage()
    data class Alert(val message: String) : CommonMessage()
}

fun CommonMessage.toResponse(): Response.Message = Response.Message(this)

fun CommonMessage.toPresentationResponse(): PresentationResponse.Message = PresentationResponse.Message(this)

fun Throwable.toResponse(): Response.Error = Response.Error(this)

fun Throwable.toPresentationResponse(): PresentationResponse.Error = PresentationResponse.Error(this)