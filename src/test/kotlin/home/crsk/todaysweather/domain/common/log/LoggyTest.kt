package home.crsk.todaysweather.domain.common.log

import org.junit.Test

class LoggyTest{


    @Test
    fun `loggy test`(){
        Loggy.d("%s","World")
        Loggy.d("%.3f",3.33434f)
        Loggy.d(Error(), "%.3f",3.33434f)
        Loggy.d(Error(), "Clean error")
        Loggy.d(Error(), "Clean error %.3f")
        Loggy.d("Clean error %.3f")
    }
}