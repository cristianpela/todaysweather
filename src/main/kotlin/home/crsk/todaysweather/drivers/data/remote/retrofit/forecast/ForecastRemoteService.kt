package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ForecastRemoteService {


    //http://api.wunderground.com/api/d264ecfa3d7fb592/lang:ro/forecast/hourly/q/zmw:00000.74.15247.json

    @GET("/api/{apiKey}/lang:{lang}/forecast/hourly/q/zmw:{locationKey}.json")
    fun getBatch(@Path("apiKey") apiKey: String, @Path("locationKey") locationZmv: String,
                 @Path("lang") langCode: String = "US"): Observable<FullForecast>

}
