package home.crsk.todaysweather.domain.common.entity

enum class UnitEnum(val unit: String, val degreeSymbol: String = "?",
                    val degreeName: String = "?", val speedMeasure: String = "?",
                    val rainMeasure: String = "?", val snowMeasure: String = "?") {
    METRIC("metric", "\u00b0C", "Celsius",
            "km/h", "mm", "cm"),
    ENGLISH("english", "\u00b0F", "Fahrenheit",
            "m/h", "in", "in"),
    BOTH("both", "degreeSymbol", "degreeName", "speedMeasure", "rainMeasure", "snowMeasure");

    fun other(): UnitEnum =  when(this){
        ENGLISH -> METRIC
        METRIC ->ENGLISH
        else -> this
    }

}