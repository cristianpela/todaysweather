package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import com.google.gson.Gson
import org.junit.Test
import kotlin.test.assertEquals

class AutoCompleteListJSONTest {

    @Test
    fun `should convert to pojo`() {
        val json = """{
            RESULTS: [{
                name: "Timisoara, Romania",
                type: "city",
                c: "RO",
                zmw: "00000.74.15247",
                tz: "Europe/Bucharest",
                tzs: "EET",
                l: "/q/locationKey:00000.74.15247",
                ll: "45.759998 21.230000",
                lat: "45.759998",
                lon: "21.230000"
            }]
        }"""

        val list = Gson().fromJson(json, AutoCompleteListJSON::class.java)

        assertEquals(autoCompleteListJSON, list)

    }

    @Test
    fun `pojo completion test`() {
        val json = """{city: "TM", name: "Foo", age: "10", passions:"Many"}"""
        val conv = Gson().fromJson(json, Pojo::class.java)
        assertEquals(Pojo("Foo", 10), conv )

    }

    data class Pojo(val name: String, val age: Int)

}

val autoCompleteListJSON = AutoCompleteListJSON(listOf(AutoCompleteJSON(
        "Timisoara, Romania",
        "city",
        "RO",
        "00000.74.15247",
        "Europe/Bucharest",
        "EET",
        "/q/locationKey:00000.74.15247",
        "45.759998 21.230000",
        45.759998,
        21.230000
)))