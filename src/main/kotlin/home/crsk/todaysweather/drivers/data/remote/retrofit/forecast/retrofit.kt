package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import home.crsk.todaysweather.domain.common.gateway.data.forecast.InvalidApiKeyError
import home.crsk.todaysweather.domain.common.gateway.data.forecast.QueryNotFoundError
import home.crsk.todaysweather.drivers.data.remote.retrofit.okHttpBaseClientBuilder
import home.crsk.todaysweather.drivers.data.remote.retrofit.retrofitBaseBuilder
import retrofit2.Retrofit

val forecastRetrofit: Retrofit.Builder by lazy {
    retrofitBaseBuilder
            .baseUrl("http://api.wunderground.com/api/")
            .client(okHttpBaseClientBuilder
                    .addInterceptor(ForecastResponseInterceptor())
                    .build())

}

val forecastService: ForecastRemoteService by lazy {
    forecastRetrofit.build().create(ForecastRemoteService::class.java)
}

fun forecastErrorHandler(httpErr: HttpException) : Error? =
        when (httpErr.code()) {
            403 -> InvalidApiKeyError(httpErr.message())
            404 -> QueryNotFoundError(httpErr.message())
            else -> null
        }
