package home.crsk.todaysweather.domain.settings.usecases

import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.common.usecases.toResponseCompleted
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

class SettingsSaveUseCase(private val settingsService: SettingsService,
                          private val languageProvider: LanguageProvider) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.publish {
            //split and send downstream rejoined settings with an optional message of completion
            Observable.merge(
                    it.ofType(Request.SaveRequest.SaveLocationRequest::class.java).flatMap { request ->
                        settingsService.current()
                                .map {
                                    Settings(request.location.locationKey, it.langCodeISO, it.unit) to languageProvider
                                            .translate(LanguageProvider.MSG_SETTINGS_LOCATION_SAVED)
                                }
                                .toObservable()
                    },
                    it.ofType(Request.SaveRequest.SaveLanguageRequest::class.java).flatMap { request ->
                        settingsService.current()
                                .map {
                                    Settings(it.locationKey, request.langCodeISO, it.unit) to languageProvider
                                            .translate(LanguageProvider.MSG_SETTINGS_LANG_SAVED)
                                }
                                .toObservable()
                    },
                    it.ofType(Request.SaveRequest.SaveUnitRequest::class.java).flatMap { request ->
                        settingsService.current()
                                .map {
                                    Settings(it.locationKey, it.langCodeISO, request.unit) to languageProvider
                                            .translate(LanguageProvider.MSG_SETTINGS_UNIT_SAVED)
                                }
                                .toObservable()
                    },
                    it.ofType(Request.SaveRequest.SaveAllRequest::class.java).map { request ->
                        request.settings to languageProvider.translate(LanguageProvider.MSG_SETTINGS_ALL_SAVED)
                    })
                    .switchMap { settingsService.save(it.first).toResponseCompleted(it.second) }
        }
    }
}