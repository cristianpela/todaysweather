package home.crsk.todaysweather.domain.setup

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.presentation.State

data class SetupState(val selectedLocation: Location? = null): State()