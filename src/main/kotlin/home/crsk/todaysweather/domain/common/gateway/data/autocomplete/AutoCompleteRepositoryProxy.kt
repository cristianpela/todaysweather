package home.crsk.todaysweather.domain.common.gateway.data.autocomplete

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.other.errorBackOffFlowable
import home.crsk.todaysweather.domain.common.other.toPrettyString
import io.reactivex.Maybe
import io.reactivex.Single

class AutoCompleteRepositoryProxy(
        private val local: AutoCompleteRepository,
        private val remote: AutoCompleteRepository,
        private val searchRepository: SearchRepository,
        schedulers: SchedulersContract)
    : AutoCompleteRepository {

    private val backOffStrategy = errorBackOffFlowable(
            initialDelay = 3,
            skip = listOf(AutoCompleteNotFoundError::class.java),
            delayScheduler = schedulers.computation()
    )

    override fun findByKey(key: String): Maybe<Location> = local.findByKey(key)

    override fun find(partialSearch: String): Maybe<List<Location>> =
            //try load from local based on the existence of the search key
            searchRepository.find(partialSearch).flatMap {
                local.find(it)
            }.switchIfEmpty(Maybe.defer {
                // else try load from remote
                remote.find(partialSearch).filter { !it.isEmpty() }
                        .flatMap { list ->
                            // if there is least one result save it locally and also save the search key
                            searchRepository.save(partialSearch)
                                    .flatMapMaybe { local.save(list).toMaybe() }
                        }
            }).defaultIfEmpty(emptyList())
                    .retryWhen(backOffStrategy)


    override fun findExact(city: String, country: String): Maybe<List<Location>> =
            local.findExact(city, country)
                    .doOnSuccess { Loggy.d("Proxy: Local results ${it.toPrettyString()}") }
                    .switchIfEmpty(Maybe.defer {
                        remote.findExact(city, country)
                                .doOnSuccess { Loggy.d("Proxy: Remote results ${it.toPrettyString()}") }
                                .flatMap {
                                    local.save(it).toMaybe()
                                }
                    }).switchIfEmpty(Maybe.error(AutoCompleteNotFoundError))
                    .retryWhen(backOffStrategy)


    override fun save(locations: List<Location>): Single<List<Location>> = local.save(locations)


}