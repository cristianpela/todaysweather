@file:Suppress("unused", "UNCHECKED_CAST")

package home.crsk.todaysweather.domain.common.log

import home.crsk.todaysweather.domain.common.other.simpleName

object Loggy {

    const val BASE_TAG = "Loggy"

    var seed: Seed? = DefaultSeed()

    fun tag(tag: String) = seed?.tag(tag)

    fun v(message: String, vararg args: Any) {
        seed?.v(message, *args)
    }

    /** Log a verbose exception.  */
    fun v(t: Throwable) {
        seed?.v1(t)
    }

    /** Log a debug message with optional format args.  */
    fun d(message: String, vararg args: Any) {
        seed?.d(message, *args)
    }

    /** Log a debug exception and a message with optional format args.  */
    fun d(t: Throwable, message: String, vararg args: Any) {
        seed?.d(t, message, *args)
    }

    /** Log a debug exception.  */
    fun d(t: Throwable) {
        seed?.d(t)
    }

    /** Log an info message with optional format args.  */
    fun i(message: String, vararg args: Any) {
        seed?.i(message, *args)
    }

    /** Log an info exception and a message with optional format args.  */
    fun i(t: Throwable, message: String, vararg args: Any) {
        seed?.i(t, message, *args)
    }

    /** Log an info exception.  */
    fun i(t: Throwable) {
        seed?.i(t)
    }

    /** Log a warning message with optional format args.  */
    fun w(message: String, vararg args: Any) {
        seed?.w(message, *args)
    }

    /** Log a warning exception and a message with optional format args.  */
    fun w(t: Throwable, message: String, vararg args: Any) {
        seed?.w(t, message, *args)
    }

    /** Log a warning exception.  */
    fun w(t: Throwable) {
        seed?.w(t)
    }

    /** Log an error message with optional format args.  */
    fun e(message: String, vararg args: Any) {
        seed?.e(message, *args)
    }

    /** Log an error exception and a message with optional format args.  */
    fun e(t: Throwable, message: String, vararg args: Any) {
        seed?.e(t, message, *args)
    }

    /** Log an error exception.  */
    fun e(t: Throwable) {
        seed?.e(t)
    }

    /** Log an assert message with optional format args.  */
    fun wtf(message: String, vararg args: Any) {
        seed?.wtf(message, *args)
    }

    /** Log an assert exception and a message with optional format args.  */
    fun wtf(t: Throwable, message: String, vararg args: Any) {
        seed?.wtf(t, message, *args)
    }

    /** Log an assert exception.  */
    fun wtf(t: Throwable) {
        seed?.wtf(t)
    }

    /** Log at `priority` a message with optional format args.  */
    fun log(priority: Int, message: String, vararg args: Any) {
        seed?.log(priority, message, *args)
    }

    /** Log at `priority` an exception and a message with optional format args.  */
    fun log(priority: Int, t: Throwable, message: String, vararg args: Any) {
        seed?.log(priority, t, message, *args)
    }

    /** Log at `priority` an exception.  */
    fun log(priority: Int, t: Throwable) {
        seed?.log(priority, t)
    }

}


class DefaultSeed : Seed {

    private var tag: String = Loggy.BASE_TAG

    override fun tag(tag: String) {
        this.tag = tag
    }

    override fun v(message: String, vararg args: Any) =
            if (args.isEmpty()) message.tagged(tag).print()
            else String.format(message, *args).tagged(tag).print()

    override fun v1(t: Throwable) = t.toString().tagged(tag).print()

    override fun d(message: String, vararg args: Any) = v(message, *args)

    override fun d(t: Throwable, message: String, vararg args: Any) =
            if (args.isEmpty()) message.tagged("${tag}:${t.simpleName()}").print()
            else String.format(message, *args).tagged("${tag}:${t.simpleName()}").print()

    override fun d(t: Throwable) = v1(t)

    override fun i(message: String, vararg args: Any) = v(message, *args)

    override fun i(t: Throwable, message: String, vararg args: Any) = d(t, message, *args)

    override fun i(t: Throwable) = v1(t)

    override fun w(message: String, vararg args: Any) = v(message, *args)

    override fun w(t: Throwable, message: String, vararg args: Any) = d(t, message, *args)

    override fun w(t: Throwable) = v1(t)

    override fun e(message: String, vararg args: Any) = v(message, *args)

    override fun e(t: Throwable, message: String, vararg args: Any) = d(t, message, *args)

    override fun e(t: Throwable) = v1(t)

    override fun wtf(message: String, vararg args: Any) = v(message, *args)

    override fun wtf(t: Throwable, message: String, vararg args: Any) = d(t, message, *args)

    override fun wtf(t: Throwable) = v1(t)

    override fun log(priority: Int, message: String, vararg args: Any) = v(message, *args)

    override fun log(priority: Int, t: Throwable, message: String, vararg args: Any) = d(t, message, *args)

    override fun log(priority: Int, t: Throwable) = v1(t)

}

fun String.tagged(tag: String = Loggy.BASE_TAG) = "$tag: $this"

fun String.print() = println(this)

fun nl() = System.getProperty("line.separator")