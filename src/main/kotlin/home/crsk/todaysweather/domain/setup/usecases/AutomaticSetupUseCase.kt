package home.crsk.todaysweather.domain.setup.usecases

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.LocationDetector
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteNotFoundError
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.log.nl
import home.crsk.todaysweather.domain.common.other.toPrettyString
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.functions.BiConsumer
import java.util.concurrent.Callable

class AutomaticSetupUseCase(private val locationDetector: LocationDetector,
                            private val autoCompleteRepository: AutoCompleteRepository,
                            private val languageProvider: LanguageProvider) : UseCase {

    private val maxDistanceAllowedInMeters = 10000

    private val collectCallable =
            Callable<ArrayList<LocationDistancePair>> {
                arrayListOf()
            }

    private val collectBiConsumer =
            BiConsumer<ArrayList<LocationDistancePair>, List<LocationDistancePair>> { collector, list ->
                collector.addAll(list)
            }

    /**
     * Obtain locations based on addresses from locationDetector
     * Use autoCompleteRepository to get locations based on locationDetector's each detected location coordinates
     * Calculate the distance from results relative to the current detected location and pair'em as LocationDistancePair
     * Collect all results into one list
     * Filter locations with distance less than maxDistanceAllowedInMeters and then sort them
     * If empty throw an  AutoCompleteNotFoundError Else
     * Unwind the pair and map the closest location to a Response
     */
    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(Request.AutomaticSetupRequest::class.java)
                .switchMap {
                    locationDetector.detect(it.useLastKnownLocation).flatMap { detected ->
                        autoCompleteRepository.findExact(detected.city, detected.countryCode)
                                .doOnSuccess { Loggy.d("AutoCompleteUseCase: Results found for detected location ${detected.city}:${nl()} ${it.toPrettyString()}") }
                                .doOnError { Loggy.d("AutoCompleteUseCase: Results not found for detected location: ${detected.city}") }
                                .map { it.map { it to Location.distanceBetweenLocations(detected, it) } }
                                .onErrorReturnItem(emptyList())
                                .toObservable()
                    }.collect(collectCallable, collectBiConsumer)
                            .map {
                                it.filter { p -> p.second <= maxDistanceAllowedInMeters }
                                        .sortedWith(Comparator { p1, p2 -> p1.second.compareTo(p2.second) })
                            }
                            .filter { it.isNotEmpty() }
                            .switchIfEmpty(Single.error(AutoCompleteNotFoundError))
                            .doOnSuccess {  Loggy.d("AutoCompleteUseCase: Best locations found within $maxDistanceAllowedInMeters meters:${nl()} ${it.toPrettyString()}") }
                            .doOnError { Loggy.d("AutoCompleteUseCase: No location found within $maxDistanceAllowedInMeters meters") }
                            .map { Response.Stateful.SelectedLocationResponse(it.first().first) }
                            .toObservable()
                            .compose(startWithInFlight())
                            .onErrorResumeNext(errorResumeCompleteTranslated(languageProvider))
                }
    }


}

typealias LocationDistancePair = Pair<Location, Double>