package home.crsk.todaysweather.domain.common.other

import org.junit.Assert.assertEquals
import org.junit.Test

class GooglemapsKtTest {

    @Test
    fun `rgb conversion`() {
        assertEquals("color:0xC0C0C0",
                Color.ofRGB(192, 192, 192).toString())
    }

    @Test
    fun `marker conversion`() {
        val markers = Markers()
        val marker = markers.marker(123.0,
                456.0) {
            color = Color.of(Color.Defined.RED)
            size = Size.MID
            label = 'A'
        }
        markers + marker
        markers + marker
        assertEquals("markers=size:mid%7Ccolor:red%7Clabel:A%7C123.0,456.0" +
                "&markers=size:mid%7Ccolor:red%7Clabel:A%7C123.0,456.0", markers.toString())
    }

    @Test
    fun googleStaticMapsTest() {
        val yourAPIKey ="API_KEY"
        val expected = "https://maps.googleapis.com/maps/api/staticmap?center=45.7489,21.2087" +
                "&zoom=10&size=600x600&scale=2&format=png32&maptype=hybrid" +
                "&markers=color:blue%7Clabel:S%7C45.7489,21.2087" +
                "&markers=color:red%7Clabel:D%7C45.793612,21.234441"+
                "&key=$yourAPIKey"
        val url = googleStaticMaps(yourAPIKey,45.7489, 21.2087) {
            map {
                size = Map.Size(600)
                scale = Map.Scale.DOUBLE
                type = Map.Type.HYBRID
            }
            markers {
                this + marker(45.7489, 21.2087) {
                    color = Color.of(Color.Defined.BLUE)
                    label = 's'
                }
                this + marker(45.793612, 21.234441) {
                    label = 'd'
                }
            }
        }
        assertEquals(expected, url)
    }
}