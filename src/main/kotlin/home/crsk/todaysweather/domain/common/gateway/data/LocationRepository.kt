package home.crsk.todaysweather.domain.common.gateway.data

import home.crsk.todaysweather.domain.common.entity.Location
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

interface LocationRepository {

    fun findByKey(key: String): Maybe<Location>

    fun save(locations: List<Location>): Single<List<Location>>

    fun observeChanges(): Observable<Location> = Observable.never()
}