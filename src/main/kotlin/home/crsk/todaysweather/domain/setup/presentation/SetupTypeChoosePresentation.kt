package home.crsk.todaysweather.domain.setup.presentation

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.NoState
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Request.NoRequest
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class SetupTypeChoosePresentation(private val navigationToUseCase: UseCase,
                                  schedulers: SchedulersContract) : Presentation<NoState>(schedulers) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is Intent.NavigationIntent -> Request.NavigationToRequest(it.route)
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<NoState, Response.Stateful, NoState> =
            BiFunction { carrier, _ -> carrier }

    override fun defaultState(): NoState = NoState

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase))
}
