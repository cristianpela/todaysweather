package home.crsk.todaysweather.domain.common.other

import io.reactivex.Completable
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import util.BaseTest
import java.util.concurrent.TimeUnit

class RxKtTest : BaseTest() {
    @Test
    fun `should do exponential back off`() {

        val testScheduler: TestScheduler = schedulers.computation() as TestScheduler


        val test = Completable.error(FooError).retryWhen(errorBackOffFlowable(delayScheduler = testScheduler)).test()

        test.assertNoErrors()

        //error sequence : 5 + 10 + 15 + 20...
        //for 3 retries = 5 + 10 the 3rd retry will return as error => we advance 15 seconds
        testScheduler.advanceTimeBy(30, TimeUnit.SECONDS)
        test.assertError(FooError)
    }

    @Test
    fun `should not do exponential back off for a specific error`() {

        val testScheduler: TestScheduler = schedulers.computation() as TestScheduler

        val error = FooError

        val test = Completable.error(error).retryWhen(errorBackOffFlowable(retries = 4,
                skip = listOf(FooError::class.java), delayScheduler = testScheduler)).test()

        test.assertError(error)
    }

    private object FooError : Error()

}