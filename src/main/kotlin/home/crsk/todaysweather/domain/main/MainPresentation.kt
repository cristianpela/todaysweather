@file:Suppress("unused")

package home.crsk.todaysweather.domain.main

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class MainPresentation(private val navigationWatchUseCase: UseCase,
                       private val navigationToUseCase: UseCase,
                       schedulers: SchedulersContract) : Presentation<NavigationState>(schedulers) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    Intent.CancelIntent -> Request.NavigationWatchRequest(true)
                    Intent.NavigationWatchIntent -> Request.NavigationWatchRequest()
                    is Intent.NavigationIntent -> Request.NavigationToRequest(it.route)
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<NavigationState, Response.Stateful, NavigationState> =
            BiFunction { carrier, response ->
                when (response) {
                    is Response.Stateful.NavigationResponse -> carrier.copy(route = response.route)
                    else -> carrier
                }
            }

    override fun defaultState(): NavigationState = NavigationState()

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> = listOf(
            dispatchRequest<Request.NavigationWatchRequest>(requestObservable, navigationWatchUseCase),
            dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase)
    )
}