package home.crsk.todaysweather.domain.common.gateway

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_LANGUAGE_SETUP
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import io.reactivex.Observable
import org.junit.Test

class NavigatorImplTest {

    @Test
    fun `should redirect to choose language setup screen`() {

        val settingService: SettingsService = mock {
            on { observe() } doReturn Observable.just(Settings())
        }

        NavigatorImpl(settingService).observe().test().assertValue(Route(Screens.SCREEN_NONE, Screens.SCREEN_LANGUAGE_SETUP))
    }


    @Test
    fun `should redirect to choose setup type mode auto or manual`() {

        val settingService: SettingsService = mock {
            on { observe() } doReturn Observable.just(Settings(langCodeISO = "RO"))
        }

        NavigatorImpl(settingService).observe().test().assertValue(Route(Screens.SCREEN_NONE, Screens.SCREEN_CHOOSE_SETUP_TYPE))
    }


    @Test
    fun `should change language if setup is done`() {

        val settingService: SettingsService = mock {
            on { observe() } doReturn Observable.just(Settings("foo", "RO"))
        }

        NavigatorImpl(settingService).observe().test().assertValue(Route(Screens.SCREEN_NONE, Screens.SCREEN_APP))
    }

    @Test
    fun `should navigation to a chosen part by user`() {

        val settingService: SettingsService = mock {
            on { observe() } doReturn Observable.just(Settings("foo", "RO"))
        }

        val navigator = NavigatorImpl(settingService)
        val test = navigator.observe().test()

        navigator.navigateTo(Route(Screens.SCREEN_APP, Screens.SCREEN_SETTINGS))

        test.assertValueAt(0, Route(Screens.SCREEN_NONE, Screens.SCREEN_APP))
                .assertValueAt(1, Route(Screens.SCREEN_APP, Screens.SCREEN_SETTINGS))
    }


    @Test
    fun `should redirect based on custom interceptor`() {
        val settingService: SettingsService = mock {
            on { observe() } doReturn Observable.empty()
        }

        val navigator = NavigatorImpl(settingService)
                .apply {
                    addInterceptor(object : NavigationInterceptor {
                        override fun onNavigationIntercept(navChain: NavChain): Route =
                                navChain.proceed(Route(navChain.route.from, Screens.SCREEN_LANGUAGE_SETUP))
                    })
                    addInterceptor(object : NavigationInterceptor {
                        override fun onNavigationIntercept(navChain: NavChain): Route {
                            val redirectScreen = if (navChain.route() == SCREEN_LANGUAGE_SETUP) {
                                Screens.SCREEN_MANUAL_SETUP
                            } else navChain.route()
                            return navChain.proceed(Route(navChain.route.from, redirectScreen))
                        }
                    })
                }
        val test = navigator.observe().test()

        navigator.navigateTo(Route(Screens.SCREEN_NONE, Screens.SCREEN_APP))
        test.assertValue(Route(Screens.SCREEN_NONE, Screens.SCREEN_MANUAL_SETUP))
    }

}