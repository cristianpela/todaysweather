package home.crsk.todaysweather.domain.common.entity

data class UnitDescriptor(val unit: String, val degreeSymbol: String,
                          val degreeName: String, val speedMeasure: String,
                          val rainMeasure: String, val snowMeasure: String)