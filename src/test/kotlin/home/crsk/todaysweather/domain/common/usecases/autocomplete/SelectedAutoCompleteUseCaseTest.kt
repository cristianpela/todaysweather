package home.crsk.todaysweather.domain.common.usecases.autocomplete

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteNotFoundError
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Maybe
import io.reactivex.Observable
import org.junit.Test
import util.BaseTest

class SelectedAutoCompleteUseCaseTest : BaseTest() {

    private val autoCompleteRepository: AutoCompleteRepository = mock {
        on { findByKey(any()) } doReturn Maybe.empty<Location>()
    }

    @Test
    fun `should return error when no key provided`() {
        val useCase = SelectedAutoCompleteUseCase(autoCompleteRepository)
        val test = Request.SelectLocationRequest().observable().compose(useCase.execute()).test()
        test.assertValueAt(0, { (it as Response.Error).err == AutoCompleteNotFoundError })
    }

    @Test
    fun `should return error when key is provided but location is not found`() {
        val useCase = SelectedAutoCompleteUseCase(autoCompleteRepository)
        val test = Request.SelectLocationRequest("foo").observable().compose(useCase.execute()).test()
        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, { (it as Response.Error).err == AutoCompleteNotFoundError })
    }


    @Test
    fun `should return location when a valid key provided`() {
        val autoCompleteRepository: AutoCompleteRepository = mock {
            on { findByKey(any()) } doReturn Maybe.just(Location())
        }
        val useCase = SelectedAutoCompleteUseCase(autoCompleteRepository)
        val test = Request.SelectLocationRequest("foo").observable().compose(useCase.execute()).test()
        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, { (it as Response.Stateful.SelectedLocationResponse).location == Location() })
    }

    @Test
    fun `should return error when an uncontrolled error occurres`() {
        val uncontrolledErr = Error()

        val autoCompleteRepository: AutoCompleteRepository = mock {
            on { findByKey(any()) } doReturn Maybe.error(uncontrolledErr)
        }
        val useCase = SelectedAutoCompleteUseCase(autoCompleteRepository)
        val test = Observable.fromArray(Request.SelectLocationRequest("foo"),
                Request.SelectLocationRequest()).compose(useCase.execute()).test()
        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, { (it as Response.Error).err == uncontrolledErr })
    }

    @Test
    fun `should return ther right stream sequence`() {
        val autoCompleteRepository: AutoCompleteRepository = mock {
            on { findByKey(any()) } doReturn Maybe.just(Location())
        }
        val useCase = SelectedAutoCompleteUseCase(autoCompleteRepository)
        val test = Observable.fromArray(
                Request.SelectLocationRequest("foo"),
                Request.SelectLocationRequest()
        ).compose(useCase.execute()).test()

        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, { (it as Response.Stateful.SelectedLocationResponse).location == Location() })
        test.assertValueAt(2, { (it as Response.Error).err == AutoCompleteNotFoundError })
    }
}