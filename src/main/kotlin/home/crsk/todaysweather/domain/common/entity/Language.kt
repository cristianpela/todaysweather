package home.crsk.todaysweather.domain.common.entity

data class Language(val isoCode: String, val wuCode: String, val name: String, val flagImage: Int, val selected: Boolean = false)
