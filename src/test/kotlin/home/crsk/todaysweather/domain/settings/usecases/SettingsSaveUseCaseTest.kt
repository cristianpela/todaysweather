package home.crsk.todaysweather.domain.settings.usecases

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest
import kotlin.test.assertEquals

class SettingsSaveUseCaseTest : BaseTest() {

    @Test
    fun `should save`() {

        val location = Location(locationKey = "foo")
        val langISO = "EN"
        val unit = UnitEnum.METRIC
        val savedMsg ="All saved"

        val settingsService: SettingsService = mock {
            on { save(any()) } doReturn Completable.complete()
            on { current() } doReturn Single.just(Settings())
        }

        val languageProvider: LanguageProvider = mock{
            on {translate(anyString(), anyVararg())} doReturn savedMsg
        }

        val test = Observable.merge(
                Request.SaveRequest.SaveLocationRequest(location).observable(),
                Request.SaveRequest.SaveLanguageRequest(langISO).observable(),
                Request.SaveRequest.SaveAllRequest(Settings("foo", langISO, unit)).observable())
                .compose(SettingsSaveUseCase(settingsService, languageProvider).execute())
                .test()

        test.assertValueAt(0, Response.Stateful.Completed)
        test.assertValueAt(1, {(((it as Response.Message).message)
                as CommonMessage.Alert).message == savedMsg })
        test.assertValueAt(2, Response.Stateful.Completed)
        test.assertValueAt(3, {(((it as Response.Message).message)
                as CommonMessage.Alert).message == savedMsg })
        test.assertValueAt(4, Response.Stateful.Completed)
        test.assertValueAt(5, {(((it as Response.Message).message)
                as CommonMessage.Alert).message == savedMsg })


        val captor = argumentCaptor<Settings>()
        verify(settingsService, times(3)).save(captor.capture())
        assertEquals(listOf(Settings(locationKey = location.locationKey), Settings(langCodeISO = langISO),
                Settings("foo", langISO, unit)), captor.allValues)

    }

}