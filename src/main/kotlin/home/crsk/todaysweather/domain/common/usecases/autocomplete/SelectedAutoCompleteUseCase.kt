package home.crsk.todaysweather.domain.common.usecases.autocomplete

import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteNotFoundError
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

class SelectedAutoCompleteUseCase(private val autoCompleteRepository: AutoCompleteRepository) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(Request.SelectLocationRequest::class.java)
                .publish { selector ->
                    Observable.merge(
                            selector.filter { it.locationKey == null }.map { Response.Error(AutoCompleteNotFoundError) },
                            selector.filter { it.locationKey != null }.flatMap {
                                autoCompleteRepository.findByKey(it.locationKey!!).map { Response.Stateful.SelectedLocationResponse(it) }
                                        .cast(Response::class.java)
                                        .defaultIfEmpty(Response.Error(AutoCompleteNotFoundError))
                                        .toObservable()
                                        .onErrorReturn { Response.Error(it) }
                                        .startWith(Response.Stateful.InFlight)
                            }
                    )
                }
    }

}