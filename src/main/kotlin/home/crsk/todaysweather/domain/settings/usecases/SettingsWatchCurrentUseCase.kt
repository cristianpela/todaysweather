package home.crsk.todaysweather.domain.settings.usecases

import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.Function3

class SettingsWatchCurrentUseCase(private val settings: SettingsService,
                                  private val locationRepository: LocationRepository,
                                  private val languageProvider: LanguageProvider) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up ->
                up.cast(Request.SettingsWatchCurrentRequest::class.java)
                        .switchMap {
                            settings.observe().flatMap { current ->
                                Observable.zip(
                                        locationRepository.findByKey(current.locationKey!!).toObservable(),
                                        languageProvider.findLanguageByISO(current.langCodeISO!!).toObservable(),
                                        current.unit.observable(),
                                        Function3<Location, Language, UnitEnum, Response> { loc, lang, unit ->
                                            Response.Stateful.DisplayableSettingsResponse(
                                                    DisplayableSettings(loc, lang, unit))
                                        }
                                )
                            }.startWith(Response.Stateful.InFlight)
                        }
            }
}