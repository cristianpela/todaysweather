package home.crsk.todaysweather.domain.common.usecases.navigation

import home.crsk.todaysweather.domain.common.gateway.Navigator
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer

class NavigationWatchUseCase(private val navigator: Navigator) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up ->
                up.cast(Request.NavigationWatchRequest::class.java).switchMap {
                    if (it.cancel)
                        Response.Stateful.Completed.observable()
                    else
                        navigator.observe().map { Response.Stateful.NavigationResponse(it) }
                }
            }
}