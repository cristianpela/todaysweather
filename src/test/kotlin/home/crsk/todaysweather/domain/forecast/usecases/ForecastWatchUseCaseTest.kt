package home.crsk.todaysweather.domain.forecast.usecases

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade
import home.crsk.todaysweather.domain.common.gateway.data.forecast.generateForecast
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

class ForecastWatchUseCaseTest : BaseTest() {

    private val langWUCode = "US"

    private val date = Calendar.getInstance().apply {
        set(Calendar.YEAR, 2017)
        set(Calendar.MONTH, 12)
        set(Calendar.DAY_OF_MONTH, 6)
        set(Calendar.HOUR_OF_DAY, 0)
    }


    @Test
    fun execute() {

        val settingsSubject = PublishSubject.create<Settings>()

        val settingsService: SettingsService = mock {
            on { observe() } doReturn settingsSubject
        }
        val facade: ForecastRepositoryFacade = mock {
            on { observe(any(), anyString(), any(), any()) } doReturn Observable.defer {
                Observable.just(generateForecast(batchSize = 1))
            }
        }
        val locationRepo: AutoCompleteRepository = mock {
            on { findByKey(anyString()) } doReturn Maybe.just(Location())
        }

        val languageProvider: LanguageProvider = mock {
            on { findLanguageByISO(anyString()) } doReturn Single
                    .just(Language("US", "US", "United Stated", 1))
        }

        val useCase = ForecastWatchUseCase(settingsService, locationRepo, languageProvider, facade, schedulers)
        val test = Observable.just(Request.ForecastWatchRequest())
                .compose(useCase.execute()).test()

        computationPoolAdvanceTo(date.timeInMillis, TimeUnit.MILLISECONDS)

        settingsSubject.onNext(Settings("FOO", langWUCode))

        computationPoolAdvanceBy(4, TimeUnit.HOURS)

        //the first cycle + 4 interval cycles
        //each cycle has a pair of InFlight + ForecastResponse
        assertEquals(10, test.valueCount()) // the first cycle + 4 interval cycles

    }

    @Test
    fun `execute error`() {
        val settingsSubject = PublishSubject.create<Settings>()

        val settingsService: SettingsService = mock {
            on { observe() } doReturn settingsSubject
        }
        val facade: ForecastRepositoryFacade = mock {
            on { observe(any(), anyString(), any(), any()) } doReturn Observable.error(Error())
        }

        val locationRepo: AutoCompleteRepository = mock {
            on { findByKey(anyString()) } doReturn Maybe.just(Location())
        }

        val languageProvider: LanguageProvider = mock {
            on { findLanguageByISO(anyString()) } doReturn Single
                    .just(Language("US", "US", "United Stated", 1))
        }

        val useCase = ForecastWatchUseCase(settingsService, locationRepo, languageProvider, facade, schedulers)
        val test = Observable.just(Request.ForecastWatchRequest())
                .compose(useCase.execute()).test()

        computationPoolAdvanceTo(date.timeInMillis, TimeUnit.MILLISECONDS)

        settingsSubject.onNext(Settings("FOO", langWUCode))

        test.values().equals(listOf(Response.Stateful.InFlight, Response.Stateful.Completed, Response.Error(Error())))

    }
}