package home.crsk.todaysweather.domain.common.entity

import home.crsk.todaysweather.domain.common.other.timeData
import java.text.SimpleDateFormat
import java.util.*

data class TimeData(val time: Long, val year: Int, val month: Int, val day: Int,
                    val hour: Int, val min: Int = 0, val second: Int = 0) {

    companion object {
        fun fromPieces(year: Int, month: Int, day: Int,
                       hour: Int, min: Int = 0, second: Int = 0): TimeData {
            val cal = GregorianCalendar.getInstance()
            cal.set(year, month - 1, day, hour, min, second)
            return cal.timeData()
        }
    }

    fun display(format: String): String = SimpleDateFormat(format).format(time)
}

data class WindSpeed(val speed: UnitPayload<Int>, val direction: String, val degrees: Int){
    override fun toString(): String {
        return "${speed.obtain().value}${speed.obtain().symbol} $direction"
    }
}

data class TextForecast(val time: TimeData, val period: Int, val icon: String, val title: String,
                        val text: UnitPayload<String>, val precipitationChance: Int)

data class SimpleForecast(val time: TimeData, val high: UnitPayload<Int>, val low: UnitPayload<Int>,
                          val conditions: String, val icon: String, val precipitationChance: Int,
                          val maxWindSpeed: WindSpeed, val averageWindSpeed: WindSpeed,
                          val humidity: Int, val rainQuantity: UnitPayload<Float>,
                          val snowQuantity: UnitPayload<Float>)

data class HourForecast(val time: TimeData, val temperatureDegree: UnitPayload<Int>,
                        val condition: String, val icon: String,
                        val windSpeed: WindSpeed, val feelsLike: UnitPayload<Int>,
                        val precipitationChance: Int, val humidity: Int)


data class Forecast(val simpleForecasts: List<SimpleForecast>,
                    val textForecasts: List<TextForecast>,
                    val hourlyForecasts: List<HourForecast>, val lastUpdate: Long) {
    companion object {
        val NULL = Forecast(emptyList(), emptyList(), emptyList(), -1)
    }
}