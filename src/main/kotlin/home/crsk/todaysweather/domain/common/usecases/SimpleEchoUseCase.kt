package home.crsk.todaysweather.domain.common.usecases

import io.reactivex.ObservableTransformer

class SimpleEchoUseCase(private val response: (Request) -> Response) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.map { response(it) }
    }
}

object EchoUseCaseFactory {
    fun create(response: Response): SimpleEchoUseCase = SimpleEchoUseCase { response }
}