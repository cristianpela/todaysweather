package home.crsk.todaysweather.domain.autocomplete

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.presentation.State

data class AutoCompleteSearchState(val partialSearch: String = "",
                                   val locations: List<Location> = emptyList(),
                                   val owner: String? = null
): State()