package home.crsk.todaysweather.domain.common.gateway.data.settings

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test
import util.BaseTest

class SettingsServiceSavingProxyTest : BaseTest() {

    @Test
    fun `should cache the changes and save to real service at the end`() {

        val settingsService: SettingsService = mock {
            on { check() } doReturn Completable.complete()
            on { save(any()) } doReturn Completable.complete()
            on { current() } doReturn Single.just(Settings("loc1", "lang1", UnitEnum.METRIC))
        }

        val proxy: SettingsService = SettingsServiceSavingProxy(settingsService)

        val test = proxy.observe()
                .take(4)
                .reduce { acc, curr ->
                    Settings("${acc.locationKey}-${curr.locationKey}",
                            "${acc.langCodeISO}-${curr.langCodeISO}",
                            curr.unit)
                }.test()

        proxy.save(Settings("loc2", "lang1", UnitEnum.METRIC))
                .andThen(proxy.save(Settings("loc2", "lang2", UnitEnum.METRIC)))
                .andThen(proxy.save(Settings("loc2", "lang2", UnitEnum.ENGLISH)))
                .andThen(proxy.save(Settings.FORCE_SAVE_SETTINGS_FLAG))
                .subscribe()

        test.assertValue(Settings(
                "loc1-loc2-loc2-loc2",
                "lang1-lang1-lang2-lang2",
                UnitEnum.ENGLISH))

        verify(settingsService).save(any())
    }


}