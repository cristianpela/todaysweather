package home.crsk.todaysweather.domain.favorites.usecases

import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer

class FavoritesFetchUseCase(
        private val settingsService: SettingsService,
        private val favoriteRepository: FavoriteRepository) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up ->
                up.cast(Request.FavoritesFetchRequest::class.java).flatMap {
                    settingsService.current().flatMap {
                        favoriteRepository.getAll(it.unit)
                                .map { Response.Stateful.FavoritesFetchResponse(it) }

                    }.toObservable()
                }
            }
}