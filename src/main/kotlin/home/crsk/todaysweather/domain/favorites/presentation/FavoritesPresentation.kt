package home.crsk.todaysweather.domain.favorites.presentation

import home.crsk.todaysweather.domain.common.entity.Favorite
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.other.asPercent
import home.crsk.todaysweather.domain.common.other.now
import home.crsk.todaysweather.domain.common.other.timeData
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.favorites.FavoriteState
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class FavoritesPresentation(private val favoriteWatchUseCase: UseCase,
                            private val favoritesFetchUseCase: UseCase,
                            private val favoriteChangeUseCase: UseCase,
                            private val navigationToUseCase: UseCase,
                            schedulersContract: SchedulersContract,
                            private val initialState: FavoriteState = FavoriteState()) : Presentation<FavoriteState>(schedulersContract) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    Intent.FavoritesFetchIntent -> Request.FavoritesFetchRequest
                    Intent.FavoriteWatchIntent -> Request.FavoriteWatchRequest()
                    Intent.CancelIntent -> Request.CompositeRequest(
                            Request.FavoriteChangeRequest(null),
                            Request.FavoriteWatchRequest(false))
                    is Intent.NavigationIntent -> Request.NavigationToRequest(it.route)
                    is Intent.FavoriteChangeIntent -> Request.FavoriteChangeRequest(it.location, it.undoable)
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<FavoriteState, Response.Stateful, FavoriteState> =
            BiFunction { carrier, response ->
                when (response) {
                    Response.Stateful.InFlight -> carrier.copy()(true)
                    is Response.Stateful.FavoritesFetchResponse -> carrier
                            .copy(favorites = response.favorites.asSequence().map { it.toDisplayable() }.toList())()

                    is Response.Stateful.UndoableFavoriteChangeResponse -> carrier
                            .copy(hasUndoHistory = response.hasUndoHistory)()
                    is Response.Stateful.FavoriteChangeResponse -> {
                        with(response.favorite) {
                            if (isFavorite && carrier.favorites.firstOrNull { it.locationKey == this.locationKey } != null) {
                                carrier // do nothing when we get the same location and is favorite
                            } else {
                                val newFavorites: List<DisplayableFavorite> = (if (isFavorite) {
                                    carrier.favorites + this.toDisplayable()
                                } else {
                                    carrier.favorites.filter { it.locationKey != this.locationKey }
                                }).sortedByDescending { it.lastUpdate }
                                carrier.copy(favorites = newFavorites)()
                            }
                        }
                    }
                    else -> carrier
                }
            }

    override fun defaultState(): FavoriteState = initialState

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(dispatchRequest<Request.FavoritesFetchRequest>(requestObservable, favoritesFetchUseCase),
                    dispatchRequest<Request.FavoriteWatchRequest>(requestObservable, favoriteWatchUseCase),
                    dispatchRequest<Request.FavoriteChangeRequest>(requestObservable, favoriteChangeUseCase),
                    dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase)
            )


}

internal fun Favorite.toDisplayable(): DisplayableFavorite {

    val dunno = "?"

    val isUpToDate = hour != null

    val hourStr = hour?.display("HH:mm") ?: now().timeData().display("HH:mm")

    val (temperatureStr, symbol) = hourTemperature?.let {
        it.obtain().value.toString() to it.obtain().symbol
    } ?: dunno to dunno

    val condition = hourCondition ?: dunno
    val icon = hourConditionIcon ?: dunno

    val precipitationChance = hourPrecipitationChance?.asPercent() ?: dunno

    return DisplayableFavorite(locationKey,
            city,
            isFavorite,
            lastUpdate,
            lastUpdate.timeData().display("dd.MM.yyyy HH:mm"),
            isUpToDate,
            hourStr,
            temperatureStr,
            symbol,
            condition,
            icon,
            precipitationChance)
}


data class DisplayableFavorite(
        val locationKey: String,
        val city: String,
        val isFavorite: Boolean,
        val lastUpdate: Long,
        val lastUpdateStr: String,
        val isUpToDate: Boolean,
        val hour: String,
        val temperature: String,
        val symbol: String,
        val condition: String,
        val conditionIcon: String,
        val precipitationChance: String)

