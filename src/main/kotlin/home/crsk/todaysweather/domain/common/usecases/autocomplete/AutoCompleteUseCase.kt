package home.crsk.todaysweather.domain.common.usecases.autocomplete

import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.*
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

class AutoCompleteUseCase(private val autoCompleteRepository: AutoCompleteRepository) : UseCase {

    private val validator: Validator<Request.AutoCompleteSearchLocationRequest> = Validator(listOf(
            object : ValidationRule<Request.AutoCompleteSearchLocationRequest> {
                /**
                 * bad luck if your location has less than 3 letters
                 */
                override fun validate(item: Request.AutoCompleteSearchLocationRequest): Boolean = item.partialSearch.length >= 3

                override fun fail(): Throwable = AutoCompleteSilentValidationError
            }
    ))

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(Request.AutoCompleteSearchLocationRequest::class.java)
                //by using switch map we got previous requests canceled
                .switchMap { req ->
                    if (req == Request.AutoCompleteSearchLocationRequest.CANCEL) {
                        Response.Stateful.Completed.observable()
                    } else
                        validator.validate(req, Observable.defer {
                            autoCompleteRepository
                                    .find(req.partialSearch)
                                    .map { Response.Stateful.AutoCompleteResponse(it, req.partialSearch, req.owner) }
                                    .cast(Response::class.java)
                                    .toObservable()
                        }).onErrorResumeNext(::errorResumeCompleted).compose(startWithInFlight())
                }
    }

    companion object {
        val INPUT_DELAY_MILLIS = 300L
    }
}

object AutoCompleteSilentValidationError : Error()