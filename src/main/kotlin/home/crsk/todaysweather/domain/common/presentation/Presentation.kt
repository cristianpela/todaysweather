package home.crsk.todaysweather.domain.common.presentation

import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

abstract class Presentation<S : State>(private val schedulers: SchedulersContract) :
        PresentationStreamOps<S> {

    private var backgroundScheduler = schedulers.io()

    private val intentProxy: Subject<Intent> = PublishSubject.create<Intent>().toSerialized()
    private val stateProxy: Subject<S> = BehaviorSubject.create<S>().toSerialized()
    private val errorProxy: Subject<Throwable> = PublishSubject.create<Throwable>().toSerialized()
    private val messageProxy: Subject<CommonMessage> = PublishSubject.create<CommonMessage>().toSerialized()

    /**
     * hacky stuff
     * call this function in child's class init block
     *
     * MVI CYCLE PIPELINE
     *
     * intent -> request ->
     *                  split and dispatch to usecases/interactors ->
     *           merge responses -> split responses into reduce state/errors/messages to be subscribed by ui/terminal
     *
     */
    protected fun superInit() {

        val publishCycle = intentProxy.map(intentToRequestMapper())
                .flatMap {
                    if (it is Request.CompositeRequest) {
                        Observable.fromArray(*it.requests)
                    } else {
                        Observable.just(it)
                    }
                }
                .filter { it != Request.NoRequest }
                //split requests - dispatch to usesase - merge responses
                .subscribeOn(backgroundScheduler)
                .publish { Observable.merge(useCaseDispatcher(it)) }
                //share for a new split
                .publish()

        publishCycle.ofType(Response.Stateful::class.java)
                .cast(Response.Stateful::class.java)
                .scan(defaultState(), reducer())
                //.distinctUntilChanged(::statesAreEqual)
                .observeOn(schedulers.main())
                .subscribe(stateProxy)

        publishCycle.ofType(Response.Error::class.java)
                .map { it.err }
                .observeOn(schedulers.main())
                .subscribe(errorProxy)

        publishCycle.ofType(Response.Message::class.java)
                .map { it.message }
                .observeOn(schedulers.main())
                .subscribe(messageProxy)

        publishCycle.autoConnect(-1) // immediate connection
    }

    fun dispatchRequestsOnMainThread(doIt: Boolean) {
        if (doIt) {
            backgroundScheduler = schedulers.main()
        } else {
            schedulers.io()
        }
    }

    /**
     * dispatches the requests to the appropriate usecase/interactor. the use case execution,</br>
     * unless specified owtherwise (#dispatchRequestsOnMainThread)
     * will be scheduled on background thread
     */
    protected fun dispatchRequest(requestObservable: Observable<out Request>,
                                  clazzez: List<Class<out Request>>,
                                  useCase: UseCase,
                                  justOnce: Boolean = false,
                                  remapRequest: Function<in Request, out Request> = Function { it }): Observable<Response> {
        return Observable.merge(clazzez.asSequence().map { requestObservable.ofType(it) }.toList())
                .map(remapRequest)
                .distinctUntilChanged { _, _ -> justOnce }
                .observeOn(backgroundScheduler)//assure that usecases are executed in background
                .doOnNext { Loggy.d("Presentation: Request $it dispatched on ${Thread.currentThread()}") }
                .compose(useCase.execute())
    }

    protected inline fun <reified R : Request> dispatchRequest(requestObservable: Observable<out Request>,
                                                               useCase: UseCase,
                                                               justOnce: Boolean = false,
                                                               remapRequest: Function<in Request, out Request> = Function { it }): Observable<Response> {
        return dispatchRequest(requestObservable, listOf(R::class.java), useCase, justOnce, remapRequest)
    }

    override fun bindIntents(intentStream: Observable<Intent>): Disposable =
            intentStream.subscribe { intentProxy.onNext(it) }

    override fun observeState(): Observable<S> = stateProxy
            .doOnSubscribe { Loggy.d("Presentation Terminal Client subscribed to presentation $it") }

    override fun observeErrors(): Observable<Throwable> = errorProxy

    override fun observeMessages(): Observable<CommonMessage> = messageProxy

    abstract fun intentToRequestMapper(): Function<Intent, Request>

    abstract fun reducer(): BiFunction<S, Response.Stateful, S>

    abstract fun defaultState(): S

    abstract fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>>
}

abstract class State() {

    var inFlight = false
        private set

    var completed = false
        private set

    fun inFlight() {
        inFlight = true
        completed = false
    }

    fun completed() {
        inFlight = false
        completed = true
    }

    @Suppress("unused")
    fun idle() {
        inFlight = false
        completed = false
    }

    @Suppress("UNCHECKED_CAST")
    operator fun <S : State> invoke(inFlight: Boolean = false): S =
            apply { if (inFlight) inFlight() else completed() } as S

}

object NoState : State()


