package home.crsk.todaysweather.domain.forecast.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.TimeData
import home.crsk.todaysweather.domain.common.gateway.data.forecast.generateForecast
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.advance
import home.crsk.todaysweather.domain.common.other.timeData
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.drivers.data.remote.retrofit.forecast.FullForecast
import home.crsk.todaysweather.drivers.data.remote.retrofit.forecast.toForecastEntity
import home.crsk.todaysweather.drivers.data.remote.retrofit.retrofitGson
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.junit.Test
import util.BaseTest
import util.assertEq
import util.neverUseCase
import java.util.*
import kotlin.test.assertEquals

class ForecastPresentationTest : BaseTest() {

    @Test
    fun `make sure the dto to view model is correct`(){
        val bodyContent = loadResource("test_temp_corner_case_nfe.json").readText()
        val fullForecast = retrofitGson().fromJson(bodyContent, FullForecast::class.java)

        val forecastWatchUseCase: UseCase = object: UseCase{
            override fun execute(): ObservableTransformer<in Request, out Response> {
                return ObservableTransformer {
                    it.flatMap {  Observable
                            .fromArray(Response.Stateful.InFlight,
                                    Response.Stateful.ForecastResponse(Location.bare("Foo"),
                                            fullForecast.toForecastEntity(System.currentTimeMillis())!!))}
                }
            }

        }



        val forecastPresentation = ForecastPresentation(
                forecastWatchUseCase,
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                schedulers,
                timeReference = { TimeData.fromPieces(2018, 2, 5, 20)})

        val test = forecastPresentation
                .observeState()
                .test()


        forecastPresentation
                .bindIntents(Intent.ForecastWatchIntent.observable())
        test.assertNoErrors()

        test.values().map { it.inFlight }.assertEq(listOf(false, true, false))
    }

    @Test
    fun `make sure the initial value si idle`(){
        val forecastPresentation = ForecastPresentation(
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                schedulers)

         forecastPresentation
                .observeState()
                .test().assertValue { !it.inFlight && !it.completed }
                 .assertValueCount(1)
    }

    @Test
    //todo this test is not passing right now, because its mock generates the same date, and the presenter mvi reducer has a distinctuntilchange
            // so instead of 4 responses only 2 are passing the condition INFLIGHT and the first generated
    fun `should watch forecast for current location 1`() {

        val forecastWatchUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.switchMap {
                    Observable.range(0, 3)
                            .map {
                                Response.Stateful.ForecastResponse(Location.bare("Foo"), generateForecast(System.currentTimeMillis()
                                        .timeData()))
                            }
                            .cast(Response::class.java)
                            .startWith(Response.Stateful.InFlight)
                }
            }
        }

        val forecastPresentation = ForecastPresentation(
                forecastWatchUseCase,
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                schedulers)

        val test = forecastPresentation
                .observeState()
                .skip(1) // skip default
                .test()


        forecastPresentation
                .bindIntents(Intent.ForecastWatchIntent.observable())
        test.assertNoErrors()

        //1 in flight and 3 values
        assertEquals(4, test.valueCount())

    }


    //todo FIX THIS TEST!!!
//    @Test
//    fun `should change current location to favorite`() {
//        val location = Location.bare("Foo")
//        val favoriteWatchUseCase: UseCase = mock {
//            on { execute() } doReturn ObservableTransformer { up ->
//                up.flatMap {
//                    Observable.fromArray(
//                            Response.Stateful.FavoriteChangeResponse(Location(locationKey = "Zoo", favorite = true)),
//                            Response.Stateful.FavoriteChangeResponse(Location(locationKey = "Foo", favorite = true)))
//                }
//            }
//        }
//
//        val forecast = generateForecast()
//        val initialState = ForecastState(currentLocation = location,
//                forecast = forecast,
//                currentHourlyForecast = forecast.hourlyForecasts.first())
//
//        val forecastPresentation = ForecastPresentation(
//                neverUseCase(),
//                neverUseCase(),
//                favoriteWatchUseCase,
//                neverUseCase(),
//                neverUseCase(),
//                neverUseCase(),
//                schedulers,
//                initialState)
//
//        val test = forecastPresentation
//                .observeState()
//                .test()
//
//        forecastPresentation.bindIntents(Intent.FavoriteWatchIntent.observable())
//
//        test.assertValueCount(2) // default + change
//        test.assertValueAt(1, initialState.copy(currentLocation = location.copy(favorite = true)))
//    }

}
