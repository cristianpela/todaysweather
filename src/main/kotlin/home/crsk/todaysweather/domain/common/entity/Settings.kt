package home.crsk.todaysweather.domain.common.entity

import java.util.*

data class Settings(val locationKey: String? = null,
                    val langCodeISO: String? = null,
                    val unit: UnitEnum = UnitEnum.METRIC) {

    companion object {
        /**
         * use to mark any proxy settings service to pass the cached settings object to
         * the real settings service and to save it
         */
        val FORCE_SAVE_SETTINGS_FLAG = Settings(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString())
    }

    fun isSet(): Boolean = locationKey != null && langCodeISO != null

}