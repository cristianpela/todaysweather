package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import org.junit.Test
import util.BaseTest


class FullForecastTest: BaseTest(){

    @Test
    fun `should parse the right data`(){
        //todo test it not just print it
        val fullForecast: FullForecast = Gson().fromJson(
                JsonReader(loadResource("test_full_forecast.json")), FullForecast::class.java)
        println(fullForecast.toForecastEntity(-1))
    }


}
