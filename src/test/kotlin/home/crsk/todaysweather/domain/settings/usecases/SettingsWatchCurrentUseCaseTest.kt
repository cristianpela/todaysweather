package home.crsk.todaysweather.domain.settings.usecases

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest

class SettingsWatchCurrentUseCaseTest : BaseTest() {
    @Test
    fun execute() {

        val settingsService: SettingsService = mock {
            on { observe() } doReturn Observable.just(Settings("", "", UnitEnum.METRIC))
        }

        val locationRepository: LocationRepository = mock {
            on { findByKey(anyString()) } doReturn Maybe.just(Location())
        }

        val languageProvider: LanguageProvider = mock {
            on { findLanguageByISO("") } doReturn
                    Single.just(Language("", "", "",1))
        }

        Request.SettingsWatchCurrentRequest().observable()
                .compose(SettingsWatchCurrentUseCase(settingsService, locationRepository,
                        languageProvider).execute())
                .test()
                .assertValueAt(0, Response.Stateful.InFlight)
                .assertValueAt(1, Response.Stateful.DisplayableSettingsResponse(
                        DisplayableSettings(
                                Location(),
                                Language("", "", "", 1),
                                UnitEnum.METRIC)))
    }

}