package home.crsk.todaysweather.domain.common.entity

data class UnitPayload<out T>(val unit: UnitEnum,
                              private val metric: UnitValue<T>, private val english: UnitValue<T>) {

    companion object {

        val noSymbolProvided = fun(_: UnitEnum): String = ""

        fun <T> english(value: T, symbolProvider: (unit: UnitEnum) -> String = { _ -> "" }): UnitPayload<T> {
            val uv = UnitValue(value, symbolProvider(UnitEnum.ENGLISH))
            return UnitPayload(UnitEnum.ENGLISH, uv, uv)
        }

        fun <T> metric(value: T, symbolProvider: (unit: UnitEnum) -> String = { _ -> "" }): UnitPayload<T> {
            val uv = UnitValue(value, symbolProvider(UnitEnum.METRIC))
            return UnitPayload(UnitEnum.METRIC, uv, uv)
        }

        //todo: brittle factory, if change enum values order, it will show bogus mappings
        fun <T> both(valueMetric: T, valueEnglish: T,
                     symbolProvider: (unit: UnitEnum) -> String = { _ -> "" }): UnitPayload<T> {
            val arr = listOf(valueMetric, valueEnglish).mapIndexed { index, value ->
                UnitValue(value, symbolProvider(UnitEnum.values()[index]))
            }
            return UnitPayload(UnitEnum.BOTH, arr[0], arr[1])
        }

        private fun <T> decide(unit: UnitEnum, valueMetric: T, valueEnglish: T,
                               symbolProvider: (unit: UnitEnum) -> String = { _ -> "" }): UnitPayload<T> {
            return if (unit == UnitEnum.ENGLISH)
                UnitPayload.english(valueEnglish, symbolProvider)
            else
                UnitPayload.metric(valueMetric, symbolProvider)
        }


    }

    fun obtain(): UnitValue<T> = when (unit) {
        UnitEnum.ENGLISH -> english
        UnitEnum.METRIC -> metric
        UnitEnum.BOTH -> metric
    }

    fun obtainOther(): UnitValue<T> = when (unit) {
        UnitEnum.ENGLISH -> metric
        UnitEnum.METRIC -> english
        UnitEnum.BOTH -> english
    }

}

data class UnitValue<out T>(val value: T, val symbol: String){
    override fun toString(): String = "${value}${symbol}"
}