package home.crsk.todaysweather.domain.setup.usecases

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doAnswer
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.LocationDetector
import home.crsk.todaysweather.domain.common.gateway.LocationNotDetectedError
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteNotFoundError
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepository
import home.crsk.todaysweather.domain.common.gateway.data.autocomplete.AutoCompleteRepositoryProxy
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete.AutoCompleteListJSON
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Test
import util.BaseTest
import java.io.InputStreamReader
import kotlin.test.assertEquals

class AutomaticSetupUseCaseTest : BaseTest() {
    //NOTE: here, lastKnownLocation flat,  doesn't matter  in AutomaticSetupRequest

    @Test
    fun `should return auto complete fetched`() {

        val location = Location("foo", "bar")
        val autoComplete = Location("foo", "bar")

        val locationDetector: LocationDetector = mock {
            on { detect(true) } doReturn Observable.just(location)
        }

        val autoCompleteRepo: AutoCompleteRepository = mock {
            on { findExact(any(), any()) } doReturn Maybe.just(listOf(autoComplete))
        }

        val languageProvider: LanguageProvider = mock {}

        val useCase = AutomaticSetupUseCase(locationDetector, autoCompleteRepo, languageProvider)

        val test = Observable.just(Request.AutomaticSetupRequest(true)).compose(useCase.execute()).test()

        assertEquals(2, test.valueCount())

        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, { (it as Response.Stateful.SelectedLocationResponse).location == autoComplete })
    }

    @Test
    fun `should return location not detected error`() {

        val locationNotDetectedError = LocationNotDetectedError(Error())

        val locationDetector: LocationDetector = mock {
            on { detect(true) } doReturn Observable.error(locationNotDetectedError)
        }

        val autoCompleteRepo: AutoCompleteRepository = mock {}

        val languageProvider: LanguageProvider = mock {}

        val useCase = AutomaticSetupUseCase(locationDetector, autoCompleteRepo, languageProvider)

        val test = Observable.just(Request.AutomaticSetupRequest(true)).compose(useCase.execute()).test()

        assertEquals(3, test.valueCount())

        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, Response.Stateful.Completed)
        test.assertValueAt(2, { (it as Response.Error).err == locationNotDetectedError.wrapped })
    }

    @Test
    fun `should return auto complete not found error`() {

        val location = Location("foo", "bar")

        val locationDetector: LocationDetector = mock {
            on { detect(true) } doReturn Observable.just(location)
        }

        val autoCompleteRepo: AutoCompleteRepository = mock {
            on { findExact(any(), any()) } doReturn Maybe.error(AutoCompleteNotFoundError)
        }

        val languageProvider: LanguageProvider = mock {}


        val useCase = AutomaticSetupUseCase(locationDetector, autoCompleteRepo, languageProvider)

        val test = Observable.just(Request.AutomaticSetupRequest(true)).compose(useCase.execute()).test()
        assertEquals(3, test.valueCount())

        test.assertValueAt(0, Response.Stateful.InFlight)
        test.assertValueAt(1, Response.Stateful.Completed)
        test.assertValueAt(2, { (it as Response.Error).err == AutoCompleteNotFoundError })
    }

    @Test
    fun `should return auto complete not found error when returned locations are not in maxAllowedDistance range`() {

        val locationDetector: LocationDetector = mock {
            on { detect(true) } doReturn Observable.fromArray(
                    Location("BogusTown", "RO", latitude = 45.666356, longitude = 21.172986),
                    Location("Timis", "RO", latitude = 45.75372, longitude = 21.22571)
            )
        }

        val autoCompleteRepoLocal: AutoCompleteRepository = mock {
            on { findExact(any(), any()) } doReturn Maybe.empty<List<Location>>()
            on { save(any())} doAnswer{
                @Suppress("UNCHECKED_CAST")
                Single.just<List<Location>>(it.arguments[0] as List<Location>)
            }
        }
        val autoCompleteRepoRemote = AutoCompleteRepositoryMockRemote(loadResource("test_auto_complete_sag_ro.json"))

        val autoCompleteRepoProxy = AutoCompleteRepositoryProxy(
                autoCompleteRepoLocal,
                autoCompleteRepoRemote,
                mock {},
                schedulers)

        val languageProvider: LanguageProvider = mock {}

        val useCase = AutomaticSetupUseCase(locationDetector, autoCompleteRepoProxy, languageProvider)

        Observable.just(Request.AutomaticSetupRequest(true))
                .compose(useCase.execute())
                .skip(1) // skip the in flight event
                .test()
                .assertValue(Response.Stateful.SelectedLocationResponse(
                        Location("Timisoara, Romania", "RO", "00000.74.15247", latitude = 45.759998, longitude = 21.230000)
                ))
    }
}


class AutoCompleteRepositoryMockRemote(input: InputStreamReader) : AutoCompleteRepository {

    private val locations: List<Location> = Gson()
            .fromJson<AutoCompleteListJSON>(JsonReader(input), AutoCompleteListJSON::class.java)
            .results
            .filter { it.type == "city" }
            .map { json ->
                Location(json.name, json.c, json.zmw, json.lat, json.lon)
            }

    override fun find(partialSearch: String): Maybe<List<Location>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findExact(city: String, country: String): Maybe<List<Location>> =
            Maybe.create<List<Location>> { emitter ->
                locations
                        .filter { it.city.startsWith(city) && it.countryCode == country }
                        .let {
                            if (it.isEmpty()) emitter.onError(AutoCompleteNotFoundError)
                            else emitter.onSuccess(it)
                        }
            }

    override fun findByKey(key: String): Maybe<Location> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun save(locations: List<Location>): Single<List<Location>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}