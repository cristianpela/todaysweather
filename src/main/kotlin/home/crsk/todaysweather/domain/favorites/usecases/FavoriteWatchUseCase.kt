package home.crsk.todaysweather.domain.favorites.usecases

import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer

class FavoriteWatchUseCase(
        private val settingsService: SettingsService,
        private val favoriteRepository: FavoriteRepository) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(Request.FavoriteWatchRequest::class.java).switchMap {
            favoriteRepository.observeChanges()
                    .flatMapSingle { location ->
                        settingsService.current().flatMap { settings ->
                            favoriteRepository.getFavorite(location.locationKey, settings.unit).map {
                                Response.Stateful.FavoriteChangeResponse(it)
                            }
                        }
                    }
        }
    }
}