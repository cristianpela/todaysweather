package home.crsk.todaysweather.domain.common.entity

import home.crsk.todaysweather.domain.common.other.googleStaticMaps

data class Location(val city: String = "", val countryCode: String = "",
                    val locationKey: String = "",
                    val latitude: Double = 0.0,
                    val longitude: Double = 0.0,
                    val favorite: Boolean = false) {


    companion object {
        val NO_RESULT = Location()
        fun qPath(zmv: String) = "$zmv.json"
        /**
         * jut a wrapper to adapt where we have just a location key and we need to pass a Location object
         */
        fun bare(zmw: String) = Location(locationKey = zmw)


        /**
         * https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
         *
         * Calculate distance between two points in latitude and longitude taking
         * into account height difference.<br>
         *  Uses Haversine method as its base.
         * Altitude is in meters
         * @returns Distance in Meters
         */
        fun distanceBetweenCoordinates(lat1: Double, lat2: Double, lon1: Double, lon2: Double,
                                       startAltitude: Double = 0.0, endAltitude: Double = 0.0): Double {

            val R = 6371 // Radius of the earth

            val latDistance = Math.toRadians(lat2 - lat1)
            val lonDistance = Math.toRadians(lon2 - lon1)
            val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                            Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
            val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
            val flatDistance: Double = R * c * 1000 // convert to meters

            val height = startAltitude - endAltitude

            val distance = Math.pow(flatDistance, 2.0) + Math.pow(height, 2.0)

            return Math.sqrt(distance)
        }

        fun distanceBetweenLocations(location1: Location, location2: Location,
                                     startAltitude: Double = 0.0, endAltitude: Double = 0.0): Double =
                distanceBetweenCoordinates(location1.latitude, location2.latitude, location1.longitude, location2.longitude,
                        startAltitude, endAltitude)

    }

}

fun Location.simpleGoogleStaticMapUrl(apiKey: String?): String =
        googleStaticMaps(apiKey, latitude, longitude) {
            markers {
                this + marker(latitude, longitude)
            }
        }
