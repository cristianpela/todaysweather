package home.crsk.todaysweather.domain.common.gateway

import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.other.TranslateableError
import io.reactivex.Single
import kotlin.properties.ReadOnlyProperty

interface LanguageProvider {
    fun availableLanguages(): Single<List<Language>>
    fun findLanguageByISO(isoCode: String): Single<Language>
    fun translate(langKeyName: String, vararg params: Any): String
    fun translator(vararg params: Any): ReadOnlyProperty<Nothing?, String>

    companion object {

        val ARRAY_LANG_ISO_CODES_KEY = "array_lang_iso_codes"
        val ARRAY_LANG_WU_CODES_KEY = "array_lang_wu_codes"
        val ARRAY_LANG_NAMES_KEY = "array_lang_names_codes"

        val CURRENT_LANGUAGE = "current_language"
        val CURRENT_UNIT_NAME = "current_unit_name"

        val LANGUAGE_FLAG = "ic_flag"

        val MSG_SETTINGS_ALL_SAVED = "msg_settings_all_saved"
        val MSG_SETTINGS_LANG_SAVED = "msg_settings_lang_saved"
        val MSG_SETTINGS_LOCATION_SAVED = "msg_settings_location_saved"
        val MSG_SETTINGS_UNIT_SAVED = "msg_settings_unit_saved"

        //errors
        val ERR_NO_INTERNET = "err_no_internet"
        val ERR_CONNECTION = "err_connection"

        val ERR_LOCATION_PROVIDER_UNAVAILABLE ="err_location_provider_unavailable"
        val ERR_LOCATION_PROVIDER_OUT_OF_SERVICE = "err_location_provider_out_of_service"
        val ERR_LOCATION_NO_PLACE_DECODED =  "err_location_no_place_decoded"

        val ERR_QUERY_NOT_FOUND = "err_query_not_found"
        val ERR_INVALID_API_KEY= "err_invalid_api_key"

    }

}

fun LanguageProvider.translateError(error: TranslateableError): Throwable =
        if (error.transId != null) Error(translate(error.transId, *error.params, error.wrapped))
        else error.wrapped