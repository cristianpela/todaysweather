package home.crsk.todaysweather.domain.common.gateway.data.autocomplete

import io.reactivex.Maybe
import io.reactivex.Single

interface SearchRepository{

    fun find(partialSearch: String): Maybe<String>

    fun save(partialSearch: String): Single<String>

}
