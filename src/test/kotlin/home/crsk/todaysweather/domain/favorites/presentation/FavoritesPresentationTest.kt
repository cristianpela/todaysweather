package home.crsk.todaysweather.domain.favorites.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Favorite
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.favorites.FavoriteState
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.junit.Test
import util.BaseTest
import util.neverUseCase
import java.util.concurrent.atomic.AtomicLong

class FavoritesPresentationTest : BaseTest() {

    @Test
    fun `reducer and order by last updated desc`() {

        val lastUpdatedCounter = AtomicLong()

        fun createFavorite(key: String, favorite: Boolean, lastUpdated: Long? = null): Favorite {
            return Favorite(key, key, favorite, lastUpdated ?: lastUpdatedCounter.incrementAndGet())
        }

        val favoritesWatchUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer {
                it.switchMap {
                    Observable.fromArray(
                            Response.Stateful.FavoriteChangeResponse(createFavorite("foo", favorite = false)),
                            Response.Stateful.FavoriteChangeResponse(createFavorite("bar", favorite = true)),
                            Response.Stateful.FavoriteChangeResponse(createFavorite("bar", favorite = true)),
                            Response.Stateful.FavoriteChangeResponse(createFavorite("moo", favorite = false)),
                            Response.Stateful.FavoriteChangeResponse(createFavorite("zar", favorite = true))
                    )
                }
            }
        }

        val presentation = FavoritesPresentation(
                favoritesWatchUseCase,
                neverUseCase(),
                neverUseCase(),
                neverUseCase(),
                schedulers,
                FavoriteState(listOf(
                        createFavorite("foo", favorite = true),
                        createFavorite("moo", favorite = true)).map { it.toDisplayable() }))

        val test = presentation.observeState().test()

        presentation.bindIntents(Observable.just(Intent.FavoriteWatchIntent))

        test.assertValueAt(0,
                FavoriteState(listOf(
                        createFavorite("foo", favorite = true, lastUpdated = 1),
                        createFavorite("moo", favorite = true, lastUpdated = 2)).map { it.toDisplayable() }))
                .assertValueAt(1,
                        FavoriteState(listOf(createFavorite("moo", favorite = true, lastUpdated = 2)).map { it.toDisplayable() }))
                .assertValueAt(2,
                        FavoriteState(listOf(
                                createFavorite("bar", favorite = true, lastUpdated = 4),
                                createFavorite("moo", favorite = true, lastUpdated = 2)).map { it.toDisplayable() }))
                .assertValueAt(3,
                        FavoriteState(listOf(
                                createFavorite("bar", favorite = true, lastUpdated = 4),
                                createFavorite("moo", favorite = true, lastUpdated = 2)).map { it.toDisplayable() }))
                .assertValueAt(4,
                        FavoriteState(listOf(createFavorite("bar", favorite = true, lastUpdated = 4)).map { it.toDisplayable() }))
                .assertValueAt(5,
                        FavoriteState(listOf(
                                createFavorite("zar", favorite = true, lastUpdated = 7),
                                createFavorite("bar", favorite = true, lastUpdated = 4)).map { it.toDisplayable() }))
    }


}