package home.crsk.todaysweather.domain.forecast.usecases

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test
import util.BaseTest
import kotlin.test.assertEquals

class SelectForecastLocationUseCaseTest: BaseTest() {
    @Test
    fun `should select a new location forecast`() {
        val settingsService: SettingsService = mock {
            on { current() } doReturn Single.just(Settings("FOO", "US"))
            on { save(any()) } doReturn Completable.complete()
        }

        val test = Request.SelectLocationRequest("BAR").observable()
                .compose(SelectForecastLocationUseCase(settingsService).execute())
                .test()

        test.assertValueAt(0, Response.Stateful.Completed)

        val captor = argumentCaptor<Settings>()
        verify(settingsService).save(captor.capture())
        assertEquals(Settings("BAR", "US"), captor.firstValue)
    }

}