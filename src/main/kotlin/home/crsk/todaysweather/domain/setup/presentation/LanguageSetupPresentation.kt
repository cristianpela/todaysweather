package home.crsk.todaysweather.domain.setup.presentation

import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Intent.SelectedLanguageIntent
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.presentation.State
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Request.NoRequest
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.Response.Stateful
import home.crsk.todaysweather.domain.common.usecases.SimpleEchoUseCase
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class LanguageSetupPresentation(private val languageSetupUseCase: UseCase,
                                private val settingsSaveUseCase: UseCase,
                                schedulersContract: SchedulersContract) :
        Presentation<LanguageSetupState>(schedulersContract) {

    private val selectedLanguageUseCase = SimpleEchoUseCase {
        @Suppress("UNCHECKED_CAST")
        (Stateful.Data<String>((it as Request.Data<String>).data))
    }

    init {
        super.superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is Intent.InitIntent<*> -> Request.LanguageSetupRequest
                    is SelectedLanguageIntent -> Request.Data(it.langCodeISO)
                    is Intent.SaveIntent -> Request.SaveRequest.SaveLanguageRequest(it.state as String)
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<LanguageSetupState, Response.Stateful, LanguageSetupState> =
            BiFunction { carrier, response ->
                when (response) {
                    Stateful.InFlight -> carrier.copy().apply { inFlight() }
                    Stateful.Completed -> carrier.copy().apply { completed() }
                    is Stateful.LanguagesResponse -> carrier.copy(languages = response.languages).apply { completed() }
                //it's from the local selectedLanguageUseCase, but try to avoid Data<*> as much as possible
                    is Stateful.Data<*> -> carrier.copy(selectedLangCodeISO = response.data as String).apply { completed() }
                    else -> carrier
                }
            }

    override fun defaultState(): LanguageSetupState = LanguageSetupState()

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(
                    dispatchRequest<Request.Data<*>>(requestObservable, selectedLanguageUseCase),
                    dispatchRequest<Request.SaveRequest.SaveLanguageRequest>(requestObservable, settingsSaveUseCase),
                    dispatchRequest<Request.LanguageSetupRequest>(requestObservable, languageSetupUseCase)
            )
}


data class LanguageSetupState(val languages: List<Language> = emptyList(),
                              val selectedLangCodeISO: String? = null) : State()