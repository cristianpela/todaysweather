package home.crsk.todaysweather.domain.favorites

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.favorite.FavoriteRepository
import home.crsk.todaysweather.domain.common.log.Loggy
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.*
import io.reactivex.ObservableTransformer
import java.util.*

interface UndoFavoriteManager {

    sealed class Op {
        class Add(val location: Location) : Op()
        class Pass(val location: Location?) : Op()
        object Clear : Op()
        object Undo : Op()
    }

    fun execute(): ObservableTransformer<Op, Info>

}

typealias UndoState = Pair<ArrayDeque<Location>, Location?>

data class Info(val hasHistory: Boolean, val size: Int, val stackLog: String)

class UndoFavoriteManagerImpl(private val repository: FavoriteRepository) : UndoFavoriteManager {

    companion object {
        private val initialState: UndoState = UndoState(ArrayDeque(), null)
    }

    override fun execute(): ObservableTransformer<UndoFavoriteManager.Op, Info> =
            ObservableTransformer {
                it.scan(initialState) { acc, op ->
                    val stack = acc.first
                    val location: Location? = when (op) {
                        Undo -> if (stack.isNotEmpty()) stack.pop().let { it.copy(favorite = !it.favorite) } else null
                        is Clear -> stack.clear().let { null }
                        is Add -> stack.push(op.location).let { stack.peek() }
                        is Pass -> op.location
                    }
                    UndoState(stack, location)
                }.skip(1).flatMap { state ->
                    state.second?.let {
                        repository.setFavorite(it)
                                .andThen(state.first.info().observable())
                    } ?: state.first.info().observable()

                }.doOnNext { Loggy.d("UndoFavoriteManager@${this.hashCode()}: $it") }
            }

    fun stackLog(iterable: Iterable<Location>): String =
            this.hashCode().toString() + ":" + iterable.asSequence().map { it.locationKey }.toList().toString()

    fun ArrayDeque<Location>.info() = Info(isNotEmpty(), size, stackLog(this))
}