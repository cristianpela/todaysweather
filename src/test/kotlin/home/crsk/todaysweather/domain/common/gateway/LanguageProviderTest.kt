package home.crsk.todaysweather.domain.common.gateway

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_ISO_CODES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_NAMES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.ARRAY_LANG_WU_CODES_KEY
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider.Companion.LANGUAGE_FLAG
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest
import util.assertEq
import kotlin.test.assertEquals

class LanguageProviderTest : BaseTest() {

    @Test
    fun availableLanguages() {

        val resourceFinder: ResourceFinder = mock {
            on { findArray(eq(ARRAY_LANG_ISO_CODES_KEY)) } doReturn arrayOf("US", "RO", "FR")
            on { findArray(eq(ARRAY_LANG_WU_CODES_KEY)) } doReturn arrayOf("USwu", "ROwu", "FRwu")
            on { findArray(eq(ARRAY_LANG_NAMES_KEY)) } doReturn arrayOf("United States", "Romania", "France")
            on { findDrawableId(anyString()) } doReturn 1
        }

        val lang = LanguageProviderImpl(resourceFinder)

        val test = lang.availableLanguages().test()

        test.assertValueAt(0, {
            it == listOf(
                    Language("US", "USwu", "United States", 1),
                    Language("RO", "ROwu", "Romania", 1),
                    Language("FR", "FRwu", "France", 1)
            )
        })

        val captor = argumentCaptor<String>()
        verify(resourceFinder, times(3)).findDrawableId(captor.capture())
        captor.allValues.assertEq(
                listOf(
                        LANGUAGE_FLAG + "_"+ "US",
                        LANGUAGE_FLAG + "_"+ "RO",
                        LANGUAGE_FLAG + "_"+ "FR"
                )
        )

    }

    @Test
    fun testTranslationDelegation() {

        val resourceFinder: ResourceFinder = mock {
            on { findString(eq("x"), anyVararg()) } doAnswer { "Hello " + it.arguments[0] }
        }

        val lang = LanguageProviderImpl(resourceFinder)
        val x by lang.translator()

        assertEquals("Hello x", x)
    }


}