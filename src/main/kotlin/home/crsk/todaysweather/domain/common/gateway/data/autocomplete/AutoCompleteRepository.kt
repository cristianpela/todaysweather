package home.crsk.todaysweather.domain.common.gateway.data.autocomplete

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import io.reactivex.Maybe

interface AutoCompleteRepository: LocationRepository {

    fun find(partialSearch: String): Maybe<List<Location>>

    fun findExact(city: String, country: String): Maybe<List<Location>>

}

object AutoCompleteNotFoundError : Error()