package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody

class ForecastResponseInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response? {
        //todo needs more kotlin sugar refactor
        return chain?.proceed(chain.request()).also {
            if (isJSONResponse(it)) {
                val content = it?.body()?.string()
                if (content != null) {
                    if (content.contains("querynotfound")) {
                        return createResponse(it, content, 404) // not found
                    } else if (content.contains("keynotfound")) {
                        return createResponse(it, content, 403) // forbidden
                    } else {
                        return createResponse(it, content)
                    }
                }
            }
        }
    }

    private fun isJSONResponse(response: Response?) =
            response != null && response.isSuccessful && response.header("content-type")?.contains("application/json")?:false

    private fun createResponse(originalResponse: Response, content: String, code: Int = 200): Response =
            originalResponse.newBuilder().code(code)
                    .body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), content)).build()

}



