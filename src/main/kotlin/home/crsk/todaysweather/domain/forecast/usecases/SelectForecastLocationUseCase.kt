package home.crsk.todaysweather.domain.forecast.usecases

import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.common.usecases.toResponseCompleted
import io.reactivex.ObservableTransformer

class SelectForecastLocationUseCase(private val settingsService: SettingsService) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(Request.SelectLocationRequest::class.java)
                .flatMap { location ->
                    settingsService.current().flatMapObservable {
                        settingsService
                                .save(Settings(location.locationKey, it.langCodeISO))
                                .toResponseCompleted()
                    }
                }
    }
}