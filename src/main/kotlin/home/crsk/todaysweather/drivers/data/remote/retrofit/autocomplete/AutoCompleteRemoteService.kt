package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import io.reactivex.Maybe
import retrofit2.http.GET
import retrofit2.http.Query

interface AutoCompleteRemoteService {

    @GET("/aq")
    fun find(@Query("query") query: String): Maybe<AutoCompleteListJSON>

    @GET("/aq")
    fun findExact(@Query("query") query: String, @Query("c") country: String): Maybe<AutoCompleteListJSON>

}