package home.crsk.todaysweather.domain.common.gateway

interface ResourceFinder {

    fun findString(name: String, vararg params: Any): String

    fun findInt(name: String): Int

    fun findColor(name: String): Int

    fun findDrawableId(name: String): Int

    fun findArray(name: String): Array<String>

}