package home.crsk.todaysweather.domain.common.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import org.junit.Test
import util.BaseTest
import java.util.concurrent.TimeUnit

class PresentationStreamOpsKtTest : BaseTest() {

    @Test
    fun compositePresentationStreamOps() {

        val fooStream = mock<PresentationStreamOps<Foo>> {
            on { observeState() } doReturn Observable
                    .interval(2, TimeUnit.SECONDS, schedulers.computation())
                    .map {
                        if (it % 2 == 0L) Foo("").apply { inFlight() } else
                            Foo("foo").apply { completed() }
                    }
        }

        val barStream = mock<PresentationStreamOps<Bar>> {
            on { observeState() } doReturn Observable.just(Bar())
        }


        val compositeStream: PresentationStreamOps<Compo> by lazy {
            compositePresentationStreamOps({
                Compo(it[0] as Foo, it[1] as Bar)
            }, fooStream, barStream)
        }

        val test = compositeStream.observeState().take(4).test()

        computationPoolAdvanceBy(10, TimeUnit.SECONDS)

        test
                .assertValueAt(0, { it == Compo(Foo(), Bar()) && it.inFlight })
                .assertValueAt(1, { it == Compo(Foo("foo"), Bar()) && it.completed })

    }
}

data class Foo(val data: String = "") : State()
data class Bar(val data: String = "") : State()
data class Compo(val foo: Foo?, val bar: Bar?) : State()