package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

import home.crsk.todaysweather.drivers.data.remote.retrofit.retrofitBaseBuilder
import retrofit2.Retrofit

val autoCompleteRetrofit: Retrofit by lazy {
    retrofitBaseBuilder
            .baseUrl("http://autocomplete.wunderground.com")
            .build()
}

val autoCompleteService: AutoCompleteRemoteService by lazy {
    autoCompleteRetrofit.create(AutoCompleteRemoteService::class.java)
}


