package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import home.crsk.todaysweather.domain.common.gateway.data.forecast.InvalidApiKeyError
import home.crsk.todaysweather.domain.common.gateway.data.forecast.QueryNotFoundError
import home.crsk.todaysweather.drivers.data.remote.retrofit.BaseRetrofitTest
import home.crsk.todaysweather.drivers.data.remote.retrofit.handleHttpException
import home.crsk.todaysweather.drivers.data.remote.retrofit.retrofitGson
import io.reactivex.observers.TestObserver
import org.junit.Test

class ForecastRemoteServiceTest : BaseRetrofitTest() {

    val test: TestObserver<FullForecast> by lazy {
        mockRetrofit.create(ForecastRemoteService::class.java)
                .getBatch("fooKey", "foo")
                .compose(handleHttpException(::forecastErrorHandler))
                .test()
    }

    @Test
    fun `should return 403 forbidden InvalidApiKeyError error`() {
        val bodyContent = loadResource("test_error_key_not_found.json").readText()
        server.enqueue(createResponse(bodyContent))
        test.assertError { it is InvalidApiKeyError }

    }

    @Test
    fun `should return 404 not found QueryNotFoundError  error`() {
        val bodyContent = loadResource("test_error_query_not_found.json").readText()
        server.enqueue(createResponse(bodyContent))
        test.assertError { it is QueryNotFoundError }
    }

    @Test
    fun `should return 200 with list of temperatures`() {
        val bodyContent = loadResource("test_full_forecast.json").readText()
        server.enqueue(createResponse(bodyContent))
        val expected = retrofitGson().fromJson(bodyContent, FullForecast::class.java)
        test.assertNoErrors()
        test.assertValueAt(0, { it == expected })
    }

    @Test
    fun `should handle number format exceptions with a fallback to a number max or min value`() {
        val bodyContent = loadResource("test_temp_corner_case_nfe.json").readText()
        server.enqueue(createResponse(bodyContent))
        val expected = retrofitGson().fromJson(bodyContent, FullForecast::class.java)
        test.assertNoErrors()
        test.assertValueAt(0, { it == expected })
    }


}