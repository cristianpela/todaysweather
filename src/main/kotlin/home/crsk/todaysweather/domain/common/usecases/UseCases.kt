package home.crsk.todaysweather.domain.common.usecases

import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.translateError
import home.crsk.todaysweather.domain.common.other.TranslateableError
import home.crsk.todaysweather.domain.common.other.observable
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

interface UseCase {

    fun execute(): ObservableTransformer<in Request, out Response>

    fun <T> startWithInFlight(): ObservableTransformer<in T, out Response> = ObservableTransformer { up ->
        up.cast(Response::class.java).startWith(Response.Stateful.InFlight)
    }

    fun errorTranslate(languageProvider: LanguageProvider): (err: Throwable) -> Response.Error = { err: Throwable ->
        when (err) {
            is TranslateableError -> Response.Error(languageProvider.translateError(err))
            else -> Response.Error(err)
        }
    }

    fun errorResumeCompleteTranslated(languageProvider: LanguageProvider): (err: Throwable) -> Observable<Response> = { err: Throwable ->
        val translatedErr = when (err) {
            is TranslateableError -> Response.Error(languageProvider.translateError(err))
            else -> Response.Error(err)
        }
        Observable.fromArray(Response.Stateful.Completed, translatedErr)
    }

    fun errorResumeCompleted(err: Throwable): Observable<Response> =
            Observable.fromArray(Response.Stateful.Completed, err.toResponse())

    fun asMessageCompleted(msg: CommonMessage): Observable<Response> =
            Observable.fromArray(Response.Stateful.Completed, msg.toResponse())

}

fun Completable.toResponseCompleted(withMessage: String? = null): Observable<out Response> =
        this.andThen(if (withMessage == null) Response.Stateful.Completed.observable()
        else Observable.fromArray(Response.Stateful.Completed, CommonMessage.Alert(withMessage).toResponse()))
