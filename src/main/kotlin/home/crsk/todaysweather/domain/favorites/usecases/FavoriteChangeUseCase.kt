@file:Suppress("unused")

package home.crsk.todaysweather.domain.favorites.usecases

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.Add
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.Undo
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.Clear
import home.crsk.todaysweather.domain.favorites.UndoFavoriteManager.Op.Pass
import io.reactivex.ObservableTransformer

class FavoriteChangeUseCase(private val undoFavoriteManager: UndoFavoriteManager) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up ->
                up.cast(Request.FavoriteChangeRequest::class.java)
                        .map {
                            if (it.undoable) {
                                it.location?.let {
                                    if (it == Location.NO_RESULT)
                                        Undo
                                    else
                                        Add(it.copy(favorite = !it.favorite))
                                } ?: Clear
                            } else {
                                Pass(it.location?.let { it.copy(favorite = !it.favorite) })
                            }
                        }
                        .compose(undoFavoriteManager.execute())
                        .map { Response.Stateful.UndoableFavoriteChangeResponse(it.hasHistory) }
            }

}