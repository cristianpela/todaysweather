package home.crsk.todaysweather.domain.common.other

import home.crsk.todaysweather.domain.common.other.GoogleStaticMaps.Companion.AND
import home.crsk.todaysweather.domain.common.other.GoogleStaticMaps.Companion.OR

/**
 * DSL url builder for
 * https://developers.google.com/maps/documentation/
 */
class GoogleStaticMaps(val apiKey: String? = null,
                       val center: Center,
                       var zoom: Zoom = Zoom.CITY,
                       var details: Details = Details(Map())) {
    companion object {
        internal const val BASE_URL = "https://maps.googleapis.com/maps/api/staticmap?"
        internal const val OR = "%7C"
        internal const val AND = "&"
        internal const val SPACE = "%20"
    }

    override fun toString(): String = buildString {
        val apiKeyStr = if(apiKey== null) "" else "${AND}key=$apiKey"
        append(BASE_URL)
                .append(center)
                .append(AND)
                .append(zoom)
                .append(AND)
                .append(details)
                .append(apiKeyStr)
    }
}

class Details(var map: Map, var markers: Markers = Markers()) {
    operator fun invoke(details: Details.() -> Unit) = this.apply(details)
    override fun toString(): String {
        val markersStr = if (markers.isEmpty()) "" else "$AND$markers"
        return "$map$markersStr"
    }
}

enum class Zoom(val level: Int) {
    WORLD(1),
    CONTINENT(2),
    CITY(10),
    STREETS(15),
    BUILDINGS(20);

    override fun toString(): String = "zoom=$level"
}

class Center(var latitude: Double, var longitude: Double) {
    override fun toString(): String = "center=$latitude,$longitude"
}

class Map(var size: Size = Size(640, 640),
          var scale: Scale = Scale.NONE,
          var format: Format = Format.PNG32,
          var type: Type = Type.ROADMAP) {

    operator fun invoke(map: Map.() -> Unit) = this.apply(map)

    override fun toString(): String = "$size$AND$scale$AND$format$AND$type"

    class Size(var width: Int, var height: Int) {

        constructor(square: Int) : this(square, square)

        operator fun invoke(size: Size.() -> Unit) = this.apply(size)

        override fun toString(): String = "size=${width}x$height"
    }

    enum class Scale(val value: Int) {
        NONE(1), DOUBLE(2);

        override fun toString(): String = "scale=$value"
    }

    enum class Format {
        PNG8, PNG32, GIF, JPG;

        override fun toString(): String = "format=${this.name.toLowerCase()}"
    }

    enum class Type {
        ROADMAP, SATELLITE, TERRAIN, HYBRID;

        override fun toString(): String = "maptype=${this.name.toLowerCase()}"
    }
}


class Markers {

    private val markers = mutableListOf<Marker>()

    operator fun invoke(markers: Markers.() -> Unit) = this.apply(markers)

    operator fun plus(marker: Marker) = markers.add(marker)

    fun marker(latitude: Double, longitude: Double, builder: Marker.() -> Unit = {}): Marker =
            Marker(latitude, longitude).apply(builder)

    fun isEmpty(): Boolean = markers.isEmpty()

    override fun toString(): String = if (markers.isEmpty()) "" else buildString {
        markers.forEachIndexed { index, marker ->
            val and = if (index < markers.size - 1) AND else ""
            append(marker).append(and)
        }
    }
}

class Marker(val latitude: Double, val longitude: Double,
             var color: Color = Color.of(Color.Defined.RED),
             var size: Size? = null,
             var label: Char? = null) {

    override fun toString(): String {
        val sizeStr = if (size != null) "$size$OR" else ""
        val upperLabel = label?.toUpperCase()
        val labelStr = if (upperLabel != null) "label:$upperLabel$OR" else ""
        return "markers=$sizeStr$color$OR$labelStr$latitude,$longitude"
    }
}

class Color private constructor(private val color: String) {

    companion object {
        fun ofHex(hexCode: String): Color = Color("0x${hexCode.trim().toUpperCase()}")
        fun ofRGB(red: Int, green: Int, blue: Int): Color {
            val hex: Int = (red shl 16) + (green shl 8) + blue
            return ofHex(hex.toString(16))
        }

        fun of(defined: Defined): Color = Color(defined.name.toLowerCase())
    }

    enum class Defined {
        BLACK, BROWN, GREEN, PURPLE, YELLOW, BLUE, GRAY, ORANGE, RED, WHITE
    }

    override fun toString(): String = "color:$color"
}

enum class Size {
    TINY, MID, SMALL;

    override fun toString(): String = "size:${this.name.toLowerCase()}"
}

fun googleStaticMaps(apiKey: String?, latitude: Double, longitude: Double, zoom: Zoom, builder: Details.() -> Unit): String {
    val details = Details(Map(), Markers())
    val gsm = GoogleStaticMaps(apiKey, Center(latitude, longitude), zoom, details.apply(builder))
    return gsm.toString()
}

fun googleStaticMaps(apiKey: String?,latitude: Double, longitude: Double, builder: Details.() -> Unit): String =
        googleStaticMaps(apiKey, latitude, longitude, Zoom.CITY, builder)





