package home.crsk.todaysweather.domain.common.other

import home.crsk.todaysweather.domain.common.entity.TimeData
import java.util.*
import java.util.concurrent.TimeUnit

fun Calendar.untilNextHour(unit: TimeUnit = TimeUnit.MINUTES): Long {
    val minuteOfHour = get(Calendar.MINUTE)
    return unit.convert(60L - minuteOfHour, TimeUnit.MINUTES)
}

fun now() = System.currentTimeMillis()

fun Long.epochToTime() = this * 1000L

fun Long.timeDiff(otherMillis: Long = System.currentTimeMillis(), timeUnit: TimeUnit): Long =
        timeUnit.convert(Math.abs(otherMillis - this), TimeUnit.MILLISECONDS)

fun Long.timeData(): TimeData =
        calendarFromMillis(this).let {
            return TimeData(this, it.get(Calendar.YEAR), it.get(Calendar.MONTH) + 1,
                    it.get(Calendar.DAY_OF_MONTH), it.get(Calendar.HOUR_OF_DAY),
                    it.get(Calendar.MINUTE), it.get(Calendar.SECOND))
        }

fun Long.isTimeOlderThan(amount: Long, unit: TimeUnit, now: Long = System.currentTimeMillis()): Boolean =
        Math.abs(this - now) >= TimeUnit.MILLISECONDS.convert(amount, unit)

fun Calendar.timeData(): TimeData = TimeData(timeInMillis, get(Calendar.YEAR), get(Calendar.MONTH) + 1,
        get(Calendar.DAY_OF_MONTH), get(Calendar.HOUR_OF_DAY), get(Calendar.MINUTE), get(Calendar.SECOND))

fun calendarFromMillis(time: Long = System.currentTimeMillis(),
                       timeZone: TimeZone = TimeZone.getDefault(),
                       locale: Locale = Locale.getDefault()): Calendar =
        Calendar.getInstance(timeZone, locale).apply {
            timeInMillis = time
        }


fun Calendar.copy(locale: Locale = Locale.getDefault()): Calendar {
    return calendarFromMillis(timeInMillis, timeZone, locale)
}

fun Calendar.shiftTime(calendarConstant: Int = Calendar.HOUR_OF_DAY, amount: Int, locale: Locale = Locale.getDefault()): Calendar =
        copy(locale).apply { add(calendarConstant, amount) }


fun TimeData.advance(calendarConstant: Int = Calendar.HOUR_OF_DAY, amount: Int = 1) =
        calendarFromMillis(this.time).shiftTime(calendarConstant, amount).timeInMillis.timeData()




