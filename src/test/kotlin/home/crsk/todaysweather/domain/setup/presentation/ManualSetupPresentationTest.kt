package home.crsk.todaysweather.domain.setup.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.setup.SetupState
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.junit.Test
import util.BaseTest
import util.neverUseCase
import kotlin.test.assertEquals

class ManualSetupPresentationTest : BaseTest() {

    val location = Location()

    val selectedAutoCompleteUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer { up ->
            up.flatMap {
                Observable.empty<Response>()
            }
        }
    }

    val saveUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer { up ->
            up.flatMap {
                Observable.empty<Response>()
            }
        }
    }

    @Test
    fun `should return selected location when selected key is saved from other session`() {

        val selectedAutoCompleteUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Response.Stateful.SelectedLocationResponse(location).observable()
                }
            }
        }

        val presentation = ManualSetupPresentation(selectedAutoCompleteUseCase, saveUseCase, neverUseCase(), schedulers)

        val test = Observable.merge(presentation.observeState(), presentation.observeMessages()).test()

        presentation.bindIntents(Observable.just(Intent.InitIntent("fooKey")))

        test.assertValueAt(0, SetupState()) // default state
        test.assertValueAt(1, SetupState(selectedLocation = location))
    }

    @Test
    fun `should return no selected location when is no selected key`() {

        val selectedAutoCompleteUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Response.Stateful.SelectedLocationResponse(location).observable()
                }
            }
        }

        val presentation = ManualSetupPresentation(selectedAutoCompleteUseCase, saveUseCase, neverUseCase(), schedulers)

        val test = Observable.merge(presentation.observeState(), presentation.observeMessages()).test()

        presentation.bindIntents(Observable.just(Intent.InitIntent<String>()))

        assertEquals(1, test.valueCount())
        test.assertValueAt(0, SetupState()) // default state

    }


    @Test
    fun `should return selected location when choosen from autocomplete list`() {

        val selectedAutoCompleteUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Response.Stateful.SelectedLocationResponse(location).observable()
                }
            }
        }

        val presentation = ManualSetupPresentation(selectedAutoCompleteUseCase, saveUseCase, neverUseCase(), schedulers)

        val test = Observable.merge(presentation.observeState(), presentation.observeMessages()).test()

        presentation.bindIntents(Observable.just(Intent.SelectLocationIntent("fooKey")))

        test.assertValueAt(0, SetupState()) // default state
        test.assertValueAt(1, SetupState(selectedLocation = location))
    }

    @Test
    fun `should save location`() {
        val saveUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Response.Stateful.Completed.observable()
                }
            }
        }
        val presentation = ManualSetupPresentation(selectedAutoCompleteUseCase, saveUseCase, neverUseCase(), schedulers)

        val test = presentation.observeState().test()

        presentation.bindIntents(Observable.just(Intent.SaveIntent(SetupState(selectedLocation = Location()))))

        test.assertValueAt(0, SetupState()) // default state
        test.assertValueAt(1, { it.completed })
    }

}