package home.crsk.todaysweather.domain.common.presentation

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.Route
import home.crsk.todaysweather.domain.common.gateway.Screen
import io.reactivex.Observable

@Suppress("unused")
sealed class Intent {

    class InitIntent<T>(val data: T? = null): Intent()
    class SaveIntent(val state: Any) : Intent()

    object RetryIntent : Intent()
    object ResetIntent : Intent()
    object CancelIntent : Intent()

    class SelectedLanguageIntent(val langCodeISO: String): Intent()

    object NavigationWatchIntent : Intent()
    class NavigationIntent(val route: Route): Intent()

    class SelectLocationIntent(val locationKey: String?, val owner: String? = null) : Intent()
    class SearchLocationIntent(val partialSearch: String, val owner: String? = null) : Intent()

    object ForecastWatchIntent : Intent()

    object FavoriteWatchIntent : Intent()
    object FavoritesFetchIntent: Intent()
    class FavoriteChangeIntent(val location: Location?, val undoable: Boolean = false) : Intent()

    data class UnitChangeIntent(val unit: UnitEnum): Intent()

    object SettingsWatchCurrentIntent : Intent()
}

fun Intent.observable(): Observable<Intent> = Observable.just(this)


