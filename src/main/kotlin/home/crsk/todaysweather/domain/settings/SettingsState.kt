package home.crsk.todaysweather.domain.settings

import home.crsk.todaysweather.domain.common.entity.DisplayableSettings
import home.crsk.todaysweather.domain.common.presentation.State

data class SettingsState(val displayableSettings: DisplayableSettings? = null): State()