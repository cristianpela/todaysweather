package home.crsk.todaysweather.drivers.data.remote.retrofit.autocomplete

data class AutoCompleteJSON(val name: String, val type: String, val c: String,
                            val zmw: String, val tz: String, val tzs: String,
                            val l: String, val ll: String,
                            val lat: Double, val lon: Double)