package home.crsk.todaysweather.domain.common.entity

data class Favorite(val locationKey: String,
                    val city: String,
                    val isFavorite: Boolean,
                    val lastUpdate: Long,
                    val hour: TimeData? = null,
                    val hourTemperature: UnitPayload<Int>? = null,
                    val hourCondition: String? = null,
                    val hourConditionIcon: String? = null,
                    val hourPrecipitationChance: Int? = null,
                    val minTemperature: UnitPayload<Int>? = null,
                    val maxTemperature: UnitPayload<Int>? = null)