package home.crsk.todaysweather.domain.common.gateway.data.forecast

import com.nhaarman.mockito_kotlin.*
import home.crsk.todaysweather.domain.common.entity.*
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade.Companion.IN_FLIGHT
import home.crsk.todaysweather.domain.common.other.TranslateableError
import home.crsk.todaysweather.domain.common.other.advance
import home.crsk.todaysweather.domain.common.other.timeData
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest
import java.util.*
import java.util.concurrent.TimeUnit

class ForecastRepositoryFacadeTest : BaseTest() {

    private val calendar = Calendar.getInstance().apply {
        set(Calendar.YEAR, 2017)
        set(Calendar.MONTH, 10)
        set(Calendar.DAY_OF_MONTH, 15)
        set(Calendar.HOUR_OF_DAY, 13)
        set(Calendar.MINUTE, 30)
    }

    private val timeOrigin = calendar.timeInMillis

    private val langWUCode = "US"

    private val backOffComputation = schedulers.computationPool[0]

    private val local by lazy<ForecastRepository> {
        mock {
            on {
                getBatch(anyString(), any())
            } doReturn Maybe.empty<Forecast>()

            on {
                saveBatch(anyString(), any())
            } doReturn Single.just(generateForecast(calendar.timeData()))

            on {
                getLastUpdate(anyString())
            } doReturn Single.just(timeOrigin)
        }
    }

    @Before
    fun setUp() {
        computationPoolAdvanceTo(timeOrigin, TimeUnit.MILLISECONDS)
    }

    @Test
    fun `should fetch from remote when there are no prefetch locally or data is stalled`() {

        val forecast: Forecast = generateForecast(calendar.timeData())

        val remote: ForecastRemoteRepository = mock {
            on {
                getBatch(anyString(), anyString())
            } doReturn Observable.just(forecast)
        }

        computationPoolAdvanceBy(1, TimeUnit.DAYS)

        val facade = ForecastRepositoryFacadeImpl(local, remote, schedulers)

        val test = facade.observe("foo", langWUCode, UnitEnum.METRIC).test()

        test.assertValueAt(0, IN_FLIGHT)
        test.assertValueAt(1, forecast)

    }


    @Test
    fun `should throw error when no remote was fetched and can't fall back local`() {
        val error = Error()

        val backOfComputation = schedulers.computationPool[0]

        val remote: ForecastRemoteRepository = mock {
            on {
                getBatch(anyString(), anyString())
            } doReturn Observable.error(error)
        }

        computationPoolAdvanceBy(1, TimeUnit.DAYS)

        val tempFacade = ForecastRepositoryFacadeImpl(local, remote, schedulers)

        val test = tempFacade.observe("foo", langWUCode, UnitEnum.METRIC).test()

        backOfComputation.advanceTimeBy(30, TimeUnit.SECONDS)

        test.assertValue(IN_FLIGHT)
        test.assertError(TranslateableError::class.java)//query not found err is wrapped by translateable err
        verify(remote, times(4)).getBatch(anyString(), anyString())
    }

    @Test
    fun `should fall back to local if there was a remote error like no network`() {
        val error = Error()


        val local: ForecastRepository = mock {
            on {
                getBatch(anyString(), any())
            } doReturn Maybe.just(generateForecast(calendar.timeData(), batchSize = 1))

            on {
                saveBatch(anyString(), any())
            } doReturn Single.just(generateForecast(calendar.timeData()))

            on {
                getLastUpdate(anyString())
            } doReturn Single.just(timeOrigin)
        }

        val remote: ForecastRemoteRepository = mock {
            on {
                getBatch(anyString(), anyString())
            } doReturn Observable.error(error)
        }

        computationPoolAdvanceBy(1, TimeUnit.DAYS)

        val tempFacade = ForecastRepositoryFacadeImpl(local, remote, schedulers)

        val test = tempFacade.observe("foo", langWUCode, UnitEnum.METRIC).test()

        backOffComputation.advanceTimeBy(30, TimeUnit.SECONDS)

        test.assertValueAt(0, IN_FLIGHT)
        test.assertValueAt(1, generateForecast(calendar.timeData(), batchSize = 1))
        test.assertNoErrors()
        verify(remote, times(4)).getBatch(anyString(), anyString()) // backOff test
    }

    @Test
    fun `should fetch from local`() {

        val local: ForecastRepository = mock {
            on {
                getBatch(anyString(), any())
            } doReturn Maybe.just(generateForecast(calendar.timeData()))

            on {
                saveBatch(anyString(), any())
            } doReturn Single.just(generateForecast(calendar.timeData()))

            on {
                getLastUpdate(anyString())
            } doReturn Single.just(timeOrigin)
        }

        val remote: ForecastRemoteRepository = mock {
            on {
                getBatch(anyString(), anyString())
            } doReturn Observable.just(Forecast.NULL)
        }

        val tempFacade = ForecastRepositoryFacadeImpl(local, remote, schedulers)

        val test = tempFacade.observe("foo", langWUCode, UnitEnum.METRIC).test()

        test.assertValueAt(0, IN_FLIGHT)
        test.assertValueAt(1, generateForecast(calendar.timeData()))
        verify(remote, never()).getBatch(anyString(), anyString()) // backOff test
    }

}


fun generateForecast(time: TimeData = System.currentTimeMillis().timeData(), batchSize: Int = 12): Forecast {
    val textForecasts = generateSequence(0) { it + 1 }.map { it ->
        val day = it / 2
        TextForecast(time.advance(Calendar.DAY_OF_MONTH, day),
                day, "icon$day", "title$day",
                UP.metric("text$day"), 100)
    }.take(8).toList()

    val simpleForecasts = generateSequence(0) { it + 1 }.map {
        SimpleForecast(time.advance(Calendar.DAY_OF_MONTH, it),
                UP.metric(it), UP.metric(it), "conditions$it", "icon$it", 100,
                WindSpeed(UP.metric(it), "N", 22),
                WindSpeed(UP.metric(it), "N", 22),
                100, UP.metric(2.2f), UP.metric(2.3f))
    }.take(8).toList()

    val hourlyForecasts = generateSequence(0) { it + 1 }.map {
        HourForecast(time.advance(Calendar.HOUR_OF_DAY, it), UP.metric(it),
                "conditions$it", "icon$it", WindSpeed(UP.metric(it), "N", 22),
                UP.metric(it), 100, 0)
    }.take(batchSize).toList()

    return Forecast(simpleForecasts, textForecasts, hourlyForecasts, time.time)
}


typealias UP<T> = UnitPayload<T>
