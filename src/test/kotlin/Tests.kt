import home.crsk.todaysweather.domain.common.other.toSnakeCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.observers.BaseTestConsumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.Test
import util.BaseTest
import util.assertEq
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals

class TestSource : BaseTest() {

    @Test
    fun debounce() {
        val test = Observable.fromArray(1, 2, 3, 4)
                .debounce(3, TimeUnit.MILLISECONDS)
                .test()
        test.awaitTerminalEvent()
        test.values().assertEq(listOf(4))
    }

    @Test
    fun `to snake case string test`() {
        assertEquals("foo_bar", "FooBar".toSnakeCase())
        assertEquals("foo_bar", "FOO_BAR".toSnakeCase())
        assertEquals("foo_bar", "FOO_Bar".toSnakeCase())
        assertEquals("foo_bar_zoo_mar", "Foo_BAR_ZooMar".toSnakeCase())
    }

    @Test
    fun `should gen 0-0-1-1-2-2-3-3`() {
        generateSequence(0) { it + 1 }
                .map { it / 2 }
                .take(8)
                .toList()
                .assertEq(listOf(0, 0, 1, 1, 2, 2, 3, 3))

    }

    //@Test
    fun `publish scheduling test`() {

        val observeExecutor = Executors.newCachedThreadPool{ Thread("Observation Thread") }
        val subscribeExecutor = Executors.newCachedThreadPool { Thread("Subscription Thread") }

        val src = Observable.fromArray(1, "Hello", 'z' to 'z')
        val abcObs = Observable.fromArray("a", "b", "c")

        val numberTransformer = ObservableTransformer<String, String> { up ->
            up
                    .doOnNext { println("$it subscribe on ${Thread.currentThread()}") }
                    .flatMap { n -> abcObs.map { "$it $n" } }
        }

        val subject = PublishSubject.create<Any>().toSerialized()
        val published = subject
                .publish {
                    Observable.merge(
                            it.ofType(Integer::class.java).map { "Int $it" }
                                    .compose(numberTransformer),
                            it.ofType(Pair::class.java).map { "Pair ${it.first}" }
                                    .compose(numberTransformer),
                            it.ofType(String::class.java).map { "Str $it" }
                                    .compose(numberTransformer)
                    )
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.newThread())
                .publish()

        val test = published.test()

        src.delay(300, TimeUnit.MILLISECONDS).subscribe(subject)

        published.autoConnect(-1)


        test.awaitCount(3, BaseTestConsumer.TestWaitStrategy.SLEEP_100MS, 3000)
        test.values().forEach { println("Sub1 : $it observe on ${Thread.currentThread()}") }
    }

}


