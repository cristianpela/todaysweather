package home.crsk.todaysweather.domain.common.other

import org.junit.Assert.assertEquals
import org.junit.Test
import util.assertEq
import java.util.*
import java.util.concurrent.TimeUnit

class TimeKtTest {
    @Test
    fun untilNextHour() {
        val untilNextHour = Calendar.getInstance()
                .apply { set(2017, 11, 14, 12, 55) }
                .untilNextHour(TimeUnit.SECONDS)
        assertEquals(5 * 60, untilNextHour)
    }

    @Test
    fun `should be older than one day`() {
        System.currentTimeMillis().timeData().advance(Calendar.HOUR, 25)
                .time.isTimeOlderThan(1, TimeUnit.DAYS)
                .assertEq(true)
    }

    @Test
    fun `should transform time unix to timedata`() {
        val timeData = 1511532000000.timeData()
        assertEquals(11, timeData.month)
    }
}