package home.crsk.todaysweather.domain.common.usecases.navigation

import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_APP
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_AUTOMATIC_SETUP
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_CHOOSE_SETUP_TYPE
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_FAVORITES
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_MANUAL_SETUP
import home.crsk.todaysweather.domain.common.gateway.Screens.SCREEN_SETTINGS
import home.crsk.todaysweather.domain.common.usecases.CommonMessage
import home.crsk.todaysweather.domain.common.usecases.SimpleEchoUseCase
import home.crsk.todaysweather.domain.common.usecases.toResponse


//val navToSetupWizard = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_CHOOSE_SETUP_TYPE).toResponse() }
//val navToApp = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_APP).toResponse() }
//val navToManualSetup = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_MANUAL_SETUP).toResponse() }
//val navToAutomaticSetup = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_AUTOMATIC_SETUP).toResponse() }
//val navToSettings = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_SETTINGS).toResponse() }
//val navToFavorites = SimpleEchoUseCase { CommonMessage.Navigation(SCREEN_FAVORITES).toResponse() }
