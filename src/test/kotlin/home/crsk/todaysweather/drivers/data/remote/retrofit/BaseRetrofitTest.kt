package home.crsk.todaysweather.drivers.data.remote.retrofit

import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import retrofit2.Retrofit
import util.BaseTest


open class BaseRetrofitTest: BaseTest(){

    protected lateinit var server: MockWebServer

    protected lateinit var mockRetrofit: Retrofit

    @Before
    fun setUp() {
        server = MockWebServer()
        val urlPath = server.url("/").toString()
        mockRetrofit = retrofitBaseBuilder
                .baseUrl(urlPath)
                //re-route request to server's url
//                .client(okHttpBaseClientBuilder.addInterceptor { chain ->
//                    chain.proceed(Request.Builder().url(urlPath).build())
//                }.build())
                .build()
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    protected fun createResponse(bodyContent: String, code: Int = 200): MockResponse? {
        return MockResponse().setResponseCode(code)
                .setHeader("content-type", "application/json")
                .setBody(bodyContent)
    }

}