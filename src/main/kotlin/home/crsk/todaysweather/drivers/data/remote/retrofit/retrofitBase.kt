package home.crsk.todaysweather.drivers.data.remote.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import home.crsk.todaysweather.drivers.data.remote.retrofit.forecast.ForecastResponseInterceptor
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val retrofitBaseBuilder: Retrofit.Builder by lazy {
    Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(retrofitGson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpBaseClientBuilder.build())
}

val okHttpBaseClientBuilder: OkHttpClient.Builder by lazy {
    OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(ForecastResponseInterceptor())
            .retryOnConnectionFailure(false)
}


inline fun <T> handleHttpException(crossinline handler: (httpErr: HttpException) -> Error?): ObservableTransformer<T, T> = ObservableTransformer { up ->
    up.onErrorResumeNext { err: Throwable ->
        when (err) {
            is HttpException -> Observable.defer<T> { Observable.error(handler(err) ?: err) }
            else -> Observable.error(err)
        }
    }
}

class SafeNumberTypeAdapter<N : Number>(
        private val nullValue: N,
        private val value: (str: String) -> N
) : TypeAdapter<N>() {

    override fun read(reader: JsonReader): N =
            if (reader.peek() == JsonToken.NULL) {
                reader.nextNull()
                nullValue
            } else {
                val next = reader.nextString()
                try {
                    value(next)
                } catch (ex: NumberFormatException) {
                    nullValue
                }

            }


    override fun write(writer: JsonWriter, value: N) {
        writer.value(value)
    }
}

fun retrofitGson(): Gson =
        GsonBuilder()
                .registerTypeAdapter(Int::class.java, SafeNumberTypeAdapter(Int.MAX_VALUE) { it.toInt() })
                .registerTypeAdapter(Class.forName("java.lang.Integer"), SafeNumberTypeAdapter(Int.MAX_VALUE) { it.toInt() })
                .registerTypeAdapter(Long::class.java, SafeNumberTypeAdapter(Long.MAX_VALUE) { it.toLong() })
                .registerTypeAdapter(Class.forName("java.lang.Long"), SafeNumberTypeAdapter(Long.MAX_VALUE) { it.toLong() })
                .registerTypeAdapter(Float::class.java, SafeNumberTypeAdapter(Float.MAX_VALUE) { it.toFloat() })
                .registerTypeAdapter(Class.forName("java.lang.Float"), SafeNumberTypeAdapter(Float.MAX_VALUE) { it.toFloat() })
                .registerTypeAdapter(Double::class.java, SafeNumberTypeAdapter(Double.MAX_VALUE) { it.toDouble() })
                .registerTypeAdapter(Class.forName("java.lang.Double"), SafeNumberTypeAdapter(Double.MAX_VALUE) { it.toDouble() })
                .create()
