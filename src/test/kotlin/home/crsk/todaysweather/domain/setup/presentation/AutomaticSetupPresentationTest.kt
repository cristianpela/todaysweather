package home.crsk.todaysweather.domain.setup.presentation

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.presentation.Intent.*
import home.crsk.todaysweather.domain.common.usecases.*
import home.crsk.todaysweather.domain.setup.SetupState
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import org.junit.Assert.assertEquals
import org.junit.Test
import util.BaseTest
import util.neverUseCase
import kotlin.test.assertEquals

class AutomaticSetupPresentationTest : BaseTest() {

    private val location = Location("foo", "bar", "key")

    private val saveSetupUseCase = mock<UseCase> {
        on { execute() } doReturn ObservableTransformer<Request, Response> { up ->
            up.flatMap {
                Observable.empty<Response>()
            }
        }
    }

    private val selectedAutoCompleteUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer<Request, Response> { up ->
            up.flatMap {
                Observable.just(Response.Stateful.SelectedLocationResponse(location))
            }
        }
    }

    private val automaticSetupUseCase: UseCase = mock {
        on { execute() } doReturn ObservableTransformer { up ->
            up.flatMap {
                Observable.empty<Response>()
            }
        }
    }

    @Test
    fun `should show auto detected location`() {

        val automaticSetupUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Observable.fromArray(Response.Stateful.InFlight, Response.Stateful
                            .SelectedLocationResponse(location))
                }
            }
        }

        val presentation = AutomaticSetupPresentation(automaticSetupUseCase, saveSetupUseCase,
                selectedAutoCompleteUseCase, neverUseCase(), schedulers)

        val test = presentation.observeState().test()

        presentation.bindIntents(Observable.fromArray(InitIntent<String>()))

        test.assertValueAt(1, { it.inFlight})
        test.assertValueAt(2, SetupState(selectedLocation = location))

        assertEquals(3, test.valueCount())

    }

    @Test
    fun `should show already found location with no need for auto detect`() {

        val automaticSetupUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Observable.fromArray(Response.Stateful.InFlight, Response.Stateful.SelectedLocationResponse(location))
                }
            }
        }

        val presentation = AutomaticSetupPresentation(automaticSetupUseCase, saveSetupUseCase,
                selectedAutoCompleteUseCase, neverUseCase(), schedulers)

        val test = presentation.observeState().test()

        //only one auto intent
        presentation.bindIntents(Observable.fromArray(InitIntent("foo")
                , InitIntent("foo"), InitIntent("foo")))

        test.assertValueAt(1, SetupState(selectedLocation = location))

        assertEquals(2, test.valueCount())

    }

    @Test
    fun `should show auto detected location after retry`() {

        val automaticSetupUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Observable.fromArray(Response.Stateful.InFlight, Response.Stateful.SelectedLocationResponse(location))
                }
            }
        }

        val presentation = AutomaticSetupPresentation(automaticSetupUseCase, saveSetupUseCase,
                selectedAutoCompleteUseCase, neverUseCase(), schedulers)

        val test = presentation.observeState().test()


        presentation.bindIntents(Observable.fromArray(
                RetryIntent,
                RetryIntent,
                RetryIntent))

        test.assertValueAt(1, {it.inFlight})
        test.assertValueAt(2, SetupState(selectedLocation = location))


        assertEquals(7, test.valueCount(), "1 default + 2 * retryAttempts")

    }

    @Test
    fun `should confirm detected location`() {
        val automaticSetupUseCase: UseCase = mock {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Observable.empty<Response>()
                }
            }
        }

        val saveSetupUseCase = mock<UseCase> {
            on { execute() } doReturn ObservableTransformer { up ->
                up.flatMap {
                    Observable.fromArray(Response.Stateful.Completed, CommonMessage.Alert("saved").toResponse())
                }
            }
        }

        val presentation = AutomaticSetupPresentation(automaticSetupUseCase,
                saveSetupUseCase,
                selectedAutoCompleteUseCase,
                neverUseCase(),
                schedulers)

        val test = presentation.observeState().test()
        val msgTest = presentation.observeMessages().test()

        presentation.bindIntents(Observable.just(SaveIntent(SetupState(selectedLocation = location))))
        test.assertValueAt(1, { it.completed})
        assertEquals(2, test.valueCount())

        msgTest.assertValue(CommonMessage.Alert("saved"))
    }

}