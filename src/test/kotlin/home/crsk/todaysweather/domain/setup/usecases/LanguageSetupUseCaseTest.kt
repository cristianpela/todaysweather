package home.crsk.todaysweather.domain.setup.usecases

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.LanguageProviderImpl
import home.crsk.todaysweather.domain.common.gateway.ResourceFinder
import home.crsk.todaysweather.domain.common.other.observable
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import org.junit.Test
import org.mockito.ArgumentMatchers

class LanguageSetupUseCaseTest {

    @Test
    fun execute() {
        val resourceFinder: ResourceFinder = mock {
            on { findArray(eq(LanguageProvider.ARRAY_LANG_ISO_CODES_KEY)) } doReturn arrayOf("US", "RO", "FR")
            on { findArray(eq(LanguageProvider.ARRAY_LANG_WU_CODES_KEY)) } doReturn arrayOf("USwu", "ROwu", "FRwu")
            on { findArray(eq(LanguageProvider.ARRAY_LANG_NAMES_KEY)) } doReturn arrayOf("United States", "Romania", "France")
            on { findDrawableId(ArgumentMatchers.anyString()) } doReturn 1
        }
        Request.LanguageSetupRequest
                .observable()
                .compose(LanguageSetupUseCase(LanguageProviderImpl(resourceFinder)).execute())
                .test()
                .assertValueAt(0, {
                    (it as Response.Stateful.LanguagesResponse).languages ==
                            listOf(Language("US", "USwu", "United States",1),
                                    Language("RO", "ROwu", "Romania",1),
                                    Language("FR", "FRwu", "France",1))
                })
    }

}