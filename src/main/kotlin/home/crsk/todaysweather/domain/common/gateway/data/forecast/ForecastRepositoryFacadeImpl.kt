package home.crsk.todaysweather.domain.common.gateway.data.forecast

import home.crsk.todaysweather.domain.common.entity.Forecast
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade.Companion.IN_FLIGHT
import home.crsk.todaysweather.domain.common.other.*
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class ForecastRepositoryFacadeImpl(private val local: ForecastRepository,
                                   private val remote: ForecastRemoteRepository,
                                   private val schedulers: SchedulersContract) : ForecastRepositoryFacade {

    override fun observe(locationKey: String, langWUCode: String, unit: UnitEnum, remoteForced: Boolean): Observable<Forecast> {

        val scheduler = schedulers.computation()

        val localBatchFetch = Observable.defer {
            local.getBatch(locationKey, unit).toObservable()
                    .switchIfEmpty(Observable
                            .error<Forecast>(TranslateableError(QueryNotFoundError("Query not found locally"),
                                    LanguageProvider.ERR_QUERY_NOT_FOUND)))
        }

        val remoteBatchFetchAndSave = Observable.defer {
            remote.getBatch(locationKey, langWUCode).flatMap {
                local.saveBatch(locationKey, it).toObservable()
            }
        }

        return Observable.defer {
            local.getLastUpdate(locationKey).toObservable()
                    .filter {
                        if (remoteForced)
                            false // skip local fetch
                        else
                            !it.isTimeOlderThan(1, TimeUnit.DAYS, scheduler.nowMillis())
                    }
                    .flatMap { localBatchFetch }
                    .switchIfEmpty(remoteBatchFetchAndSave
                            //call schedulers.computation for back-off handling tests to work (see BaseTest class)
                            .retryWhen(errorBackOffObservable(delayScheduler = schedulers.computation())))
                    .onErrorResumeNext { err: Throwable ->
                        when (err) {
                        //in case of api related error we show them
                            is InvalidApiKeyError -> Observable
                                    .error<Forecast>(TranslateableError(err, LanguageProvider.ERR_INVALID_API_KEY))
                            is QueryNotFoundError -> Observable
                                    .error<Forecast>(TranslateableError(err, LanguageProvider.ERR_QUERY_NOT_FOUND))
                        //in case of other errors : network related errors or server errors, we fall-back to local
                        //if possible
                            else -> localBatchFetch
                        }
                    }
        }
    }

}