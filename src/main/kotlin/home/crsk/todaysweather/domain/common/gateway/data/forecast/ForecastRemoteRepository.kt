package home.crsk.todaysweather.domain.common.gateway.data.forecast

import home.crsk.todaysweather.domain.common.entity.Forecast
import io.reactivex.Observable

interface ForecastRemoteRepository {

    fun getBatch(locationKey: String, langCode: String = "US"): Observable<Forecast>

}

class InvalidApiKeyError(msg: String) : Error(msg)
class QueryNotFoundError(msg: String) : Error(msg)
object CorruptedQueryResultError : Error()
