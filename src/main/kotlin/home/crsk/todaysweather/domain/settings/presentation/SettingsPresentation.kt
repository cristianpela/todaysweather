package home.crsk.todaysweather.domain.settings.presentation

import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.Settings
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.settings.SettingsState
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class SettingsPresentation(private val settingsWatchCurrentUseCase: UseCase,
                           private val settingsSaveUseCase: UseCase,
                           private val navigationToUseCase: UseCase,
                           schedulersContract: SchedulersContract,
                           private val initialState: SettingsState = SettingsState()) :
        Presentation<SettingsState>(schedulersContract) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    Intent.SettingsWatchCurrentIntent -> Request.SettingsWatchCurrentRequest()
                    is Intent.UnitChangeIntent -> Request.SaveRequest.SaveUnitRequest(it.unit)
                    is Intent.SaveIntent -> Request.SaveRequest.SaveAllRequest(Settings.FORCE_SAVE_SETTINGS_FLAG)
                    is Intent.SelectLocationIntent -> it.locationKey
                            ?.let { Request.SaveRequest.SaveLocationRequest(Location.bare(it)) }
                            ?: Request.NoRequest
                    is Intent.NavigationIntent -> Request.NavigationToRequest(it.route)
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<SettingsState, Response.Stateful, SettingsState> =
            BiFunction { carrier, response ->
                when (response) {
                    Response.Stateful.InFlight -> carrier.copy().apply { inFlight() }
                    is Response.Stateful.DisplayableSettingsResponse -> carrier
                            .copy(displayableSettings = response.displayableSettings)
                            .apply { completed() }
                    else -> carrier
                }
            }

    override fun defaultState(): SettingsState = initialState

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(
                    dispatchRequest<Request.SettingsWatchCurrentRequest>(requestObservable, settingsWatchCurrentUseCase, true),
                    dispatchRequest(requestObservable, listOf(
                            Request.SaveRequest.SaveUnitRequest::class.java,
                            Request.SaveRequest.SaveLocationRequest::class.java,
                            Request.SaveRequest.SaveAllRequest::class.java
                    ), settingsSaveUseCase),
                    dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase)
            )

}