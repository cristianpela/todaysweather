package util

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals



open class BaseTest(computationPoolSize: Int = 1) {

    protected val schedulers: TestSchedulersContract = TestSchedulersContract(computationPoolSize)

    init {
        assert(computationPoolSize >= 1)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    fun computationPoolAdvanceTo(value: Long, unit: TimeUnit) {
        schedulers.computationPool.forEach {
            it.advanceTimeTo(value, unit)
        }
    }

    fun computationPoolAdvanceBy(value: Long, unit: TimeUnit) {
        schedulers.computationPool.forEach {
            it.advanceTimeBy(value, unit)
        }
    }

    fun loadResource(file: String) = InputStreamReader(javaClass.classLoader
            .getResourceAsStream(file))

}

class TestSchedulersContract(private val testPoolSize: Int) : SchedulersContract {

    private var counter = 0

    val computationPool: List<TestScheduler> = generateSequence { TestScheduler() }.take(testPoolSize).toList()

    override fun main(): Scheduler = Schedulers.trampoline()

    override fun io(): Scheduler = Schedulers.trampoline()

    override fun computation(): Scheduler = computationPool[counter++ % testPoolSize]

}

fun neverUseCase(): UseCase = mock {
    on { execute() } doReturn ObservableTransformer { up ->
        up.flatMap {
            Observable.never<Response>()
        }
    }
}

fun <T> T.assertEq(expected: T, msg: String? = null) = assertEquals(expected, this, msg)