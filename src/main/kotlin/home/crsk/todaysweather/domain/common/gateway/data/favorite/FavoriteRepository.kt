package home.crsk.todaysweather.domain.common.gateway.data.favorite

import home.crsk.todaysweather.domain.common.entity.Favorite
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.entity.UnitEnum
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface FavoriteRepository: LocationRepository{

    fun getFavorite(locationKey: String, unit: UnitEnum): Single<Favorite>

    fun setFavorite(location: Location): Completable

    fun getAll(unit: UnitEnum): Single<List<Favorite>>

}

