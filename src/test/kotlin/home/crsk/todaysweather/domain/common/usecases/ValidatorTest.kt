package home.crsk.todaysweather.domain.common.usecases

import io.reactivex.Observable
import org.junit.Test

class ValidatorTest {
    @Test
    fun validate() {

        val err = Error("Length must be at least three")

        val validator = Validator<String>(listOf(
                object : ValidationRule<String> {
                    override fun validate(item: String): Boolean = item.length >= 3
                    override fun fail(): Throwable = err
                }
        ))

        val test = Observable.fromArray("house", "hello", "do", "some text")
                .compose(validator.validateTransformer()).test()

        test.assertError(err)
    }

}