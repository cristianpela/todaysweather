package home.crsk.todaysweather.domain.autocomplete

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.*
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class AutoCompleteSearchPresentation(
        private val autoCompleteUseCase: UseCase,
        private val navigationToUseCase: UseCase,
        schedulers: SchedulersContract) : Presentation<AutoCompleteSearchState>(schedulers) {

    private val autoCompleteSelectedLocationKeyUseCase = SimpleEchoUseCase {
        val req = it as Request.AutoCompleteSelectLocationRequest
        Response.Message(CommonMessage.AutoCompleteSelectLocationMessage(req.locationKey, req.owner))
    }

    private val autoCompleteSelectedLocationResetUseCase = ResetUseCase()


    init {
        super.superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is Intent.SelectLocationIntent -> if (it.locationKey != null && it.owner != null) {
                        Request.AutoCompleteSelectLocationRequest(it.locationKey, it.owner)
                    } else
                        Request.NoRequest
                    Intent.CancelIntent -> Request.AutoCompleteSearchLocationRequest.CANCEL
                    Intent.ResetIntent -> Request.ResetRequest
                    is Intent.SearchLocationIntent -> Request.AutoCompleteSearchLocationRequest(it.partialSearch, it.owner)
                    is Intent.NavigationIntent -> Request.NavigationToRequest(it.route)
                    else -> Request.NoRequest
                }
            }

    override fun reducer(): BiFunction<AutoCompleteSearchState, Response.Stateful, AutoCompleteSearchState> =
            BiFunction { carrier, response ->
                when (response) {
                    Response.Stateful.InFlight -> carrier(true)
                    Response.Stateful.Completed -> carrier()
                    is Response.Stateful.AutoCompleteResponse -> carrier.copy(
                            partialSearch = response.partialSearch,
                            //assure that once owner is not null it can't be null again
                            owner = if (response.owner != null) response.owner else carrier.owner,
                            locations = response.locations
                    )()
                    is Response.Stateful.Reset -> AutoCompleteSearchState()()
                    else -> carrier
                }
            }

    override fun defaultState(): AutoCompleteSearchState = AutoCompleteSearchState()

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(
                    dispatchRequest<Request.AutoCompleteSearchLocationRequest>(requestObservable, autoCompleteUseCase),
                    dispatchRequest<Request.AutoCompleteSelectLocationRequest>(requestObservable, autoCompleteSelectedLocationKeyUseCase),
                    dispatchRequest<Request.ResetRequest>(requestObservable, autoCompleteSelectedLocationResetUseCase),
                    dispatchRequest<Request.NavigationToRequest>(requestObservable, navigationToUseCase)
            )

}


