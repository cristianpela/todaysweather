package home.crsk.todaysweather.domain.setup.presentation

import home.crsk.todaysweather.domain.common.other.SchedulersContract
import home.crsk.todaysweather.domain.common.presentation.Intent
import home.crsk.todaysweather.domain.common.presentation.Intent.*
import home.crsk.todaysweather.domain.common.presentation.Presentation
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Request.*
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.setup.SetupState
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function

class AutomaticSetupPresentation(private val automaticSetupUseCase: UseCase,
                                 private val settingsSaveUseCase: UseCase,
                                 private val selectedAutoCompleteUseCase: UseCase,
                                 private val navigationToUseCase: UseCase,
                                 schedulers: SchedulersContract) : Presentation<SetupState>(schedulers) {

    init {
        superInit()
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is NavigationIntent -> NavigationToRequest(it.route)
                    RetryIntent -> RetryAutomaticSetupRequest(false)
                    is InitIntent<*> -> it.data?.let { SelectLocationRequest(it as String) } ?: AutomaticSetupRequest(true)
                    is SaveIntent -> (it.state as SetupState).selectedLocation?.let { SaveRequest.SaveLocationRequest(it) } ?: NoRequest
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<SetupState, Response.Stateful, SetupState> =
            BiFunction { carrier, response ->
                when (response) {
                    Response.Stateful.InFlight -> carrier.copy().apply { inFlight() }
                    Response.Stateful.Completed -> carrier.copy().apply { completed() }
                    is Response.Stateful.SelectedLocationResponse -> carrier.copy(selectedLocation = response.location)
                            .apply { completed() }
                    else -> carrier
                }
            }

    override fun defaultState(): SetupState = SetupState()

    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
            listOf(
                    dispatchRequest<AutomaticSetupRequest>(requestObservable, automaticSetupUseCase, true),
                    dispatchRequest<RetryAutomaticSetupRequest>(requestObservable, automaticSetupUseCase,
                            remapRequest = Function{ AutomaticSetupRequest((it as RetryAutomaticSetupRequest).useLastKnownLocation)}),
                    dispatchRequest<SelectLocationRequest>(requestObservable, selectedAutoCompleteUseCase, true),
                    dispatchRequest<NavigationToRequest>(requestObservable, navigationToUseCase),
                    dispatchRequest<SaveRequest.SaveLocationRequest>(requestObservable, settingsSaveUseCase)
            )


}