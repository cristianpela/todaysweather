package home.crsk.todaysweather.domain.common.usecases.navigation

import home.crsk.todaysweather.domain.common.gateway.Navigator
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import home.crsk.todaysweather.domain.common.usecases.toResponseCompleted
import io.reactivex.Completable
import io.reactivex.ObservableTransformer

class NavigationToUseCase(private val navigator: Navigator) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up ->
                up.cast(Request.NavigationToRequest::class.java)
                        .switchMap { req ->
                            Completable.create {
                                navigator.navigateTo(req.route)
                                it.onComplete()
                            }.toResponseCompleted()
                        }
            }
}