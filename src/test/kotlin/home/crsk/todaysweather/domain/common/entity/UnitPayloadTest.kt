package home.crsk.todaysweather.domain.common.entity

import org.junit.Assert.assertEquals
import org.junit.Test

class UnitPayloadTest {
    @Test
    fun obtain() {
        val unit = UnitEnum.BOTH

        val metric = UnitValue(2, UnitEnum.METRIC.degreeSymbol)
        val english = UnitValue(10, UnitEnum.ENGLISH.degreeSymbol)
        val payload = UnitPayload<Int>(unit, metric, english)

        assertEquals(metric, payload.obtain())
        assertEquals(english, payload.obtainOther())


        val metricPayload = UnitPayload.metric(2) { it.degreeSymbol }
        assertEquals(metric, metricPayload.obtain())
        assertEquals(metric, metricPayload.obtainOther())

        val englishPayload = UnitPayload.english(10) { it.degreeSymbol }
        assertEquals(english, englishPayload.obtain())
        assertEquals(english, englishPayload.obtainOther())


        val bothPayload = UnitPayload.both(2, 10, { it.degreeSymbol })
        assertEquals(metric, bothPayload.obtain())
        assertEquals(english, bothPayload.obtainOther())

    }

}