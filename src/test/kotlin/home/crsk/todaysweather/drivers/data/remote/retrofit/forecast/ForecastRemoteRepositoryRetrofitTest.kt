package home.crsk.todaysweather.drivers.data.remote.retrofit.forecast

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import home.crsk.todaysweather.domain.common.gateway.data.forecast.CorruptedQueryResultError
import io.reactivex.Observable
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import util.BaseTest

class ForecastRemoteRepositoryRetrofitTest : BaseTest() {

    @Test
    fun getBatch() {

        val mockSvc: ForecastRemoteService = mock {
            on { getBatch(anyString(), anyString(), anyString()) } doReturn Observable.just(
                    FullForecast(
                            forecast = Forecast(Simpleforecast(emptyList()),
                                    txtForecast = TxtForecast(forecastday = emptyList())),
                            hourlyForecast = emptyList()
                    )
            )
        }

      ForecastRemoteRepositoryRetrofit("", mockSvc)
                .getBatch("","").test().assertError(CorruptedQueryResultError)
    }
}