package home.crsk.todaysweather.domain.setup.usecases

import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.ObservableTransformer

class SetupCheckUseCase(private val settingsService: SettingsService) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> {
        return ObservableTransformer { up ->
            up.flatMap {
                settingsService.observe().map { Response.Stateful.Setup(it) }
            }
        }
    }
}

object NoSetupError : Error()