package home.crsk.todaysweather.domain.forecast.usecases

import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.other.*
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Request.ForecastWatchRequest
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

class ForecastWatchUseCase(private val settingsService: SettingsService,
                           private val locationRepository: LocationRepository,
                           private val languageProvider: LanguageProvider,
                           private val forecastFacade: ForecastRepositoryFacade,
                           private val schedulers: SchedulersContract) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.cast(ForecastWatchRequest::class.java)
                .debounce(300, TimeUnit.MILLISECONDS, schedulers.computation())
                .switchMap {
                    if (it.cancel) {
                        Response.Stateful.Completed.observable()
                    } else {
                        settingsService.observe()
                                .switchMap { settings ->
                                    //we get the location and language objects based on settings first
                                    Observable.zip(
                                            //this is npe-crash worthy cause it means that setup operations were
                                            // not done in the first place
                                            locationRepository.findByKey(settings.locationKey!!).toObservable(),
                                            languageProvider.findLanguageByISO(settings.langCodeISO!!).toObservable(),
                                            BiFunction<Location, Language, Pair<Location, Language>> { loc, lang -> loc to lang }
                                    ).switchMap { locLang ->
                                        val hourlyForecast = forecastFacade.observe(locLang.first.locationKey, locLang.second.wuCode, settings.unit)
                                                .map { Response.Stateful.ForecastResponse(locLang.first, it) }
                                                .cast(Response::class.java)
                                                .onErrorResumeNext(errorResumeCompleteTranslated(languageProvider))
                                                .startWith(Response.Stateful.InFlight)

                                        //cycle once per hour
                                        val scheduler = schedulers.computation()
                                        val untilNextHourDelay = calendarFromMillis(scheduler.nowMillis())
                                                .untilNextHour(TimeUnit.SECONDS)

                                        Observable.interval(untilNextHourDelay, ForecastRepositoryFacade.ONE_HOUR_IN_SECONDS,
                                                TimeUnit.SECONDS, scheduler)
                                                .startWith(0L) // emit immediately force hack
                                                .switchMap {
                                                    hourlyForecast
                                                }
                                    }
                                }
                    }
                }
    }
}