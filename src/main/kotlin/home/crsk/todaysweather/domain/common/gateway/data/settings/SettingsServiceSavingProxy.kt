package home.crsk.todaysweather.domain.common.gateway.data.settings

import home.crsk.todaysweather.domain.common.entity.Settings
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject

class SettingsServiceSavingProxy(private val settingsService: SettingsService) : SettingsService {

    private val savingCache = BehaviorSubject.create<Settings>()

    init {
        settingsService.current().subscribe { settings, err ->
            settings?.let { savingCache.onNext(settings) }
            err?.let { savingCache.onError(it) }
        }
    }

    override fun check(): Completable = settingsService.check()

    override fun current(): Single<Settings> = Single.just(savingCache.value)

    override fun save(settings: Settings): Completable =
            when (settings) {
                Settings.FORCE_SAVE_SETTINGS_FLAG -> Completable.defer {
                    settingsService.save(savingCache.value)
                }.doOnComplete { savingCache.onNext(settings) }
                else -> Completable.complete().doOnComplete { savingCache.onNext(settings) }
            }


    override fun observe(): Observable<Settings> = savingCache.serialize()
}