package home.crsk.todaysweather.domain.forecast.usecases

import home.crsk.todaysweather.domain.common.entity.Language
import home.crsk.todaysweather.domain.common.entity.Location
import home.crsk.todaysweather.domain.common.gateway.LanguageProvider
import home.crsk.todaysweather.domain.common.gateway.data.LocationRepository
import home.crsk.todaysweather.domain.common.gateway.data.forecast.ForecastRepositoryFacade
import home.crsk.todaysweather.domain.common.gateway.data.settings.SettingsService
import home.crsk.todaysweather.domain.common.usecases.Request
import home.crsk.todaysweather.domain.common.usecases.Response
import home.crsk.todaysweather.domain.common.usecases.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction

class ForecastUpdateUseCase(private val settingsService: SettingsService,
                            private val locationRepository: LocationRepository,
                            private val forecastFacade: ForecastRepositoryFacade,
                            private val languageProvider: LanguageProvider) : UseCase {
    
    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer { up ->
        up.switchMap {
            settingsService.current().flatMapObservable { settings ->
                Observable.zip(
                        //this is npe-crash worthy cause it means that setup operations were
                        // not done in the first place
                        locationRepository.findByKey(settings.locationKey!!).toObservable(),
                        languageProvider.findLanguageByISO(settings.langCodeISO!!).toObservable(),
                        BiFunction<Location, Language, Pair<Location, Language>> { loc, lang -> loc to lang }
                ).switchMap { locLang ->
                    //force remote-ing
                    forecastFacade.observe(locLang.first.locationKey, locLang.second.wuCode, settings.unit, true)
                            .map {
                                when (it) {
                                    ForecastRepositoryFacade.IN_FLIGHT -> Response.Stateful.InFlight
                                    else -> Response.Stateful.ForecastResponse(locLang.first, it)
                                }
                            }
                            .cast(Response::class.java)
                            .onErrorResumeNext(errorResumeCompleteTranslated(languageProvider))
                }
            }
        }
    }
}